
{
	"state": {
		"DEPTH_READ":  false,
		"DEPTH_WRITE": false,
		"BLEND":       false
	},

	"attribute_names": ["a_pos"],
	"uniform_names":   ["u_texture0:NEAREST","u_texture0_dim","u_camera_exposure"],

	"vertex_lines": [
		"attribute vec4 a_pos;",

		"varying highp vec2  v_uv;",

		"void main() {  ",
			"gl_Position = vec4(a_pos.x, a_pos.y, 0.0, 1.0);",
			"v_uv    = a_pos.xy * 0.5 + vec2(0.5);",
		"}"
	],

	"fragment_lines": [
		"varying highp vec2  v_uv;",
		"uniform highp float u_camera_exposure;",

		"uniform sampler2D   u_texture0;",
		"uniform highp vec2  u_texture0_dim;",

		"void main()",
		"{",
			"highp float gamma    = 2.2;",
			"highp vec3 tex_color = texture2D(u_texture0, v_uv).xyz;",
			"gl_FragColor         = vec4(tex_color, 1.0);",
		"}"
	]
}



