{
	"state": {
		"DEPTH_READ": false,
		"DEPTH_WRITE": false,
		"BLEND": true
	},

	"attribute_names": ["a_pos", "a_uv", "a_color"],
	"uniform_names":   ["u_view", "u_proj", "u_model", "u_texture0:LINEAR"],

	"vertex_lines": [
		"attribute vec4 a_pos;",
		"attribute vec2 a_uv;",
		"attribute vec4 a_color;",
		"uniform mat4   u_view;",
		"uniform mat4   u_proj;",
		"uniform mat4   u_model;",

		"varying highp vec2 v_uv;",
		"varying highp vec4 v_color;",

		"void main() {  ",
			"gl_Position = u_proj * u_view * u_model * a_pos;",
			"v_color     = a_color;",
			"v_uv        = a_uv;",
		"}"
	],

	"fragment_lines": [
		"uniform sampler2D u_texture0;",

		"varying highp vec2 v_uv;",
		"varying highp vec4 v_color;",

		"void main() {",
			"lowp float gamma = 0.0;",
			"// the threshold.. we define 128 to be the character's edge",
			"lowp float buffer = 0.7;",
			"lowp float dist   = texture2D(u_texture0, v_uv).a;",
			"lowp float alpha  = smoothstep(buffer - gamma, buffer + gamma, dist);",
			"gl_FragColor      = v_color * alpha;",
		"}"
	]
}