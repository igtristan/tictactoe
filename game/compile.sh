# This file builds the project into the compiled directory
# In order for this to run it needs to be at the root of a webserver
# local cors rules make it annoying to get running just in the browser.
# Compiling the code requires the Java 8 SDK to be installed.
#
# Testing locally:
# PHP has an auto hosted server, that can do this work for you
# cd ./compiled
# php -S localhost:8080


test  ! -d "./compiled"     && mkdir ./compiled 
test -d "./compiled/js "    && rm -r ./compiled/js    
test -d "./compiled/assets" && rm -r ./compiled/assets 


# build the source
java -jar ../layer0/igc2.jar -S -js ./src/ ../layer0/ig/ ./compiled/js || exit 1

# copy over the assets
cp -r     ./assets          ./compiled/assets || exit 1

# copy over the js from the layer0 lib
cp        ../layer0/js/*.js ./compiled/js || exit 1

# copy over the index.html
cp        ./src/index.html  ./compiled/index.html || exit 1

#cd compiled
#php -S localhost:8080


#readfile("/path/to/file");