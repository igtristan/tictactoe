<?php

$return_val = 0;

chdir(dirname(__DIR__, 1));

ob_start();
passthru("./compile.sh 2>&1", $return_val);
$output = ob_get_contents();
ob_end_clean(); //Use this instead of ob_flush()

if ($return_val === 0)
{
	readfile('./compiled/index.html');
}
else {
	echo '<h1>Build Failed</h1>';
	echo '<pre>';
	echo $output;
}