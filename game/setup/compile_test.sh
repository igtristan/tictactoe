DIR=`pwd`
cd ~/Projects/iglang2/compiler
./build.sh || exit 1
cd $DIR
cp ../../iglang2/compiler/jar/igc2.jar ../layer0/igc2.jar

java -jar ../../iglang2/compiler/jar/igc2.jar -S -js ./src/ ../layer0/ig/ ./compiled/igvm
rm -r ../layer0

cp -r ../../roadofbones/components/layer0 ../layer0