package;

public class Action
{
	public var m_type      :ActionType;
	
	public var m_player    :Player;			
	
	public var m_token     :BoardToken;
	
	public var m_x         :int;
	
	public var m_y         :int;

	public function initAsPlaceToken(p :Player, t: BoardToken, x : int, y :int) : Action 
	{
		m_type   = ActionType.PLACE_TOKEN;
		m_player = p;
		m_token  = t;
		m_x      = x;
		m_y      = y;
		
		return this;
	}


	public function initAsRestart(p :Player) : Action 
	{
		m_type   = ActionType.RESTART;
		m_player = p;
		return this;
	}
}
