package;

/**
 * This class encompases all of the actions executed for a single match of the game.
 * This allows us to easily generate replays, or convert the game to over the network
 * multiplayer.
 */

public class ActionQueue
{
	private var m_queue :Vector<Action> = new Vector<Action>();

	private var m_index :int = 0;

	public function enqueue(a :Action) :void {
		m_queue.push(a);
	}

	public function dequeue() : Action 
	{
		if (m_queue.length > m_index) {
			var a : Action = m_queue[m_index];
			m_index ++;
			return a;
		}
		return null;
	}

	public function hasNext() : bool {
		return m_index < m_queue.length;
	}
}