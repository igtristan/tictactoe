package;

public enum ActionType extends int
{
	UNDEFINED   = 0,
	PLACE_TOKEN = 1,
	RESTART     = 2;
}
