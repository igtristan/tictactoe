package;

public class Board
{
	// the dimensions of the board (which is always squared)
	public var m_size   :int;
	public var m_width  :int;
	public var m_height :int;

	// tokens
	public var m_board  :Array<BoardToken>;

	// did the cell contribute to the win?
	public var m_winner :Array<bool>;


	//public var m_turn : int; // 0 = first player, 1 = second player
	public var m_turn_number : int;


	public var m_state :BoardState;
	public var m_state_time : double;
	public var m_players :Array<Player>;

	public override function toString() :String {
		var text :String = "[";
		for (var j :int in 0 => m_height) {
			text += "[";
			for (var i :int in 0 => m_width) {
				text += m_board[i + j *m_width] + ",";
			}

			text += "],";
		}
		return text + "]";
	}

	public function initFromBoard(board : Board) :Board
	{
		m_size        = board.m_size;
		m_width       = board.m_width;
		m_height      = board.m_height;

		// looks like copy was broken breaking the prediction logic
		m_board.copy(board.m_board);
		m_winner.copy(board.m_winner);
		m_turn_number = board.m_turn_number;
		m_state       = board.m_state;
		m_players     = board.m_players;

		//for (var i :int in 0 => board.m_width*board.m_height) {
		//	m_board[i]  = board.m_board[i];
		//	m_winner[i] = board.m_winner[i];
		//}
		return this;
	}

	public function  isGameOver() :bool {
		return m_state == BoardState.DRAW ||
				m_state == BoardState.WINNER_X ||
				m_state == BoardState.WINNER_O;
	}

	public function isPlayerWinner(p :Player) : bool {
		if (m_state == BoardState.WINNER_X && getPlayer(0) == p) {
			return true;
		}
		if (m_state == BoardState.WINNER_O && getPlayer(1) == p) {
			return true;
		}
		return false;
	}

	public function isDraw() : bool {
		return m_state == BoardState.DRAW;
	}

	public function constructor(sz :int) {
		m_board   = new Array<BoardToken>(sz*sz);
		m_winner  = new Array<bool>(sz*sz);
		m_width   = sz;
		m_height  = sz;
		m_size    = sz;
		m_players = new Array<Player>(2);
		m_state   = BoardState.TURN_X;
	}


	public function get state_time() : double {
		return m_state_time;
	}

	public function get state() : BoardState {
		return m_state;
	}

	public function set state(s : BoardState) : void {
		if (m_state != s) {
			m_state_time = 0;
			m_state = s;
		}
	}

	public function setPlayer(idx :int, p :Player) :void {
		m_players[idx] = p;
	}

	public function getPlayer(idx :int) : Player {
		return m_players[idx];
	}

	public function isPlayersTurn(p :Player) :bool 
	{
		if (m_state == BoardState.TURN_X && m_players[0] == p) {
			return true;
		}
		if (m_state == BoardState.TURN_O && m_players[1] == p) {
			return true;
		}
		return false;
	}

	/**
	 * Get the value of a cell on the grid
	 */

	public function getValue(x :int, y :int) :BoardToken {
		if (x < 0 || y < 0 || x >= m_width || y >= m_height) {
			throw new Exception("Invalid coordinate: " + x + "," + y);
		}	

		return m_board[x + y*m_width];
	}

	/**
	 * Set a value of a cell on the grid
	 */

	public function setValue(x :int, y :int, value :BoardToken) :void {
		if (x < 0 || y < 0 || x >= m_width || y >= m_height) {
			throw new Exception("Invalid coordinate: " + x + "," + y);
		}	

		m_board[x + y*m_width] = value;
	}

	/**
	 * Did this cell contribute to the win?
	 */


	public function isCellWinner(x :int, y :int) :bool {
		if (x < 0 || y < 0 || x >= m_width || y >= m_height) {
			throw new Exception("Invalid coordinate: " + x + "," + y);
		}	

		return m_winner[x + y*m_width] ;
	}

	public function update(dt : double) :void {
		m_state_time += dt;
	}

	public function action(a :Action, speculative : bool) : ActionResult {

		//System.log("State: " + m_state);

		if (a == null) {
			return ActionResult.NULL_ACTION;
		}

	
		var next_player : bool = false;
		switch(a.m_type) 
		{
			case (ActionType.RESTART) {

				if (!isGameOver()) {
					return ActionResult.INVALID_STATE;
				}

				this.state = BoardState.TURN_X;
				m_board.fill(BoardToken.UNDEFINED);
				m_winner.fill(false);
				m_turn_number = 0;
			}

			case (ActionType.PLACE_TOKEN) 
			{
				if (m_state != BoardState.TURN_X &&
					m_state != BoardState.TURN_O) {

					return ActionResult.INVALID_STATE;
				}


				if (!isPlayersTurn(a.m_player)) 
				{
					System.log("" + m_players[0] + " " + m_players[1] + " outsider: " + a.m_player);
					return ActionResult.INVALID_TURN;
				}

				if (a.m_token != a.m_player.token) {
					return ActionResult.INVALID_PLAYER;
				}

				if (BoardToken.UNDEFINED != getValue(a.m_x, a.m_y)) {
					System.log("" + this);
					System.log("Invalid coordinate: " + a.m_x + "," + a.m_y);
					return ActionResult.INVALID_COORDINATE;
				}

				if (speculative) {
					return ActionResult.OK;
				}

				setValue(a.m_x, a.m_y, a.m_token);
				next_player = true;
				m_turn_number ++;
			}
			
			default {
				return ActionResult.UNKNOWN_ACTION_TYPE;
			}
		}

		//////////////////////////////////////
		// Test for win state
		//////////////////////////////////////
		var winner :BoardToken = checkForWinner();
		if (winner != BoardToken.UNDEFINED) {
			if (winner == BoardToken.X) {
				m_players[0].gameResults(1);
				m_players[1].gameResults(-1);
				this.state = BoardState.WINNER_X;
			}
			else {
				m_players[0].gameResults(-1);
				m_players[1].gameResults(1);
				this.state = BoardState.WINNER_O;
			}
		}
		else {
			if (m_turn_number == m_size * m_size) {
				m_players[0].gameResults(0);
				m_players[1].gameResults(0);
				this.state = BoardState.DRAW;
			}
			else if(next_player) 
			{
				if (m_state == BoardState.TURN_X) {
					this.state = BoardState.TURN_O;
				}
				else if (m_state == BoardState.TURN_O) {
					this.state = BoardState.TURN_X;
				}

			}
		}
	
		return ActionResult.OK;
	}

	public function checkForWinner() : BoardToken
	{
		// look for horizonta and vertical runs
		for (var j :int in 0 => m_size) {

			var run_h : BoardToken = getValue(0,j);
			var run_v : BoardToken = getValue(j,0);

			for (var i :int in 1 => m_size) 
			{
				if (run_h != getValue(i,j)) {
					run_h = BoardToken.UNDEFINED;
				}
				if (run_v != getValue(j,i)) {
					run_v = BoardToken.UNDEFINED;
				}
			}

			if (run_h != BoardToken.UNDEFINED) {
				for (var i :int in 0 => m_size) {
					m_winner[i + j*m_width] = true;
				}
				return run_h;
			}
			if (run_v != BoardToken.UNDEFINED) {
				for (var i :int in 0 => m_size) {
					m_winner[j + i*m_width] = true;
				}
				return run_v;
			}
		}



		var top_left  :BoardToken = getValue(0,0);
		var top_right :BoardToken = getValue(m_size - 1, 0);

		for (var i :int in 1 => m_size) {
			if (getValue(i,i) != top_left) {
				top_left = BoardToken.UNDEFINED;
			}
			if (getValue(m_size - 1 - i, i) != top_right) {
				top_right = BoardToken.UNDEFINED;
			}
		}

		if (top_left != BoardToken.UNDEFINED) {
			for (var i :int in 0 => m_size) {
				m_winner[i + i*m_width] = true;
			}
			return top_left;
		}
		if (top_right != BoardToken.UNDEFINED) {
			for (var i :int in 0 => m_size) {
				m_winner[(m_size - 1 - i) + i*m_width] = true;
			}
			return top_right;
		}

		return BoardToken.UNDEFINED;
	}
	

}
