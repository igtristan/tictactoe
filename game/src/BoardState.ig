package;

public enum BoardState
{
	TURN_X,
	TURN_O,
	WINNER_X,
	WINNER_O,
	DRAW;
}