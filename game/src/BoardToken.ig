package;

public enum BoardToken
{
	UNDEFINED = 0,
	X = 1,
	O = 2;
}