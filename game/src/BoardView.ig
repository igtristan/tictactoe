package;

import renderer.*;
import org.iglang.layer0.*;

public class BoardView
{
	// screen width and height
	private var m_width    :int;
	private var m_height   :int;

	// camera utility class
	private var m_camera   :Camera;

	// graphics utility class
	private var m_graphics :Graphics;

	// the board 
	private var m_board    :Board;

	// how many cells horizontally and vertically does the board run
	private var m_board_width :int;
	private var m_board_height :int;

	// position the board is to be rendered on screen
	private var m_board_x     :double;
	private var m_board_y     :double;

	// the width and height of an individual cell
	private var m_cell_height :double;
	private var m_cell_width  :double;

	// the cell the cursor is hovering over
	private var m_hover_cell       :CellId = CellId.UNDEFINED;
	private var m_hover_cell_alpha :double = 1.0;

	// internal timer for each cell that starts counting
	// up as soon as the cell is filed
	private var m_cell_timers               :Array<double>;
	
	// indicator that the cell contributed to a win for ultra special
	// win animation
	private var m_cell_contributed_to_win   :Array<bool>;

	///////////////////////////////////////////////////////
	// Graphics assets
	///////////////////////////////////////////////////////

	private var m_surface_primary :SimpleSurface;
	private var m_shader_camera   :L0Shader;
	private var m_mesh_plane      :L0Mesh;
	private var u_camera_exposure :L0Name = L0.nameToId("u_camera_exposure");

	public function constructor(b :Board) 
	{
		System.log("Screen Size: " + L0.width + "," + L0.height);

		SDFFontManager.load("/assets/ui_font.dat", "/ui_font.png");
		m_shader_camera = L0Shader.load("/camera.shader");
		m_mesh_plane    = L0Mesh  .load("/plane.mesh");
		
		m_board        = b;
		m_board_width  = b.m_width;
		m_board_height = b.m_height;
		m_width        = L0.width;
		m_height       = L0.height;
		m_camera       = (new Camera()).initAs2d(m_width, m_height, Camera.ANCHOR_TOP_LEFT);
		m_graphics     = new Graphics();

		m_cell_timers             = new Array<double>(m_board_width * m_board_height);
		m_cell_contributed_to_win = new Array<bool>(m_board_width * m_board_height);
		
		m_surface_primary = new SimpleSurface().init(m_width, m_height);
		m_surface_primary.setClearColor(0.0,0.0,0.0,1);

		///////////////////////////////////////////////////////////
		// Center the Board in the screen by adjusting its orientation
		// 32px is left at the top of the screen for text notifications
		///////////////////////////////////////////////////////////
		m_board_x     = 0;
		m_board_y     = 32;
		m_cell_width   = int((m_width  - m_board_x) / double(m_board_width ));
		m_cell_height  = int((m_height - m_board_y) / double(m_board_height));

		if (m_cell_width > m_cell_height) {
			m_cell_width = m_cell_height;
		}
		else {
			m_cell_height = m_cell_width;
		}

		var horizontal_pixels : int = m_cell_width * m_board_width;
		var vertical_pixels :int = m_cell_height * m_board_height;

		// keep on even pixel boundaries to avoid uncessary aliasing
		m_board_x += int((m_width - horizontal_pixels) * 0.5);
		m_board_y += int(((m_height - m_board_y) - vertical_pixels ) * 0.5);
	}

	/**
	 * Get the minimum cell timer for all cells that aren't undefined
	 */

	private function minCellTimer() :double
	{
		var value : double = 10000;
		for (var j :int in 0 => m_board_height)
		{
			for (var i :int in 0 => m_board_width) {
				if (m_board.getValue(i,j) != BoardToken.UNDEFINED) 
				{
					value = Math.min(value, m_cell_timers[i+j*m_board_width]);
				}
			}
		}
		return value;
	}

	/**
	 * Board is notified when an action is processed
	 */

	public function action(a :Action) :void {
		if (a.m_type == ActionType.PLACE_TOKEN) {
			if (m_board.isGameOver()) {
				for (var j :int in 0 => m_board_height) {
					for (var i :int in 0 => m_board_width) {
						m_cell_contributed_to_win[i+j*m_board_width] = m_board.isCellWinner(i,j);
					}
				}
			}
		}
		else if (a.m_type == ActionType.RESTART) {
			m_cell_timers.fill(0.0);
			m_cell_contributed_to_win.fill(false);
		}
	}	

	/**
	 * Transform screen space position to a cell id
	 */

	public function screenPositionToCellId(x :double, y :double) :CellId 
	{	
		x -= m_board_x;
		y -= m_board_y;


		var cell_id : CellId =  CellId.UNDEFINED;

		var cx :int = Math.floor(x / m_cell_width);
		var cy :int = Math.floor(y / m_cell_height);
		if (0 <= cx && cx < m_board_width && 
			0 <= cy && cy < m_board_height) {
			cell_id  =  CellId.create(cx,cy);
		}

		return cell_id;
	}



	public function update(dt : double) :void 
	{
		// update the cell timer for non empty cells
		for (var j :int in 0 => 3) {
			for (var i :int in 0 => 3) {
				var token :BoardToken = m_board.getValue(i,j);
				if (token == BoardToken.UNDEFINED) {
					continue;
				}

				m_cell_timers[i + j*m_board_width] += dt;
			}
		}
	}

	public function renderBoard(g :Graphics) : void 
	{
		g.translate(m_board_x, m_board_y);

		// Render the background of the board Background
		/////////////////////////////////////////////////
		g.push();
		{
			for (var j :int in 0 => 3) 
			{
				for (var i :int in 0 => 3) 
				{
					var x0 : double = i * m_cell_width;
					var y0 : double = j * m_cell_height;

					// use some bit math to do a checkerboard pattern
					var alpha :double = (((i ^ j) & 1) == 0) ? 0.25 : 0.2;

					g.push();
					g.translate(x0, y0);
					g.setTint(1.0, 0.0, 0.0, alpha);
					g.fillRect(m_cell_width, m_cell_height);
					g.pop();
				}
			}
		}
		g.pop();


		// Draw a hilite in the cell that the cursor is over
		/////////////////////////////////////////////
		if (m_hover_cell != CellId.UNDEFINED && m_hover_cell_alpha > 0.0)
		{
			g.push();
		
			var x0 : double = m_hover_cell.x * m_cell_width;
			var y0 : double = m_hover_cell.y * m_cell_height;

			g.setTint(1.0, 1.0, 1.0, 0.05 + 0.025 * Math.sin(m_board.state_time * Math.PI) );
			g.translate(x0,y0);
			g.fillRect(m_cell_width, m_cell_height);
		
			g.pop();
		}
		

		g.push();

		var game_over :bool = m_board.isGameOver();

		var cell_padding :int = 4;
		g.setSDFFont("default", m_cell_height );

		var seq_number :int = -1;
		for (var j :int in 0 => m_board_height) 
		{
			for (var i :int in 0 => m_board_width) 
			{
				var winner   :bool = m_cell_contributed_to_win [i + j *m_board_width];
				var timer  :double = m_cell_timers[i + j*m_board_width];
				if (timer == 0) {
					continue;
				}

				if (winner) {
					seq_number ++;
				}

				var intro_timer :double = Math.min(1.0, timer / 0.1);

				var alpha :double = Easing.smoothStep(0,   1, intro_timer);
				var zoom  :double = Easing.smoothStep(100, 0, intro_timer);


				if (game_over) 
				{
					if ( !winner)
					{
						// fade out
						alpha *= Easing.smoothStep(1, 0.25, Math.min(1.0, m_board.state_time / 0.25));
					}
					else {
						// stagger a grow by 100ms
						zoom += Easing.easeOutBack(0, 15,  
							Math.max(0.0, Math.min(1.0, (m_board.state_time - seq_number*0.15 - 0.25) / 0.25) ));
					}
				}

				var token :BoardToken = m_board.getValue(i,j);

				var x0 : double = i * m_cell_width;
				var y0 : double = j * m_cell_height;

				g.push();
				g.translate(x0, y0);
				g.setTint(1.0, 1.0, 1.0, alpha );

				var char : int = 0;
				var token_string :String = null;
				if (token == BoardToken.X) 
				{
					token_string = ("X");
					char = 'X';
				}
				else if (token == BoardToken.O) 
				{
					token_string = ("O");
					char = 'O';
				}
				
				if (token_string != null)
				{
					// okayisn approximation
					var h :double = 
						SDFFontManager.findFontGlyph("default", char).getRenderedHeight(m_cell_height);

					g.translate3d(
						0.5 * (m_cell_width - g.getTextWidth(token_string)),
						zoom,
						m_cell_height - int(m_cell_height * 0.15) );	// -  0.5 * (m_cell_height - h));
					g.drawText(token_string);
				}
				g.pop();
			}
		}
		g.pop();
	}


	public function render() :void 
	{
		L0.setGlobalUniform(u_camera_exposure, 0x01, 1.0, 0,0,0);

		m_surface_primary.bindAndClear();
		m_camera.bind();
	
		//System.log("" + m_camera.transform);

		
		var g :Graphics = m_graphics;
		
		g.clear();
		g.setFont("default",  16);
		g.setTint(1.0, 1.0, 1.0,1.0);


		m_hover_cell = CellId.UNDEFINED;
		for (var i :int in 0 => 2) {
			var p :Player = m_board.getPlayer(i);
			if (m_board.isPlayersTurn(p) && p.is_local) {
				m_hover_cell = p.hover_cell_id;
			}
		}

		g.push();
		{
			renderBoard(g);
		}
		g.pop();

		g.push();
		switch (m_board.state) 
		{
			case (BoardState.TURN_X) 
			{
				turnIndicatorText(g, "Player 1");
			}

			case (BoardState.TURN_O) 
			{
				turnIndicatorText(g, "Player 2 - AI");		
			}

			case (BoardState.WINNER_X) {
				endgameText(g,"Player 1 Wins");
			}

			case (BoardState.WINNER_O) 
			{
				endgameText(g,"Player 2 Wins");
			}

			case (BoardState.DRAW) 
			{
				endgameText(g,"Draw");
			}
		}
		g.push();

		
		// flush all pending graphics data to scren
		m_graphics.flush();
		
		// write the double buffered graphics to screen
		// this is done so that in the future we can perform post effects
		L0Surface.DEFAULT.bind();
		L0Surface.DEFAULT.clear();
		m_shader_camera.bind();
		L0.meshDraw1(m_mesh_plane, m_surface_primary.color_texture);
	}


	public function turnIndicatorText(g : Graphics, txt :String) : void 
	{
		g.setSDFFont("default",  24);
		g.translate(0.5 * (L0.width - g.getTextWidth(txt)), 28);
		g.drawText(txt);
	}

	public function endgameText(g :Graphics, txt :String) : void
	{
		var display_delay :double = 1.0;

		var min_cell_timer :double = minCellTimer();
		if (min_cell_timer < display_delay) {
			return;
		}

		var alpha :double = Math.min(1.0, (min_cell_timer - display_delay) / 0.5);

		if (alpha > 0)
		{
			alpha = Easing.smoothStep(0, 1, alpha);

			g.setTint(0.0, 0.0, 0.0, 0.75 * alpha);
			g.fillRect(m_width + 1, m_height + 1);


			g.setTint(1.0, 1.0, 1.0, 1.0 * alpha);
			g.setSDFFont("default",  64);
			g.translate(0.5 * (L0.width - g.getTextWidth(txt)), L0.height*0.5);
			g.drawText(txt);
			g.setSDFFont("default",  24);
			g.translate(0, 32);
			g.drawText("Click to play again...");
		}
	}


	private var m_frame_fps          :int = 0;
	private var m_frame_bucket       :int = 0;
	private var m_frame_bucket_count :int = 0;	

	public function framePost(frame_start :double, frame_end :double) :void 
	{
		var second :int = frame_start / 1000.0;
		if (second != m_frame_bucket) {


			m_frame_fps = m_frame_bucket_count;
			m_frame_bucket = second;
			m_frame_bucket_count = 0;
		}

		m_frame_bucket_count ++;


		m_camera.bind();
		var g :Graphics = m_graphics;
		g.clear();
		g.translate(0, 24);	
		g.setSDFFont("default", 24);
		g.setTint(1,0,0,1);
		g.drawText("FPS: " + m_frame_fps + " MS: " + (frame_end - frame_start));
		g.flush();
	}
}