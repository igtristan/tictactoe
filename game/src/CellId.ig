package;

public enum CellId
{
	UNDEFINED = -1;

	public static function create(x :int, y :int) : CellId {
		return (y #<< 16) | x;
	}

	public function get x() : int {
		return int(this) & 0xffff;
	}

	public function get y() :int {
		return int(this) #>>> 16;
	}

	public override function toString() :String {
		return "CellId[" + this.x + "," + this.y + "]";
	}
}