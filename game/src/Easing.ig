package;

/**
 *
 **/
public class Easing
{
	/////////////////////////////////////////////////////////////////////////
	// Functional Easings
	/////////////////////////////////////////////////////////////////////////
	public static function lerp(x0:double, x1:double, k:double) : double
	{
		return x0 + (x1-x0) * k;
	}
	
	public static function smoothStep(x0:double, x1:double, k:double):double
	{
		return lerp(x0, x1, k*k*(3 - 2*k));
	}
	
	/////////////////////////////////////////////////////////////////////////
	// Standard Traditional Easing
	/////////////////////////////////////////////////////////////////////////
	/**
	 * Rears back beyond x0 point then shoots forward.
	 **/
	public static function easeInBack(x0:double, x1:double, k:double) : double
	{
		var s:double = 1.70158;
		x1 -= x0;
		k  /= 1;
		return x1 * k * k * ((s + 1) * k - s) + x0;
	}
	
	/**
	 * shoots forward, slowly goes past x1 point and retracts.
	 **/
	public static function easeOutBack(x0:double, x1:double, k:double) : double
	{
		var s:double = 1.70158;
		x1 -= x0;
		k   = (k/1) - 1;
		return x1 * ((k) * k * ((s + 1) * k + s) + 1) + x0;
	}	
	
	// rears back beyond x0 point then shoots forward, slows down and goes past x1 point and retracts.
	public static function easeInOutBack(x0:double, x1:double, k:double) : double
	{
		var s:double = 1.70158;
		x1 -= x0;
		k /= 0.5;
		
		if ((k) < 1)
		{
			s *= (1.525);
			return x1 / 2 * (k * k * (((s) + 1) * k - s)) + x0;
		}
		
		k -= 2;
		s *= (1.525);
		return x1 / 2 * ((k) * k * (((s) + 1) * k + s) + 2) + x0;
	}	
	
	/////////////////////////////////////////////////////////////////////////
	// Geometric Easing Functions
	// These are in order of the brutality of their curve.
	// ie; Quint is a nice easy acceleration/decelleration while expo 
	// basically doen'st move until it flies like a bat out of hell.
	/////////////////////////////////////////////////////////////////////////
	public static function easeInExpo(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return x1 * Math.pow(2, 10 * (k / 1 - 1)) + x0;
	}

	public static function easeOutExpo(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return x1 * (-Math.pow(2, -10 * k / 1) + 1) + x0;
	}

	public static function easeInOutExpo(x0:double, x1:double, k:double) : double
	{
		k /= 0.5;
		x1 -= x0;
		if (k < 1) {
			return x1 / 2 * Math.pow(2, 10 * (k - 1)) + x0;
		}
		k--;
		return x1 / 2 * (-Math.pow(2, -10 * k) + 2) + x0;
	}


	public static function easeInQuint(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return x1 * k * k * k * k * k + x0;
	}

	public static function easeOutQuint(x0:double, x1:double, k:double) : double
	{
		k--;
		x1 -= x0;
		return x1 * (k * k * k * k * k + 1) + x0;
	}

	public static function easeInOutQuint(x0:double, x1:double, k:double) : double
	{
		k /= 0.5;
		x1 -= x0;
		if (k < 1) {
			return x1 / 2 * k * k * k * k * k + x0;
		}
		k -= 2;
		return x1 / 2 * (k * k * k * k * k + 2) + x0;
	}
	

	public static function easeInQuart(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return x1 * k * k * k * k + x0;
	}

	public static function easeOutQuart(x0:double, x1:double, k:double) : double
	{
		k--;
		x1 -= x0;
		return -x1 * (k * k * k * k - 1) + x0;
	}

	public static function easeInOutQuart(x0:double, x1:double, k:double) : double
	{
		k /= 0.5;
		x1 -= x0;
		if (k < 1) {
			return x1 / 2 * k * k * k * k + x0;
		}
		k -= 2;
		return -x1 / 2 * (k * k * k * k - 2) + x0;
	}

	public static function easeInCubic(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return x1 * k * k * k + x0;
	}

	public static function easeOutCubic(x0:double, x1:double, k:double) : double
	{
		k--;
		x1 -= x0;
		
		return x1 * (k * k * k + 1) + x0;
	}
	
	public static function easeOutCubicInverse(k:double) : double
	{
		return 1.0 - Math.pow( (1.0-k), 1.0/3.0);
	}
	
	

	public static function easeInOutCubic(x0:double, x1:double, k:double) : double
	{
		k /= 0.5;
		x1 -= x0;
		if (k < 1) {
			return x1 / 2 * k * k * k + x0;
		}
		k -= 2;
		return x1 / 2 * (k * k * k + 2) + x0;
	}

	public static function easeInQuad(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return x1 * k * k + x0;
	}

	public static function easeOutQuad(x0:double, x1:double, k:double) : double{
		x1 -= x0;
		return -x1 * k * (k - 2) + x0;
	}

	public static function easeInOutQuad(x0:double, x1:double, k:double) : double
	{
		k /= 0.5;
		x1 -= x0;
		if (k < 1) {
			return x1 / 2 * k * k + x0;
		}
		k--;
		return -x1 / 2 * (k * (k - 2) - 1) + x0;
	}

	/////////////////////////////////////////////////////////////////////////
	// Circular Based Easings
	/////////////////////////////////////////////////////////////////////////
	public static function easeInCirc(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return -x1 * (Math.sqrt(1 - k * k) - 1) + x0;
	}

	public static function easeOutCirc(x0:double, x1:double, k:double) : double
	{
		k--;
		x1 -= x0;
		return x1 * Math.sqrt(1 - k * k) + x0;
	}

	public static function easeInOutCirc(x0:double, x1:double, k:double) : double
	{
		k /= 0.5;
		x1 -= x0;
		if (k < 1) {
			return -x1 / 2 * (Math.sqrt(1 - k * k) - 1) + x0;
		}
		k -= 2;
		return x1 / 2 * (Math.sqrt(1 - k * k) + 1) + x0;
	}


	public static function easeInSine(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return -x1 * Math.cos(k / 1 * (Math.PI / 2)) + x1 + x0;
	}

	public static function easeOutSine(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return x1 * Math.sin(k / 1 * (Math.PI / 2)) + x0;
	}

	public static function easeInOutSine(x0:double, x1:double, k:double) : double
	{
		x1 -= x0;
		return -x1 / 2 * (Math.cos(Math.PI * k / 1) - 1) + x0;
	}
	
	
	
	
	public static function sin(x0:double, x1:double, k:double) : double
	{
		k = Math.sin( Math.PI/2 * k ); 
		return x0 + (x1-x0) * k;
	}

	public static function cos(x0:double, x1:double, k:double) : double
	{
		k = 1 - Math.cos( Math.PI/2 * k );
		return x0 + (x1-x0) * k;
	}

	
}
