package;

/**
	This is the primary entrypoint of the application.
	This does the following on init:
		- set up gameboard and view
		- set up a local player and an ai player
		- wire everyone together

	On update:
		- update each Player
		- execute the action queues on the board
		- update the view
*/

import org.iglang.layer0.**;

public class Main implements L0App
{
	var m_players :Array<Player> = new Array<Player>(2);
	var m_view    :BoardView;

	var m_board   :Board;
	var m_queue   :ActionQueue = new ActionQueue();


	public function init(origin :double): void
	{
		System.log("Init");

		// local player also doubles as the view
		m_board = new Board(3);
		m_view  = new BoardView(m_board);

		m_players[0] = new PlayerLocal(m_view);
		m_players[0].init(0, m_board, BoardToken.X, m_queue);
	
		m_players[1] = new PlayerAI();
		m_players[1].init(1, m_board, BoardToken.O, m_queue);

		m_board.setPlayer(0, m_players[0]);
		m_board.setPlayer(1, m_players[1]);

		System.log("Init Complete");

	}

	
	public function update(time :double, dt :double): void
	{
		for (var i :int in 0 => m_players.length) {
			m_players[i].update(dt);
		}

		var a :Action = m_queue.dequeue();
		if (a != null) 
		{
			if (m_board.action(a, false) == ActionResult.OK) {	
				m_view.action(a);
			}
		}
		
		m_board.update(dt);
		m_view.update(dt);
	}

	public function render(time :double, dt :double) : void 
	{
		
		m_view.render();
		
	
	}

	public function framePost(frame_start :double, frame_end :double) :void 
	{
		m_view.framePost(frame_start, frame_end);
	}
}
