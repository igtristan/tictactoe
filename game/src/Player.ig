package;

public class Player
{
	// the id of the player
	protected var m_id           :int;

	// what token the player is placing o n the board
	protected var m_token        :BoardToken;

	// the board the game is taking place on
	protected var m_board        :Board;

	// the queue used to write actions to
	protected var m_action_queue :ActionQueue;

	// what cell the player is hovering over
	protected var m_hover_cell   :CellId = CellId.UNDEFINED;

	// the results thus far for the player over all games
	protected var m_wins         :int = 0;
	protected var m_losses       :int = 0;
	protected var m_draws        :int = 0;


	public function init(player_id :int, b :Board, t :BoardToken, q :ActionQueue) :void {
		m_id           = player_id;
		m_token        = t;
		m_board        = b;
		m_action_queue = q;
	}

	public function gameResults(status :int) :void {
		if (status == -1) {
			m_losses ++;
		}
		else if (status == 1) {
			m_wins ++;
		}
		else {
			m_draws ++;
		}
	}

	public override function equals(other : Object) : bool {
		return Player(other).m_id == m_id;
	}

	public override function toString() : String {
		return "Player[" + m_id + "," + m_token + ",is-local:" + this.is_local + "]";
	}

	public function get token() : BoardToken {
		return m_token;
	}

	public function get id() : int {
		return m_id;
	}

	public function get hover_cell_id() : CellId {
		return m_hover_cell;
	}

	public function get is_local() :bool {
		return false;
	}

	public function enqueue(a : Action) : void {
		if (a.m_player != this) {
			throw new Exception("Can only enqueue events for yourself.");
		}
		m_action_queue.enqueue(a);  // 
	}

	public function update(dt :double) :void {
	}
}
