package;

public class PlayerAI extends Player
{

	private var m_prediction :Board = null;
	

	/**
		Not the smartest AI.

		In order of priorities:
			1) Choose a move that will cause a win
			2) Choose a move that will cause a draw
			3) Choose a random move
	*/




	public override function update(dt : double) :void
	{
		if (m_prediction == null) {
			m_prediction = new Board(m_board.m_size);
		}

		if (m_board.isPlayersTurn(this) )
		{


			if (m_board.state_time > 0.5)
			{
				System.log("AI... THINKING");

				var priority        :int    = -1;
				var candidate_set : Vector<CellId> = new Vector<CellId>();

				var action : Action = new Action();

				for (var j :int in 0 => m_board.m_height)
				{
					for (var i :int in 0 => m_board.m_width) 
					{
						var val :BoardToken = m_board.getValue(i,j);

						// Ignore occupied cells
						//////////////////////////
						if (val != BoardToken.UNDEFINED) {
							continue;
						}

						// Predict what would happen if we placed our token here
						//////////////////////////////////
						m_prediction.initFromBoard(m_board);

						action.initAsPlaceToken(this, this.token, i, j);
						if (ActionResult.OK != m_prediction.action(action, false)){
							System.log("ACTION AI FAILURE: " + m_board);
						}

						var move_priority :int = -1;
						var move_cell_id:CellId = CellId.create(i,j);

						if (m_prediction.isPlayerWinner(this)) {
							move_priority = 2;
						}
						else if (m_prediction.isDraw()) {
							move_priority = 1;
						}
						else {
							move_priority = 0;
						}

						// reject moves of a lower priority than what we found
						if (priority > move_priority) {
							continue;
						}
						
						// we found a higher priority so clear out the candidate set	
						if (priority < move_priority) {
							candidate_set.length = 0;
						}

						candidate_set.push(move_cell_id);
						priority = move_priority;
					}
				}






				// if multiple equivilant (in terms of priority) moves are found randomly choose one
				var target_cell :CellId = CellId.UNDEFINED;
				if (candidate_set.length > 0) {
					target_cell = candidate_set[int(Math.random()*candidate_set.length)];
				}

				if (target_cell == CellId.UNDEFINED) {
					throw new Exception("Was not able to decide on a move");
				}

				System.log("Placing token for AI: " + m_token + " " + target_cell.x + " " + target_cell.y);
				action.initAsPlaceToken(this, m_token, target_cell.x, target_cell.y);
				this.enqueue(action);
			}
		}
	}
}