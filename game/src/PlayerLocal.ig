package;

import org.iglang.layer0.**;

public class PlayerLocal extends Player
{
	private var m_event :L0Event ;
	private var m_view  :BoardView;

	public function constructor(v :BoardView)  {
		m_view  = v;
		m_event = new L0Event();
	}

	public override function get is_local() :bool {
		return true;
	}

	public override function update(dt : double) : void 
	{
		// dequeue UI events and process them
		while (L0.eventPoll(m_event)) 
		{
			var e :L0Event = m_event;
			if (m_event.is_mouse) 
			{
				m_hover_cell = m_view.screenPositionToCellId(m_event.screen_x, m_event.screen_y);
				
				if (e.type == L0Event.MOUSE_PRESSED)
				{
					if (m_board.isGameOver()) {
						var action :Action = (new Action()).initAsRestart(this);
						enqueue(action);
					}
					if (m_hover_cell != CellId.UNDEFINED)
					{
						var action : Action = (new Action()).initAsPlaceToken(this, m_token, m_hover_cell.x, m_hover_cell.y);
						var action_result :ActionResult = m_board.action(action, true);
						if (action_result == ActionResult.OK) {
							enqueue(action);
						}
						else {
							// log the error for debugging purposes
							System.log("Action failed: " + action_result);
						}
					}
				}
			}
		}
	}
}