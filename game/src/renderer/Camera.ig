package renderer;

import org.iglang.layer0.**;


/*

	Initially the camera points straight down at the target.
	It is positioned "distance" units from the target.



	*/


public class Camera
{
	// focal point of the camera
	public var m_x :double = 0;
	public var m_y :double = 0;
	public var m_z :double = 0;

	public var m_has_target :bool = false;
	public var m_target_x :double = 0;
	public var m_target_y :double =0;
	public var m_target_z :double = 0;

	public var m_target_height :double = 1.0;

	// how far the camera is from the focal point
	public var m_distance    :double;

	//
	public var m_rot_x       :double = 0;
	public var m_rot_y       :double = 90 * Math.PI / 180.0;
	public var m_rot_z       :double = 0;

	//
	public var m_fov_degrees :double = 90;
	public var m_near        :double = 1;
	public var m_far         :double = 1000;

	//
	public var m_x_offset    :double = 0;
	public var m_z_offset    :double = 0;

	public var m_time        :double;
	public var m_trauma      :double;
	public static const TRAUMA_DAMPER :double  = 1.0;
	//public var m_noise : L0Noise = new L0Noise().init(44);

	private  var m_transform_inverse : L0Affine3D = new L0Affine3D();
	private var m_transform : L0Affine3D = new L0Affine3D();

	public function incTargetf(x :double, y:double, z:double) :void
	{
		m_target_x += x;
		m_target_y += y;
		m_target_z += z;
	}


	public function cut():void {
		m_has_target = true;
		m_x = m_target_x;
		m_y = m_target_y;
		m_z = m_target_z;
		
	}

	public function setTargetf(x :double, y:double, z:double) : void
	{

		m_target_x = x;
		m_target_y = y;
		m_target_z = z;
		
		if (!m_has_target) {
			cut();
		}
	}

	public function update(dt :double) : void {


		//trace("camera angle: " + m_rot_x + ":" + m_rot_y + ":" + m_rot_z);
		//trace("camera dt: " + dt);

		m_time += dt;
		m_trauma = m_trauma - 0.10;	//m_trauma * (1.0 - dt * TRAUMA_DAMPER);
		if (m_trauma < 0.001) {
			//m_trauma --;
			m_trauma = 0;
		}

	
		


		var MAX_SPEED :double = 2;
		var max_dist :double = dt * MAX_SPEED;
		if (max_dist == 0) {
			max_dist = 0.00001;
		}
		var dx :double = m_target_x - m_x;
		var dy :double = m_target_y - m_y;
		var dz :double = m_target_z - m_z;

		//trace ("" + dx + " " + dy + " " + dz );

//		trace ("" + m_x + " " + m_y + " " + m_z );

//		trace ("" + m_target_x + " " + m_target_y + " " + m_target_z );
		var dist :double = Math.length3(dx,dy,dz);
		if (dist <= max_dist) {
			m_x = m_target_x;
			m_y = m_target_y;
			m_z = m_target_z;
		}
		else {


			dx = (dx * 2) * dt;// * MAX_SPEED;
			dy = (dy * 2) * dt;// * MAX_SPEED;
			dz = (dz * 2) * dt;// * MAX_SPEED;
			
			//trace ("" + dx + " " + dy + " " + dz + " " + dist);
			m_x += dx; //m_target_x;
			m_y += dy;// m_target_y;
			m_z += dz;// m_target_z;


		//	m_x +=
		//	m_y += (dy / dist) * dt * MAX_SPEED;
		//	m_z += (dz / dist) * dt * MAX_SPEED;		
		}


		calcTransforms();
	}
	/**
	 * Initialize a standard 2d camera where the screen coordinatess
	 */

	public static const ANCHOR_TOP_LEFT :int = 0;
	public static const ANCHOR_CENTER_CENTER   :int = 1;

	public function initAs2d(
		screen_width :double, screen_height :double, mode: int) : Camera
	{
		
		m_x = 0;
		m_y = 0;
		m_z = 0;

		if (mode == ANCHOR_TOP_LEFT) {
			m_x_offset = screen_width/2;
			m_z_offset = screen_height/2;
		}

		m_rot_x = 0;
		m_rot_y = 0;
		m_rot_z = 0;

		m_fov_degrees = 90;

		// if we're using 45 deg as the fov
		m_distance = screen_height/2;

		calcTransforms();

		return this;
	}

	public function makeRelative(a : L0Event) : void 
	{
		a.m_relative_x = a.screen_x - m_x_offset - m_x;
		a.m_relative_y = 0;
		a.m_relative_z = a.screen_y - m_z_offset - m_z;
	}

	public function impulse(amt :double): void
	{
		m_trauma += amt;
	}


	public function concatRotation(m :L0Affine3D) : void
	{
		m.concatEuler(
				m_rot_x, 
				m_rot_y, 
				m_rot_z);
		
	}

	public function concatShake(m :L0Affine3D) : void
	{
		m.concatEuler(-Math.PI/2, 0, 0);
	/*	
		var shake :double = m_trauma * m_trauma;
		var time  :double = m_time;
		var m_noise_scale_rx :double = 0.1;
		var m_noise_scale_ry : double = 0.1;
		var m_noise_scale_rz :double = 0.0;

		//
		var shake_rx :double = shake * m_noise_scale_rx * m_noise.simplex2(time * 5 , 0);   
		var shake_ry :double = shake * m_noise_scale_ry * m_noise.simplex2(time * 5 + 0.5, 0.33);
		var shake_rz :double = shake * m_noise_scale_rz * m_noise.simplex2(time * 5 + 1.0, 0.66);

		m.concatEuler(
				 - Math.PI/2 + 
				 shake_rx, 
				shake_ry, 
				 shake_rz);
	*/	
	}


	public function screenPositionToOriginRay(x :double, y:double,
		origin :L0Vec3, ray :L0Vec3) : void 
	{

		//trace ("#######################################");
		//trace ("screen pos: " + x + "," + y);
		// input mouse coords will always be in the resolution of the screen
		var nx :double = (x / L0.width) * 2.0 - 1.0;
		var ny :double = 1.0 - (y / L0.height) * 2.0;
		
		//trace ("normalized " + nx + " " + ny);


		// figure out the width to height ratio
		var ratio :double = double(L0.width) / L0.height;

		// basically we'd want to figure out the position
		// relative to the near plane whi

		/////////////////////////////////////
		// tan THETA = opposite / near
		// tan THETA * near  = opposite;

		var yradius :double = Math.tan((m_fov_degrees / 2) * (Math.PI / 180)) * m_near;
		var xradius :double = yradius * ratio;

		var near_y :double = ny * yradius;
		var near_x :double = nx * xradius;
		var near_z :double = -m_near;

		ray.init(0,0,0);
		origin.init(near_x,near_y,near_z);

		var tra :L0Affine3D = m_transform;

		tra.mulVec3(origin, origin);
		tra.mulVec3(ray,    ray);

		//trace ("plane: " + origin);
		//trace ("ZERO POINT: " + ray);
		ray.x = origin.x - ray.x;
		ray.y = origin.y - ray.y;
		ray.z = origin.z - ray.z;
		ray.normalize();
	}

	///////////////////////////////////////////////////////////
	// Access the view transform (does not contain projection)
	///////////////////////////////////////////////////////////

	public function bind() :void {
		L0.projectionSetNearFar(m_near, m_far);
		L0.perspectiveSetAspectAndFov(L0.width / double(L0.height), m_fov_degrees * Math.PI/180.0);
		L0.setViewAffine3D(m_transform);
	}



	public function calcTransforms() : void {

		var m : L0Affine3D = m_transform;
		m.init().
			concatTranslation(
				(m_x,m_y,m_z) + (m_x_offset,m_target_height,m_z_offset)
				);

		this.concatRotation(m);
		m.concatTranslation(0,m_distance,0);

		// is the rotation being applied too late??
		// should there be a second rottion involved?
		this.concatShake(m);

		m_transform_inverse.initFromAffine3D(m);
		m_transform_inverse.invert();
	}


	public function get transform_inverse() :L0Affine3D {
		return m_transform_inverse;
	}

	public function get transform() : L0Affine3D {
		return m_transform;
	}

/*
	public function applyAudioOrientation() : void 
	{
		var pos     : 3 double = (m_transform.m03, m_transform.m13, m_transform.m23);
		var forward : 3 double = (m_transform.m02, m_transform.m12, m_transform.m22);
		var up      : 3 double = (m_transform.m01, m_transform.m11, m_transform.m21);

		L0.audioListenerSetOrientation(
			pos, forward * vec3.maginv(forward), up * vec3.maginv(up));
	}
*/

	
}