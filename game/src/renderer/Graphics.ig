package renderer;

import org.iglang.layer0.**;


public class Graphics
{
	private var m_white :L0Image;

	private var m_stack :Vector<GraphicsState> = new Vector<GraphicsState>();
	private var m_state :GraphicsState         = null;
	private var m_index :int                   = 0;

	private var m_last_x :double = 0;
	private var m_last_y :double = 0;
	private var m_last_down :bool = false;

	// Vertex cache
	private var m_mesh             :L0Mesh;
	private var m_image            :L0Image;
	private var m_max_vertex_count :int = 100;
	private var m_vertex_data      :Array<float> = new Array<float>(m_max_vertex_count * 9);
	private var m_vertex_count     :int = 0;

	private var m_text_width_cache :Map<String,double> = new Map<String,double>();


	private var m_sdf_shader :L0Shader;


	public function get loaded() :bool {
		return m_white.loaded && m_sdf_shader.loaded;
	}

	public function constructor() {
		m_white      = L0Image.load("/white.png");
		m_mesh       = L0.meshCreate("3f:a_pos,2f:a_uv,4f:a_color", 2);
		m_sdf_shader = L0Shader.load("/sdf.text.shader");

		for (var i :int = 0; i < 16; i++) {
			m_stack[i] = new GraphicsState();
		}
		m_state = m_stack[0];
		
	}


	/**
	 * Clear out any lingering state at the start of a tick
	 **/

	public function clear() :void {
		m_state.init();
		m_index        = 0;
		m_vertex_count = 0;
		m_image        = L0Image.NULL;
		//L0.shaderBind(m_sdf_shader);
		L0.shaderBind(L0Shader.DEFAULT);
	}

	/**
	 * Flush out any lingering draw calls
	 **/

	public function flush() :void {
		if (m_vertex_count > 0) {

			m_mesh.setVerticiesFromFloatArray(m_vertex_data, m_vertex_count);
			L0.setAffine3D(L0Affine3D.identity);
			L0.meshDraw1(m_mesh, m_image);
		}

		m_vertex_count = 0;
		m_image = L0Image.NULL;
	}

	// okay so we need to do this
	public function setTint(r :double, g:double, b:double, a:double) :void{
		m_state.r = r*a;
		m_state.g = g*a;
		m_state.b = b*a;
		m_state.a = a;
	}

	public function translate(x :double, z:double) :void {
		m_state.affine3d.concatTranslation(x,0,z);
	}

	public function translate3d(pos : 3 double) :void {
		m_state.affine3d.concatTranslation(pos);
	}

	public function scale(x :double, z:double) :void {
		m_state.affine3d.concatScale(x,0,z);
	}


	public function _drawImage(
		img :L0Image, 

		p :2 double, w:double, h :double, 
		origin : 2 double, reach : 2 double) : void 
	{
		if (m_image != img  ||
			m_vertex_count + 6 > m_max_vertex_count) {
			flush();
		}
		//const pattern = [0,3,1,0,2,3];  //[0,1,3,0,3,2];



		var pattern :int        = 0x320130;
		var m :L0Affine3D       = m_state.affine3d;
		var v_out :Array<float> = m_vertex_data;


		// find the starting index for output
		var o :int = (m_vertex_count) * 9;
		

		for (var j :int  = 0; j < 6; j++) 
		{

			//var i  = pattern[j];
			var xo :int = (pattern & 1);
			var yo :int = (pattern & 2) #>> 1;
			pattern = pattern #>> 4;

			var mw :double = xo * w + p_0;
			var mh :double = yo * h + p_1;

			// 3d position
			v_out[o + 0] = m.m00 * mw + m.m02 * mh + m.m03;
			v_out[o + 1] = m.m10 * mw + m.m12 * mh + m.m13;
			v_out[o + 2] = m.m20 * mw + m.m22 * mh + m.m23;

			// u,v
			v_out[o + 3] = origin_0 + reach_0 * xo;
			v_out[o + 4] = origin_1 + reach_1 * yo;

			// tint
			v_out[o + 5] = m_state.r;
			v_out[o + 6] = m_state.g;
			v_out[o + 7] = m_state.b;
			v_out[o + 8] = m_state.a;

			o += 9;
		}

		m_vertex_count += 6;
		m_image = img;
	}


	public function fillRect(w:double, h:double) :void {
		_drawImage(m_white, 0,0,w, h,0,0,1,1);
	}

	public function drawImage(img :L0Image, sz : 2 double) :void {
		_drawImage(img,  0,0, sz, 0,0, 1,1);
	}

	public function drawUniformImageRegion(img: L0Image, sz : 2 double, origin : 2 double, reach : 2 double) : void {
		_drawImage(img, 0,0,sz, origin, reach);
	}


	public function setFont(name :String, sz:double) :void
	{
		m_state.font       = name;
		m_state.font_size  = sz;
		m_state.font_sdf   = SDFFont.UNDEFINED;
	}

	public function setSDFFont(name :String, sz :double) :void {
		m_state.font      = null;
		m_state.font_size = sz;
		m_state.font_sdf  = SDFFontManager.findFont(name, false);
	}

	public function getSDFGlyphsLoaded(name :String, txt :String) : bool {

		if (!m_sdf_shader.loaded) {
			return false;
		}
		
		var fnt :SDFFont = SDFFontManager.findFont(name, false);
		if (SDFFont.UNDEFINED == fnt) {
			return false;
		}

		for (var j :int in 0 => txt.length) 
		{
			var glyph :SDFGlyph = SDFFontManager.findGlyph(fnt, txt[j]);
			if (glyph == SDFGlyph.UNDEFINED) {
				return false;
			}
		}

		return true;
	}
	
	public function getTextWidth(txt :String) :double {

		var width :double = 0;
		if (m_state.font_sdf == SDFFont.UNDEFINED)
		{
			// TODO make this work with SDF fonts
			width = m_text_width_cache.getWithDefault(txt, -1);
			if (width == -1) {
				width = L0.fontWidth(m_state.font, m_state.font_size, txt);
				m_text_width_cache[txt] = width;
			}
		}
		else {
			for (var j :int in 0 => txt.length) 
			{
				var glyph :SDFGlyph = SDFFontManager.findGlyph(m_state.font_sdf, txt[j]);
				if (glyph == SDFGlyph.UNDEFINED) {
					continue;
				}

				var sz  :double  = m_state.font_size / glyph.font_size;
				width += glyph.advance * sz;
			}
		}
		return width; 
	}


	public function drawText(txt :String) : void 
	{
		if (m_state.font_sdf == SDFFont.UNDEFINED)
		{
			var img :L0Image = L0.imageCreateFromText(m_state.font, m_state.font_size, txt);
			_drawImage(img, 0,0, img.width, img.height,0,0,1,1);
		}
		else 
		{
			flush();
			L0.shaderBind(m_sdf_shader);

			push();
			var baseline :2 double = (0.0,0.0);

			for (var j :int in 0 => txt.length) 
			{
				var glyph :SDFGlyph = SDFFontManager.findGlyph(m_state.font_sdf, txt[j]);
				if (glyph == SDFGlyph.UNDEFINED) {
					// what should be the default font size??
					//baseline_0 += m_state.font_size;
					continue;
				}

				var sz  :double  = m_state.font_size / glyph.font_size;
				var img :L0Image = glyph.image;
				var wi  :double  = 1.0 / img.width;
				var hi  :double  = 1.0 / img.height;


				_drawImage(
					glyph.image, 
					// where to render it
					(glyph.x, glyph.y) * sz + baseline,
					(glyph.w, glyph.h) * sz, 
					// normalized texture coordinates
					glyph.ax*wi , glyph.ay*hi, 
					glyph.w*wi , glyph.h*hi);

				baseline_0 += glyph.advance * sz;
			}

			pop();

			flush();
			L0.shaderBind(L0Shader.DEFAULT);
		}
	}

	public function push() : void {
		m_index++;
		m_state = m_stack[m_index].initFromGraphicsState(m_stack[m_index - 1]);
	}

	public function pop() : void
	{
		m_index--;
		m_state = m_stack[m_index];
	}

	public function panelStart(x :double, y:double, w:double, h:double) :void {
		push();
		translate(x,y);
	}

	public function panelEnd() :void {
		pop();
	}

	public function button(id :int, x :double, y:double, w :double, h:double) :void {

	}
}