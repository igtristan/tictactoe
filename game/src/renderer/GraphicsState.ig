package renderer;

import org.iglang.layer0.**;

public class GraphicsState
{
	// font data
	public var font :String = "";
	public var font_size :double = 12;
	public var font_sdf : SDFFont; 
	
	// transformation
	public var affine3d :L0Affine3D = new L0Affine3D().init();
	
	// color
	public var r:double = 1; 
	public var g:double = 1;
	public var b:double = 1;
	public var a:double = 1;
	


	public function init() :void {
		font       = "";
		font_size  = 12;
		font_sdf   = SDFFont.UNDEFINED;
		affine3d.init();
		r          = 1; 
		g          = 1;
		b          = 1;
		a          = 1;
	}

	public function initFromGraphicsState(other :GraphicsState) : GraphicsState
	{
		this.font      = other.font;
		this.font_size = other.font_size;
		this.font_sdf  = other.font_sdf;
		this.affine3d.initFromAffine3D(other.affine3d);
		this.r         = other.r;
		this.g         = other.g;
		this.b         = other.b;
		this.a         = other.a;
		
		return this;
	}
}
