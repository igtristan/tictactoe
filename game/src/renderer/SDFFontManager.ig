package renderer;

import org.iglang.layer0.**;

public class SDFFontManager
{
	internal static const DATA_WIDTH   :int = 0;
	internal static const DATA_HEIGHT  :int = 1;
	internal static const DATA_X       :int = 2;
	internal static const DATA_Y       :int = 3;
	internal static const DATA_ATLAS_X :int = 4;
	internal static const DATA_ATLAS_Y :int = 5;
	internal static const DATA_IMAGE   :int = 6;
	internal static const DATA_RENDERER_SZ :int = 7;
	internal static const DATA_ADVANCE :int = 8;

	internal static var s_glyph_data : Vector<float> = new Vector<float>();
	private static var s_font_idx :int = 0;
	private static var s_font_map : Map<String,int> = new Map<String,int>();


	internal static const INDEX_CHARACTER :int = 0;
	internal static const INDEX_FONT :int = 1;
	internal static const INDEX_OFFSET :int = 2;
	
	private  static var s_glyph_index :Vector<int> = new Vector<int>();
	internal static var s_images      :Vector<L0Image> = new Vector<L0Image>();
	internal static var s_meta_load   :Map<L0HttpRequest, L0Image> = new Map<L0HttpRequest,L0Image>();

	public static function load(meta :String, image :String) :void 
	{
	//	System.log("Loading font: " + meta + " img: " + image);

		// how are we going to do this shit
		var rid :L0HttpRequest = 
			L0.httpGet(meta, -> static function(req:L0HttpRequest, status :int, ba :ByteArray):void 
		{
	//		System.log("starting work");
			var image_id :int = s_images.length;
			s_images.push(s_meta_load[req]);

			ba.endian = Endian.LITTLE_ENDIAN;


			// read the header
			var header      :int = ba.readInt();
			var version     :int = ba.readInt();
			var font_count  :int = ba.readInt();
			var glyph_count :int = ba.readInt();



			if (version != 1) {
				throw new Error("Invalid version: " + version + " header: " + header);
			}



			var  font_map : Array<SDFFont> = new Array<SDFFont>(font_count);
			for (var i:int in 0 => font_count) {
				var f :String = ba.readUTF();
				font_map[i] = findFont(f, true);
			}

			for (var i :int in 0 => glyph_count) 
			{	
				var c :int      = ba.readInt();
				var f :int      = ba.readInt();
				var w :double   = ba.readFloat();
				var h :double   = ba.readFloat();
				var x :double   = ba.readFloat();
				var y :double   = ba.readFloat();
				var ax :double  = ba.readFloat();
				var ay :double  = ba.readFloat();
				var sz :double  = ba.readFloat();
				var adv :double = ba.readFloat();

				injectGlyph(image_id, font_map[f], c,
					w, h, x, y, ax, ay, sz,adv);
			}
		});


		var img :L0Image = L0Image.load(image);
		s_meta_load[rid] = img;
	}

	public static function findFont(font :String, create :bool) : SDFFont
	{
		var font_idx :int = s_font_map.getWithDefault(font, -1);
		if (font_idx == -1) 
		{
			if (!create) {
				return SDFFont.UNDEFINED;
			}

			//System.log("created font: " + font + " @" + (s_font_idx + 1));

			s_font_idx++;
			s_font_map[font] = s_font_idx;
			font_idx = s_font_idx;
		}
		return SDFFont(font_idx);
	}

	/**
	 * Inject a glyph into our internal table
	 **/
	private static function injectGlyph(
		 image :int, font :SDFFont, char : int,
		 w  :double, h  :double, 
		 x  :double, y  :double, 
		 ax :double, ay :double,
		 sz :double, advance :double) : void 
	{
		var gd : Vector<float> = s_glyph_data;
		var gd_len :int = gd.length;

		s_glyph_index.push(char);
		s_glyph_index.push(int(font));
		s_glyph_index.push(gd_len);

		//System.log("created glyph: " + char + " " + int(font) + " " + gd_len + " " + image + 
		//	" (" + ax+"," +ay+" " +w+"x"+h);
		// write in reverse to guarantee capacity

		gd[gd_len + DATA_ADVANCE] = advance;
		gd[gd_len + DATA_RENDERER_SZ] = sz;
		gd[gd_len + DATA_IMAGE]   = image;
		gd[gd_len + DATA_ATLAS_Y] = ay;
		gd[gd_len + DATA_ATLAS_X] = ax;
		gd[gd_len + DATA_Y]       = y;
		gd[gd_len + DATA_X]       = x;
		gd[gd_len + DATA_HEIGHT]  = h;
		gd[gd_len + DATA_WIDTH]   = w;

	}


	/**
 	 * Find a glyph in a specified font
     */

	public static function findGlyph(font :SDFFont, char :int) :SDFGlyph 
	{
		if (font == SDFFont.UNDEFINED) {
			return SDFGlyph.UNDEFINED;
		}

		var len :int = s_glyph_index.length;
		for (var i :int = 0; i < len; i += 3)
		{
			if (s_glyph_index[i+INDEX_CHARACTER] != char) {
				continue;
			}

			if (s_glyph_index[i+INDEX_FONT] != int(font)) {
				continue;
			}

			return SDFGlyph(s_glyph_index[i+INDEX_OFFSET]);
		}

		return SDFGlyph.UNDEFINED;
	}


	/**
	 * Find a glyph in a named font
	 */

	public static function findFontGlyph(font :String, char :int) :SDFGlyph {

		var font_idx :SDFFont = findFont(font, false);
		if (font_idx == SDFFont.UNDEFINED) {
			return SDFGlyph.UNDEFINED;
		}

		return findGlyph(font_idx, char);
	}	
}