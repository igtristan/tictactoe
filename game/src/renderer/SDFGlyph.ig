package renderer;


import org.iglang.layer0.**;

public enum SDFGlyph
{
	UNDEFINED = -1;
	
	public function get width() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_WIDTH];
	}

	public function get height() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_HEIGHT];
	}

	public function get w() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_WIDTH];
	}

	public function get h() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_HEIGHT];
	}

	public function get x() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_X];
	}

	public function get y() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_Y];
	}

	public function get advance() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_ADVANCE];
	}

	public function get ax() :int {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_ATLAS_X];
	}

	public function get ay() :int {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_ATLAS_Y];
	}

	public function get image() :L0Image {
		var idx :int =  SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_IMAGE];
		return SDFFontManager.s_images[idx];
	}


	public function get font_size() :double {
		return SDFFontManager.s_glyph_data[int(this) + SDFFontManager.DATA_RENDERER_SZ];
	}

	public function getRenderedHeight(size : double) : double {
		var sz : double = size / this.font_size;
		return (y + this.h) * sz;
	}

	public function getRenderedWidth(size : double) : double {
		var sz : double = size / this.font_size;
		return this.w * sz;
	}

}
