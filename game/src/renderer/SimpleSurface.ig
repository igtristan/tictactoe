package renderer;

import org.iglang.layer0.**;

/**
Simple surface with an RGBA attachment and a depth renderbuffer
**/

public class SimpleSurface
{

	private var m_surface_color :L0Image;
	private var m_surface_color2 :L0Image;
	private var m_surface_depth :L0Image;
	private var m_surface :L0Surface;

	public function init(w :int, h:int) :SimpleSurface
	{
		m_surface_depth = L0.imageCreate(w, h, L0ImageFormat.RENDERBUFFER_DEPTH);
		m_surface_color = L0.imageCreate(w, h, L0ImageFormat.RGBA16F);
		m_surface       = L0.surfaceCreate1(m_surface_color,m_surface_depth);
		return this;
	}

	public function initWithImageFormat(w :int, h:int, color_format : L0ImageFormat) :SimpleSurface
	{
		m_surface_depth = L0.imageCreate(w, h, L0ImageFormat.RENDERBUFFER_DEPTH);
		m_surface_color = L0.imageCreate(w, h, color_format);
		m_surface       = L0.surfaceCreate1(m_surface_color,m_surface_depth);
		return this;
	}

	public function initWithImageFormat2(w :int, h:int, 
		color_format : L0ImageFormat,
		color_format2 : L0ImageFormat) :SimpleSurface
	{
		m_surface_depth  = L0.imageCreate(w, h, L0ImageFormat.RENDERBUFFER_DEPTH);
		m_surface_color  = L0.imageCreate(w, h, color_format);
		m_surface_color2 = L0.imageCreate(w, h, color_format2);
		m_surface        = L0.surfaceCreate2(m_surface_color,m_surface_color2,m_surface_depth);
		return this;
	}


	public function setClearColor(r :double, g :double, b :double, a :double) :void {
		L0.surfaceSetClearColor(m_surface, r,g,b,a);
	}

	public function bindAndClear() :void {
		m_surface.bind();
		m_surface.clear();
	}



	public function get color_texture() :L0Image {
		return m_surface_color;
	}

	public function get color_texture2() :L0Image {
		return m_surface_color2;
	}

	public function get depth_texture() :L0Image {
		return m_surface_depth;
	}
}