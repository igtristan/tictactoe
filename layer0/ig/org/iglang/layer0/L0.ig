package org.iglang.layer0;

import org.iglang.layer0.**;

public class L0 extends Object
{
  public static native function getDebugI(name :String) :int;

  private static native function graphicsStateClear(): void;
  private static native function graphicsStateSet(key :L0GraphicsState, v :bool) : void;

  public static native function getConfig(key :String, defaux :String):String;
  public static native function exit(code :int): void;
  public static native function get width()  :int;
  public static native function get height() :int;
  public static native function nameToId(name :String) :L0Name;
  public static native function eventPoll   (event :L0Event) :bool;

  private static var s_req_map :
        Map<L0HttpRequest,Function<(L0HttpRequest,int,ByteArray):void>> = 
    new Map<L0HttpRequest,Function<(L0HttpRequest,int,ByteArray):void>>();

  public static native function time(): double;


  public static const HMAC_SHA256 :int = 1;

  public static native function hmac(type :int, value :String, key :String) : String;


  ////////////////////////////////////////////////////
  // Socket Group
  // - this should only be used via the interfaces in L0Socket
  ////////////////////////////////////////////////////


  // ie.  ws://localhost:8080/w
  //      tcp://192.178.0.1:50
  internal static native function socketImpConnect
    (url :String, listener :Object, is_server :bool):L0Socket;
  

  internal static native function socketImpOp
    (s :L0Socket, op :int, ba :ByteArray, start :int, len:int):int;


  ////////////////////////////////////////////////////
  // HTTP Request Group
  ////////////////////////////////////////////////////


  //public static native function assetLoad(path :String, delegate : Function<(int,String,ByteArray)) :void;
  

  public static function httpGet(query_string :String, 
    delegate :Function<(L0HttpRequest, int,ByteArray):void>): L0HttpRequest
  {
    var qid :L0HttpRequest = httpRequest(query_string, null, null, 0,0);
    s_req_map[qid] = delegate;
    return qid;
  }

  private static native function httpRequest(
    query_string :String,
    post_mime    :String,
    post_data    :ByteArray,
    post_start   :int,
    post_length  :int) :L0HttpRequest;

  public static native function httpRequestGetQueryString (rid : L0HttpRequest) :String;

  public static function httpResponse(qid : L0HttpRequest, status :int, ba :ByteArray) :void {
    if (!s_req_map.containsKey(qid)) {
      return;
    }
    s_req_map[qid](qid, status, ba);
    s_req_map.deleteKey(qid);
  }

  public static function httpCancel(qid :L0HttpRequest) :void
  {
    // might want to actually call a cancel
    // should we even retrigger the callback?
    s_req_map.deleteKey(qid);
  }

  //////////////////////////////////////////////////////////////////////
  // Audio Group
  //////////////////////////////////////////////////////////////////////

  public static native function audioDataLoad(path:String) :L0Audio;
  public static native function audioDataLoaded(s :L0Audio): bool;
  
  // this is a look at for audio
  public static native function audioListenerSetOrientation(
    pos : 3 double,
    fx :double, fy :double, fz:double,
    ux :double, uy :double, uz :double):void;

  public static native function audioSourceCreate(bus_id :int, positional :bool, p : 3 double):L0AudioSource;
  public static native function audioBusSetVolume(bus_id :int, volume : double) :void;
  public static native function audioSourceDestroy    (s :L0AudioSource):void;
  public static native function audioSourceSetPosition(a :L0AudioSource, p : 3 double):void;
  public static native function audioSourceSetVelocity(a :L0AudioSource, p : 3 double):void;
  public static native function audioSourcePlay       (a :L0AudioSource, s:L0Audio, vol :double) :void;



  //////////////////////////////////////////////////////////////////////
  // Camera group
  //////////////////////////////////////////////////////////////////////

  public static native function projectionSetNearFar(n :double, f :double) :void;
  public static native function perspectiveSetAspectAndFov(a :double, f :double):void;
  public static native function orthoSetBounds(l:double,r:double,b:double,t:double):void;

  //////////////////////////////////////////////////////////////////////
  // Image group
  //////////////////////////////////////////////////////////////////////

  public static native function imageLoad   (path  :String)  :L0Image;
  public static native function imageLoaded (img   :L0Image) :bool;
  public static native function imageHeight (img   :L0Image) :int;
  public static native function imageWidth  (img   :L0Image) :int;
 // public static native function imageDraw   (img   :L0Image, w :double, h :double) :int;
  public static native function imageCreate (w :int, h:int, format :L0ImageFormat):L0Image;
  public static native function imageSetSpanFromFloatArray(img :L0Image, layer :int, x0 :int, y0:int, 
     pixel_count :int, data :Array<float>, data_offset :int) :void;
  /////////////////////////////////////////////////////////////////////
  // Surface group
  /////////////////////////////////////////////////////////////////////

  public static function surfaceCreate1(color :L0Image, depth :L0Image) :L0Surface {
    return surfaceCreate(new Array<L0Image>(1)[color], depth);
  }
    public static function surfaceCreate2(color :L0Image, color2 : L0Image, depth :L0Image) :L0Surface {
    return surfaceCreate(new Array<L0Image>(2)[color, color2], depth);
  }
  public static native function surfaceCreate(
      color_attachements :Array<L0Image>, 
      depth_atttacment :L0Image): L0Surface;
  public static native function surfaceSetClearColor(s :L0Surface, r :double, g :double, b :double, a :double) :void;
  public static native function surfaceWidth(s :L0Surface):int;
  public static native function surfaceHeight(s :L0Surface):int;
  public static native function surfaceGetImage(s :L0Surface, idx :int): L0Image;
  public static native function surfaceBind(s:L0Surface):void;
  public static native function surfaceClear(s:L0Surface):void;
  /////////////////////////////////////////////////////////////////////
  // Shader group
  /////////////////////////////////////////////////////////////////////

  public static native function shaderLoad (path :String) :L0Shader;
  public static native function shaderLoaded(shader :L0Shader): bool;
  public static native function shaderBind(shader :L0Shader):void;
 
  public static native function setGlobalUniform(name :L0Name, double_count :int, v0 :double, v1 :double, v2 :double, v3 :double):void;
  public static native function setGlobalImage  (name :L0Name, image_id :L0Image):void;
  public static native function setGlobalAffine3D  (name :L0Name, aff : L0Affine3D):void;


  /////////////////////////////////////////////////////////////////////
  // Mesh group
  /////////////////////////////////////////////////////////////////////

  public static native function meshLoad      (path  :String)  :L0Mesh;
  public static native function meshLoadFrom  (data :ByteArray) :L0Mesh;
  public static native function meshLoaded    (img   :L0Mesh) :bool;
  

  private static var s_mesh_draw :Array<L0Image> = new Array<L0Image>(8);

  public static  function meshDraw1     (mesh :L0Mesh, i0 :L0Image, mode :int = 3): void {
    s_mesh_draw[0] = i0;
    meshDrawN(mesh, s_mesh_draw, 1, mode);
  }
  public static  function meshDraw2     (mesh :L0Mesh, i0 :L0Image, i1 :L0Image, mode :int = 3): void
  {
    s_mesh_draw[0] = i0;
    s_mesh_draw[1] = i1;
    meshDrawN(mesh, s_mesh_draw, 2, mode);
  }
  public static  function meshDraw3     (mesh :L0Mesh, i0 :L0Image, i1 :L0Image, i2 :L0Image, mode :int = 3): void
  {
    s_mesh_draw[0] = i0;
    s_mesh_draw[1] = i1;
    s_mesh_draw[2] = i2;
    meshDrawN(mesh, s_mesh_draw, 3, mode);
  }

  public static native function meshDrawN(mesh :L0Mesh, arr : Array<L0Image>, arr_cnt :int, mode :int):void;
  public static native function meshCreate    (format_string :String, update_freq :int) :L0Mesh;
  public static native function meshDestroy   (mesh :L0Mesh) :void;
  public static native function meshClear     (mesh :L0Mesh) :void;
  public static native function meshSetVerticiesFromFloatArray(mesh :L0Mesh, data : Array<float>, vertex_count :int) : void;
  public static native function meshAddVerticiesFromMesh(mesh :L0Mesh, src :L0Mesh, affine :L0Affine3D) :void;

  ///////////////////////////////////////////////////////////////////////
  // Font group

  public static native function imageCreateFromText(family :String, sz:int, text:String) :L0Image;
  public static native function fontWidth          (family :String, sz:int, text:String):double;
  /**
   * Set the view matrix
   */

  public static native function setViewAffine3D(a : L0Affine3D):void;

  /**
   * Set the Y plane that 2d objects should be rendered on
   */
  //public static native function setAffine2DYPlane(y :double):void;

  /**
   * Set the XZ affine transformation for 2d rendering
   */
  //public static native function setAffine2Df(
  //  m01 :double, m02 :double, m03 :double,
  //	m11 :double, m12 :double, m13 :double
  //  ) : void;

  /**
   * Set the RGBA tint to be used for rendering
   */  

  public static native function setAffine3D(v :L0Affine3D): void;

  // used by some rendering
  //public static native function setTint(r :double, g :double, b :double, a:double):void;
}