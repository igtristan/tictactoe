package org.iglang.layer0;

public interface L0App
{
	public function init(origin :double): void;
	public function update(time :double, time_inc :double): void;
	public function render(time :double, time_inc :double) : void;
}