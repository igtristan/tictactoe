package org.iglang.layer0;

import org.iglang.layer0.**;

public class L0AssetList
{
	private var m_images_pending :int;
	private var m_images : Vector<L0Image> = new Vector<L0Image>();

	private var m_shaders_pending :int;
	private var m_shaders :Vector<L0Shader> = new Vector<L0Shader>();

	private var m_audio_pending :int;
	private var m_audio :Vector<L0Audio> = new Vector<L0Audio>();

	private var m_meshes_pending :int;
	private var m_meshes :Vector<L0Mesh> = new Vector<L0Mesh>();


	public function get loaded() : bool
	{
		for (var i :int in m_images_pending => m_images.length) {
			if (m_images[i].loaded) {
				m_images_pending++;
			}
			else {
				return false;
			}
		}
	


		for (var i :int in m_shaders_pending => m_shaders.length) {
			if (m_shaders[i].loaded) {
				m_shaders_pending++;
			}
			else {
				return false;
			}
		}


		for (var i :int in m_audio_pending => m_audio.length) {
			if (m_audio[i].loaded) {
				m_audio_pending++;
			}
			else {
				return false;
			}
		}


		for (var i :int in m_meshes_pending => m_meshes.length) {
			if (m_meshes[i].loaded) {
				m_meshes_pending++;
			}
			else {
				return false;
			}
		}

		return true;
	}


	public function addImage(img : L0Image) {
		if (img == L0Image.NULL) {
			throw new Exception("Can't add null resource.");
		}
		m_images.push(img);
	}


	public function addShader(img : L0Shader) {
		if (img == L0Shader.NULL) {
			throw new Exception("Can't add null resource.");
		}
		m_shaders.push(img);
	}


	public function addAudio(img : L0Audio) {
		if (img == L0Audio.NULL) {
			throw new Exception("Can't add null resource.");
		}
		m_audio.push(img);
	}

	public function addMesh(img : L0Mesh) {
		if (img == L0Mesh.NULL) {
			throw new Exception("Can't add null resource.");
		}
		m_meshes.push(img);
	}
}