package org.iglang.layer0;

public class L0Event
{
  	public static const MOUSE_PRESSED  :int = 0x11;
  	public static const MOUSE_RELEASED :int = 0x10;
  	public static const MOUSE_MOVED    :int = 0x14;

  	public static const KEY_PRESSED  :int = 0x21;
  	public static const KEY_RELEASED :int = 0x20;

	public var m_type :int;
	public var m_x    :double;
	public var m_y    :double;	
	public var m_key_code :int;

	public var m_relative_x :double;
	public var m_relative_y :double;
	public var m_relative_z :double;

	public override function toString(): String {
		var t :String =  "L0Event[type:" + m_type + ",x:" + m_x + ",y:" + m_y + "]";
		return t;
	}

	// or should these just be renamed to world_{x,y,z} ??
	public function get relative_x() : double { return m_relative_x; }
	public function get relative_y() : double { return m_relative_y; }
	public function get relative_z() : double { return m_relative_z; }

	public function get screen_x() : double { return m_x; }
	public function get screen_y() :double {return m_y; }
	public function get type() :double { return m_type; }
	public function get key_code() : int { return m_key_code; }

	public function get is_key()   : bool { return 2 == (m_type #>> 4); }
	public function get is_mouse() : bool { return 1 == (m_type #>> 4); }
}