package org.iglang.layer0;


public enum L0GraphicsState extends int{ 
  // GRAPHICS STATES
  DEPTH_READ  = 1,
  DEPTH_WRITE  = 2,
  BLENDING = 4;
}