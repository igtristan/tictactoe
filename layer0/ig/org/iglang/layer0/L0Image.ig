package org.iglang.layer0;

public enum L0Image
{
	NULL = 0;

	public function get width() : int{
		return L0.imageWidth(this);
	}

	public function get height() :int {
		return L0.imageHeight(this);
	}

	public function get w() : int{
		return L0.imageWidth(this);
	}

	public function get h() :int {
		return L0.imageHeight(this);
	}

	public function get loaded() :bool {
		return L0.imageLoaded(this);
	}

	public static function load(path :String) :L0Image {
		return L0.imageLoad(path);
	}

	public function setSpanFromFloatArray( layer :int, x0 :int, y0:int, pixel_count :int, 
      data :Array<float>, data_offset :int) :void {
		L0.imageSetSpanFromFloatArray(this, layer, x0, y0, pixel_count, data, data_offset);
	}

}