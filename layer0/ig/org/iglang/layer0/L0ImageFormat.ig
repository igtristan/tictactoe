package org.iglang.layer0;

public enum L0ImageFormat
{
	RGBA8 = 0x14,
	RGB8  = 0x13,
	RGBA16F = 0x24,
	RGB16F  = 0x23,
	RGBA32F = 0x44,
	RGB32F  = 0x43,
	DEPTH = 0xe1,

	RENDERBUFFER_DEPTH = 0xf1;
}