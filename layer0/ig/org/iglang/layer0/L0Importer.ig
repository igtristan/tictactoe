package org.iglang.layer0;

public class L0Importer
{
	protected var meta :L0ImporterMeta = null;
	private var m_path :String;
	internal function exec(path :String, inz :ByteArray, _meta :ByteArray, out:ByteArray) :int {
		meta = new L0ImporterMeta(_meta);
		m_path = path;
		return this.process(inz, out);
	}

	public function getDependency(path :String) :ByteArray {
		if (path.startsWith("/")) {
			path = "./assets/" + path;
			return readDependency(m_path, path);		
		}
		var li :int = m_path.lastIndexOf("/");
		var full_path :String = m_path.substr(0, li + 1) + path;
		return readDependency(m_path, full_path);
	}

	public function resetDependencies() :void {
		clearDependencies(m_path);
	}

	public function process(inz :ByteArray, out :ByteArray) :int {
		throw new Error("process must be implemented for in importer");
	}

	// native implementation
	// this is only available server side with compilation enabled
	private static native function clearDependencies(path :String) :void;
	private static native function readDependency(dep_src :String, dep_path :String) : ByteArray;

	// this is available serverside for immediate reading of files
	public  static native  function readFile(path :String) :ByteArray;

}