package org.iglang.layer0;


// this needs to be more robust
public class L0ImporterMeta
{
	private var m_params :Map<String,String> = new Map<String,String>();
	
	public function constructor(ba :ByteArray) 
	{
		if (ba != null) {

			eatWhitespace(ba);
			while (ba.position < ba.length)
			{
				var k:String = readToken(ba);
				eatWhitespace(ba);
				ba.readByte();
				eatWhitespace(ba);
				var v :String = readToken(ba);
				eatWhitespace(ba);

				if (k.length == 0 ||
					v.length == 0) {
		//			trace ("Invalid key/value pair in meta");
		//			throw new Error("Invalid key/value pair in meta");
				}

		//		trace ("kv: " + k + " " + v);
		//		trace ("Before assign: " + m_params.length);
				m_params[k] = v;
		//		trace ("Post assing");
			}
		}
	}

	private static function eatWhitespace(ba :ByteArray) :void {
		var i :int = ba.peek();
		while (i == ' ' || i == '\t' || i == 12 || i == 13) {
			ba.readByte();
			i = ba.peek();
		}
	}

	private static function readToken(ba :ByteArray) :String {
		var i:int = 0;
		var c :int = ba.peek(i);
		while (c > ' ' && c != ':') {
			i ++;
			c= ba.peek(i);  // yeah this doesn't handle eof properly
		}
		return ba.readUTFBytes(i);
	}

	
	public function getDouble(k :String, d :double = 0.0) :double {


		//trace ("key: " + k);
		//trace ("defaux: " + d);
		//trace ("size: " + m_params.length);
		return double(m_params.getWithDefault(k, "" + d));
	}
}