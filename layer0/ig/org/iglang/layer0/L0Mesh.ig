package org.iglang.layer0;

public enum L0Mesh extends int
{
	NULL       = 0,
	FULLSCREEN = 1;

	public static function create(format_string :String, update_freq :int) :L0Mesh
	{
		return L0.meshCreate(format_string, update_freq);
	}
	
	public static function load(path :String) :L0Mesh {
		return L0.meshLoad(path);
	}

	public function get loaded() :bool {
		return L0.meshLoaded(this);
	}

	public function clear() : void{
		L0.meshClear(this);
	}

	public function setVerticiesFromFloatArray(fa : Array<float>, count :int) :void {
		L0.meshSetVerticiesFromFloatArray(this, fa, count);
	}
}