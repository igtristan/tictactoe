package org.iglang.layer0;

public class L0Pow2IntMap<T1>
{
	public var m_keys  :Array<int>;
	public var m_values :Array<T1>;
	public var m_count :int;
	public var m_mask  :int;
	public var m_prime :int;
	public var m_max_load :int;
	public var m_max_load_percentage :double;

	public function constructor(capacity :int = 16, max_load :double = 0.75)
	{	
		var shift :int = 0;
		while ((1 #<< shift) < capacity) {
			shift ++;
		}

		capacity = 1 #<< shift;

		m_keys = new Array<int>(capacity);
		m_values = new Array<T1>(capacity);

		m_prime = 13;
		m_mask  = ~(0xFFFFFFFE #<< shift);

		max_load = Math.min(1.0, max_load);
		m_max_load = capacity * max_load;
		m_max_load_percentage = max_load;
	}

	public function get[](key :int) : T1
	{
		var idx :int = (key * m_prime) & m_mask;
	
		var k :int;

		// do linear probing right now
		do
		{
			k = m_keys[idx];
			if (k == key) {
				return m_values[idx];
			}
			idx = (idx + 1) & m_mask;
		}
		while (k != 0);

		var nil:T1;
		return nil;
	}

	public function set[](key :int, value :T1):void
	{
		if (m_count+1 >= m_max_load) 
		{
			m_max_load *= 2;

			var old_keys :Array<int> = m_keys;
			var old_values :Array<T1> = m_values;

			m_mask = m_mask #<< 1;
			m_keys   = new Array<int>(m_keys.length * 2);
			m_values = new Array<T1> (m_keys.length);
		
			for (var i :int in 0 => old_keys.length) {
				var k:int = old_keys[i];
				if (0 == k) { continue; }
				var v:int = old_values[i];
				this[k] = v;
			}
		}

		var idx :int = (key * m_prime)  & m_mask;
		// check if the key already exists in the map
		{
			var k   :int = m_keys[idx];
			while (k != 0) {
				if (k == key) {
					m_values[idx] = value;
					return;
				}
				idx = (1 + idx) & m_mask;
			}
		}
		m_keys[idx] = key;
		m_values[idx] = value;
		m_count ++;
	}

	public function containsKey(key :int) :bool {

		var idx :int = (key * m_prime) & m_mask;
		while (m_keys[idx ] != 0) {
			if (m_keys[idx] == key) {
				return true;
			}
			idx = (idx + 1) & m_mask;
		}
		return false;
	}

	public function deleteKey(key :int) :void
	{

	}

	// fuck how to handle this case.... barf
	public function removeKey(key :int) :void
	{
		// eww complicated
	}
}