package org.iglang.layer0;

public enum L0Shader extends int
{
	NULL    = 0,
	DEFAULT = -1;
	
	public function get loaded() :bool {
		return L0.shaderLoaded(this);
	}

	public static function load(path :String) :L0Shader {
		return L0.shaderLoad(path);
	}

	public function bind() : void{
		L0.shaderBind(this);
	}
}