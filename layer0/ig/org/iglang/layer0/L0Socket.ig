package org.iglang.layer0;

public enum L0Socket extends int
{
	NULL       = 0;

	public static function connect(url :String, listener :L0SocketListener) :L0Socket
	{
 		return L0.socketImpConnect(url, Object(listener), false);
	}

	public static function listen(url :String, listener :L0SocketServerListener) :L0Socket
	{
 		return L0.socketImpConnect(url, Object(listener), true);
	}

	public function read(ba :ByteArray) :bool {
		return L0.socketImpOp (this, L0SocketOp.READ, ba, 0, 0) == 1;
	}

	public function write(ba :ByteArray, offset :int, length :int) :void{
		L0.socketImpOp (this, L0SocketOp.WRITE, ba, offset, length);
	}

	public function close() :void {	
		L0.socketImpOp (this, L0SocketOp.CLOSE, null, 0, 0);
	}
}