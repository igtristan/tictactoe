package org.iglang.layer0;

public enum L0SocketEventType
{
    UNDEFINED   = 0,
	CONNECTING  = 1,
    CONNECTED   = 2,
    ERROR       = 3,
    CLOSING     = 4,
    CLOSED      = 5,
    ACCEPTING   = 10,
    ACCEPT      = 11,
    MESSAGE     = 12;

    public function isClosedOrClosing() :bool {
    	return this == CLOSED || this == CLOSING;
    }
}