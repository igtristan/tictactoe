package org.iglang.layer0;

public interface L0SocketListener
{
	public function onSocketEvent  (sid :L0Socket, state :L0SocketEventType) :void;
}