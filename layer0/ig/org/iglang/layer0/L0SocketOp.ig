package org.iglang.layer0;

public enum L0SocketOp extends int
{
	UNDEFINED        = 0,
	READ             = 1,
	WRITE            = 2,
	CLOSE            = 3;
}