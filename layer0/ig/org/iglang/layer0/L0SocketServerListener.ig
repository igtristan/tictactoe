package org.iglang.layer0;

public interface L0SocketServerListener
{
	// how to deal with this correctly
	public function onSocketAccept(
		server_socket :L0Socket, 
		child_socket :L0Socket, 
		child_address :String, 
		child_referrer :String,
		child_data :String) :bool;
}