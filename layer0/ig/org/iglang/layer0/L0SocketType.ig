package org.iglang.layer0;

public enum L0SocketType extends int
{
	UNDEFINED       = 0,
	WEBSOCKET       = 1;
}