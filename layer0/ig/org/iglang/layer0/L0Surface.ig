package org.iglang.layer0;

public enum L0Surface extends int
{
	NULL    = 0,
	DEFAULT = -1;		// default full screen framebuffer

	public function get width() : int {
		return L0.surfaceWidth(this);
	}

	public function get height() :int {
		return L0.surfaceHeight(this);
	}

	public function getImage(idx :int): L0Image {
		return L0.surfaceGetImage(this, idx);
	}

	public function bind() : void {
		L0.surfaceBind(this); 
	}

	public function clear() : void {
		L0.surfaceClear(this); 
	}
}