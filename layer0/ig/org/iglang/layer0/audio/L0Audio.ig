package org.iglang.layer0.audio;

import org.iglang.layer0.L0;

public enum L0Audio
{
	NULL = 0;

	public static function load(url :String) : L0Audio {
		return L0.audioDataLoad(url);
	}

	public function get loaded() : bool {
		return L0.audioDataLoaded(this);
	}
}