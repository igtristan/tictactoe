package org.iglang.layer0.audio;


import org.iglang.layer0.L0;

public enum L0AudioSource
{
	NULL       = 0;


	public static function create(bus_id :int) : L0AudioSource {
		return L0.audioSourceCreate(bus_id, false, 0,0,0);
	}

	public static function createPositional(bus_id :int, p : 3 double) :L0AudioSource {
		return L0.audioSourceCreate(bus_id, true, p);
	}

	public function setPosition(p : 3 double) :void
	{
		L0.audioSourceSetPosition(this, p);
	}

	public function setVelocity(p : 3 double) :void
	{
		L0.audioSourceSetVelocity(this,p);
	}

	public function play(s :L0Audio, volume :double = 1.0) :void
	{
		L0.audioSourcePlay(this, s, volume);
	}

	public function destroy() :void {
		L0.audioSourceDestroy(this);
	}
}