package org.iglang.layer0.math;

// could use this as an example for the non trivial work
// http://glmatrix.net/docs/mat4.js.html#line23
public class L0Affine3D
{
	public var m00 :double = 1;
	public var m01 :double = 0;
	public var m02 :double = 0;
	public var m03 :double = 0;		

	public var m10 :double = 0;
	public var m11 :double = 1;
	public var m12 :double = 0;
	public var m13 :double = 0;

	public var m20 :double = 0;
	public var m21 :double = 0;
	public var m22 :double = 1;
	public var m23 :double = 0;

	private static var s_temp_quat :L0Quat4 = new L0Quat4();
	private static var s_identity :L0Affine3D = new L0Affine3D();

	public static function get identity() :L0Affine3D {
		return s_identity;
	}

	public function dup(): L0Affine3D {
		var tmp :L0Affine3D = new L0Affine3D();
		tmp.initFromAffine3D(this);
		return tmp;
	}

	private static var s_tmp :L0Affine3D = new L0Affine3D();




	 /**
	 public static Matrix DualQuaternionToMatrix(
DualQuaternion_c q )
{
 q = DualQuaternion_c.Normalize( q );
 Matrix M = Matrix.Identity;
 float w = q.m_real.W;
 float x = q.m_real.X;
 float y = q.m_real.Y;
 float z = q.m_real.Z;
 // Extract rotational information
 M.M11 = w*w + x*x - y*y - z*z;
 M.M12 = 2*x*y + 2*w*z;
 M.M13 = 2*x*z - 2*w*y;

 M.M21 = 2*x*y - 2*w*z;
 M.M22 = w*w + y*y - x*x - z*z;
 M.M23 = 2*y*z + 2*w*x;
 M.M31 = 2*x*z + 2*w*y;
 M.M32 = 2*y*z - 2*w*x;
 M.M33 = w*w + z*z - x*x - y*y;
// Extract translation information
 Quaternion t = (q.m_dual * 2.0f) * Quaternion.Conjugate(
q.m_real);
 M.M41 = t.X;
 M.M42 = t.Y;
 M.M43 = t.Z;
 return M;
 */
 
	 public function initFromDualQuat(q :L0DualQuat) :L0Affine3D {
	 	initFromQuaternion(q.m_real);
	 	
	 	s_temp_quat.initAsCopy(q.m_dual);
	 	s_temp_quat.scale(2.0);
	 	s_temp_quat.concatConjugate(q.m_real);
	 	m03 = s_temp_quat.x;
	 	m13 = s_temp_quat.y;
	 	m23 = s_temp_quat.z;

	 	return this;	
	 }

	 /**
	  * Create a rotation matrix from a NORMALIZED quaternion.
	  * TODO eliminate a bunch f the common factors
	  */

	public function initFromNormalizedQuat(q :L0Quat4) :L0Affine3D {
		var w :double = q.w;
		var x :double = q.x;
		var y :double = q.y;
		var z :double = q.z;
		// Extract rotational information
		m00 = w*w + x*x - y*y - z*z;
		m01 = 2*x*y + 2*w*z;
		m03 = 2*x*z - 2*w*y;

		m10 = 2*x*y - 2*w*z;
		m11 = w*w + y*y - x*x - z*z;
		m13 = 2*y*z + 2*w*x;
		m20 = 2*x*z + 2*w*y;
		m21 = 2*y*z - 2*w*x;
		m23 = w*w + z*z - x*x - y*y;

		m03 = 0;
		m13 = 0;
		m23 = 0;

		return this;
	} 

	/** 
	 * This is also semi useful
	 * http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm
	 * has code to set up a rotation around a point
	 */


	public function initFromQuaternion(q :L0Quat4) : L0Affine3D
	{
		var sqw :double = q.w*q.w;
	    var sqx :double = q.x*q.x;
	    var sqy :double = q.y*q.y;
	    var sqz :double = q.z*q.z;

	    // invs (inverse square length) is only required if quaternion is not already normalised
	    var invs :double = 1.0 / (sqx + sqy + sqz + sqw);
	    m00 = ( sqx - sqy - sqz + sqw)*invs ; // since sqw + sqx + sqy + sqz =1/invs*invs
	    m11 = (-sqx + sqy - sqz + sqw)*invs ;
	    m22 = (-sqx - sqy + sqz + sqw)*invs ;
	    
	    var tmp1 :double = q.x*q.y;
	    var tmp2 :double = q.z*q.w;
	    m10 = 2.0 * (tmp1 + tmp2)*invs ;
	    m01 = 2.0 * (tmp1 - tmp2)*invs ;
	    
	    tmp1 = q.x*q.z;
	    tmp2 = q.y*q.w;
	    m20 = 2.0 * (tmp1 - tmp2)*invs ;
	    m02 = 2.0 * (tmp1 + tmp2)*invs ;
	    tmp1 = q.y*q.z;
	    tmp2 = q.x*q.w;
	    m21 = 2.0 * (tmp1 + tmp2)*invs ;
	    m12 = 2.0 * (tmp1 - tmp2)*invs ;   

		m03 = 0;
		m13 = 0;
		m23 = 0;

		return this;
	}

	public function initFromAffine3D(other :L0Affine3D) :L0Affine3D {
		m00 = other.m00;
		m01 = other.m01;
		m02 = other.m02;
		m03 = other.m03;
		
		m10 = other.m10;
		m11 = other.m11;
		m12 = other.m12;
		m13 = other.m13;

		m20 = other.m20;
		m21 = other.m21;
		m22 = other.m22;
		m23 = other.m23;

		return this;
	}

	public function mulRow0(x :double, y:double, z:double) :double {
		return m00 * x + m01 *y + m02*z + m03;
	}

	public function mulRow1(x :double, y:double, z:double) :double {
		return m10 * x + m11 *y + m12*z + m13;
	}

	public function mulRow2(x :double, y:double, z:double) :double {
		return m20 * x + m21 *y + m22*z + m23;
	}

	public function mulVec3(src :L0Vec3, dst :L0Vec3) : L0Vec3 
	{
		var x :double = src.x;
		var y :double = src.y;
		var z :double = src.z;

		dst.x = x*m00 + y*m01 + z*m02 + m03;
		dst.y = x*m10 + y*m11 + z*m12 + m13;
		dst.z = x*m20 + y*m21 + z*m22 + m23;

		return dst;
	}

	public override function toString() :String {
		return "L0Affine3D[\n" +
				"\t" + m00 + ",\t" + m01 + ",\t" + m02 + ",\t" + m03 + "\n" +
				"\t" + m10 + ",\t" + m11 + ",\t" + m12 + ",\t" + m13 + "\n" +
				"\t" + m20 + ",\t" + m21 + ",\t" + m22 + ",\t" + m23 + "\n" +
				"\t0,\t0,\t0,\t1]";

	}

	public function init() : L0Affine3D {
		m00 = 1;
		m01 = 0;
		m02 = 0;
		m03 = 0;
		m10 = 0;
		m11 = 1;
		m12 = 0;
		m13 = 0;
		m20 = 0;
		m21 = 0;
		m22 = 1;
		m23 = 0;
		return this;
	}

	public function initAsIdentity() : L0Affine3D {
		m00 = 1;
		m01 = 0;
		m02 = 0;
		m03 = 0;
		m10 = 0;
		m11 = 1;
		m12 = 0;
		m13 = 0;
		m20 = 0;
		m21 = 0;
		m22 = 1;
		m23 = 0;
		return this;
	}

	public function zero(): void {
		m00 = 0;
		m01 = 0;
		m02 = 0;
		m03 = 0;
		m10 = 0;
		m11 = 0;
		m12 = 0;
		m13 = 0;
		m20 = 0;
		m21 = 0;
		m22 = 0;
		m23 = 0;	
	}

	public function initAsTranslation(x:double,y:double,z:double) : L0Affine3D
	{
		m00 = 1;
		m01 = 0;
		m02 = 0;
		m03 = x;
		m10 = 0;
		m11 = 1;
		m12 = 0;
		m13 = y;
		m20 = 0;
		m21 = 0;
		m22 = 1;
		m23 = z;	
		return this;
	}

	public function initAsScale(sx:double,sy:double,sz:double) : L0Affine3D {
		m00 = sx;
		m01 = 0;
		m02 = 0;
		m03 = 0;
		m10 = 0;
		m11 = sy;
		m12 = 0;
		m13 = 0;
		m20 = 0;
		m21 = 0;
		m22 = sz;
		m23 = 0;
		return this;
	}


	public function initAsAxisAngle(x :double, y:double, z:double, angle :double ) :L0Affine3D
	{
		c :double = Math.cos(angle);
		s :double = Math.sin(angle);
		t :double = 1 - c;

		m00 = t*x*x + c	;
		m01 = t*x*y - z*s;	
		m02 = t*x*z + y*s;

		m10 = t*x*y + z*s;	
		m11 = t*y*y + c	;
		m12 = t*y*z - x*s;
		
		m20 = t*x*z - y*s;	
		m21 = t*y*z + x*s;	
		m22 = t*z*z + c;
		
		m03 = 0;
		m13 = 0;
		m23 = 0;
		return this;
	}


	public function initAsXRotation(radians :double) : L0Affine3D
	{
		var s :double = Math.sin(radians);
		var c :double = Math.cos(radians);
		m00 = 1.0;
		m01 = 0;
		m02 = 0;
		m03 = 0;
		
		m10 = 0;
		m11 = c;
		m12 = -s;
		m13 = 0;

		m20 = 0;
		m21 = s;
		m22 = c;
		m23 = 0;
		return this;
	}


	public function initAsYRotation(radians :double) : L0Affine3D
	{

		var s :double = Math.sin(radians);
		var c :double = Math.cos(radians);
		m00 = c;
		m01 = 0;
		m02 = s;
		m03 = 0;
		
		m10 = 0;
		m11 = 1.0;
		m12 = 0;
		m13 = 0;

		m20 = -s;
		m21 = 0;
		m22 = c;
		m23 = 0;
		return this;
	}

	public function initAsZRotation(radians :double) : L0Affine3D
	{
		var s :double = Math.sin(radians);
		var c :double = Math.cos(radians);
		m00 = c;
		m01 = -s;
		m02 = 0;
		m03 = 0;
		
		m10 = s;
		m11 = c;
		m12 = 0;
		m13 = 0;

		m20 = 0;
		m21 = 0;
		m22 = 1.0;
		m23 = 0;
		return this;
	}


	public function initFromRowValues(
		a:double,b:double,c:double,d:double,
		e:double,f:double,g:double,h:double,
		i:double,j:double,k:double,l:double) : L0Affine3D
	{
		m00 = a; m01 = b; m02 = c; m03 = d;
		m10 = e; m11 = f; m12 = g; m13 = h;
		m20 = i; m21 = j; m22 = k; m23 = l;
		return this;
	}

	public function concatQuaternion(q :L0Quat4) :L0Affine3D
	{
		var qw :double = q.w;
		var qx :double = q.x;
		var qy :double = q.y;
		var qz :double = q.z;
		var q00: double = 1 - 2*qy*qy - 2*qz*qz;	
		var q01: double = 2*qx*qy - 2*qz*qw;	
		var q02: double = 2*qx*qz + 2*qy*qw;
		var q10: double = 2*qx*qy + 2*qz*qw;	
		var q11: double = 1 - 2*qx*qx - 2*qz*qz;
		var q12: double = 2*qy*qz - 2*qx*qw;
		var q20: double = 2*qx*qz - 2*qy*qw;
		var q21: double = 2*qy*qz + 2*qx*qw;
		var q22: double = 1 - 2*qx*qx - 2*qy*qy;


		var x00:double = m00;
		var x01:double = m01;
		var x02:double = m02;

		var x10:double = m10;
		var x11:double = m11;
		var x12:double = m12;
		
		var x20:double = m20;
		var x21:double = m21;
		var x22:double = m22;

		m00  = x00*q00 + x01*q10 + x02*q20;
		m01  = x00*q01 + x01*q11 + x02*q21;
		m02  = x00*q02 + x01*q12 + x02*q22;	
	
		m10  = x10*q00 + x11*q10 + x12*q20;
		m11  = x10*q01 + x11*q11 + x12*q21;
		m12  = x10*q02 + x11*q12 + x12*q22;	

		m20  = x20*q00 + x21*q10 + x22*q20;
		m21  = x20*q01 + x21*q11 + x22*q21;
		m22  = x20*q02 + x21*q12 + x22*q22;	
		return this;
	}

	// this seems likely incorrect.. as in its column major instead of row major
	public function concatRowMajorMat3f(m : 9 double): L0Affine3D
	{
		var x00:double = m00;
		var x01:double = m01;
		var x02:double = m02;

		var x10:double = m10;
		var x11:double = m11;
		var x12:double = m12;
		
		var x20:double = m20;
		var x21:double = m21;
		var x22:double = m22;

		m00  = x00*m_0 + x01*m_3 + x02*m_6;
		m01  = x00*m_1 + x01*m_4 + x02*m_7;
		m02  = x00*m_2 + x01*m_5 + x02*m_8;	
		
		
		m10  = x10*m_0 + x11*m_3 + x12*m_6;
		m11  = x10*m_1 + x11*m_4 + x12*m_7;
		m12  = x10*m_2 + x11*m_5 + x12*m_8;

		
		m20  = x20*m_0 + x21*m_3 + x22*m_6;
		m21  = x20*m_1 + x21*m_4 + x22*m_7;
		m22  = x20*m_2 + x21*m_5 + x22*m_8;
		return this;
	}

	public function concat(o :L0Affine3D) : L0Affine3D {

		var x00:double = m00;
		var x01:double = m01;
		var x02:double = m02;

		var x10:double = m10;
		var x11:double = m11;
		var x12:double = m12;
		
		var x20:double = m20;
		var x21:double = m21;
		var x22:double = m22;

		m00  = x00*o.m00 + x01*o.m10 + x02*o.m20;
		m01  = x00*o.m01 + x01*o.m11 + x02*o.m21;
		m02  = x00*o.m02 + x01*o.m12 + x02*o.m22;	
		m03 += x00*o.m03 + x01*o.m13 + x02*o.m23;
	
		m10  = x10*o.m00 + x11*o.m10 + x12*o.m20;
		m11  = x10*o.m01 + x11*o.m11 + x12*o.m21;
		m12  = x10*o.m02 + x11*o.m12 + x12*o.m22;	
		m13 += x10*o.m03 + x11*o.m13 + x12*o.m23;

		m20  = x20*o.m00 + x21*o.m10 + x22*o.m20;
		m21  = x20*o.m01 + x21*o.m11 + x22*o.m21;
		m22  = x20*o.m02 + x21*o.m12 + x22*o.m22;	
		m23 += x20*o.m03 + x21*o.m13 + x22*o.m23;
		return this;
	}


	/**
	 * Concat the transpose of the 3x3 comonent of the passed affine matrix
	 */

	public function concatTranspose(o :L0Affine3D) : L0Affine3D {

		var x00:double = m00;
		var x01:double = m01;
		var x02:double = m02;

		var x10:double = m10;
		var x11:double = m11;
		var x12:double = m12;
		
		var x20:double = m20;
		var x21:double = m21;
		var x22:double = m22;

		m00  = x00*o.m00 + x01*o.m01 + x02*o.m02;
		m01  = x00*o.m10 + x01*o.m11 + x02*o.m12;
		m02  = x00*o.m20 + x01*o.m21 + x02*o.m22;	
		//m03 += x00*o.m03 + x01*o.m13 + x02*o.m23;
	
		m10  = x10*o.m00 + x11*o.m01 + x12*o.m02;
		m11  = x10*o.m10 + x11*o.m11 + x12*o.m12;
		m12  = x10*o.m20 + x11*o.m21 + x12*o.m22;	
		//m13 += x10*o.m03 + x11*o.m13 + x12*o.m23;

		m20  = x20*o.m00 + x21*o.m01 + x22*o.m02;
		m21  = x20*o.m10 + x21*o.m11 + x22*o.m12;
		m22  = x20*o.m20 + x21*o.m21 + x22*o.m22;	
		//m23 += x20*o.m03 + x21*o.m13 + x22*o.m23;
		return this;
	}

	public function concatTranslation(x :double, y:double, z:double) : L0Affine3D {
		m03 += m00 * x + m01 * y + m02 * z;	
		m13 += m10 * x + m11 * y + m12 * z;
		m23 += m20 * x + m21 * y + m22 * z;
		return this;
	}

	public function concatScale(x : double, y:double, z:double) :L0Affine3D {
		m00 *= x;
		m01 *= y;
		m02 *= z;
	
		m10 *= x;
		m11 *= y;
		m12 *= z;

		m20 *= x;
		m21 *= y;
		m22 *= z;
	
		return this;
	}


	/*
		Returns a rotation that rotates z degrees around the z axis, x degrees around the x axis, and y degrees around the y axis (in that order).
	
		** so in our game I guess everything points south by default
		twist => pitch => rotatiion
	*/


	public function concatEuler(x :double, y:double, z:double) : L0Affine3D 
	{	
		concatYRotation(y);
		concatXRotation(x);
		concatZRotation(z);
		/*
		s_tmp.initAsYRotation(y);
		concat(s_tmp);
		s_tmp.initAsXRotation(x);
		concat(s_tmp);
		s_tmp.initAsZRotation(z);
		concat(s_tmp);
		*/
		return this;
	}

	public function concatQuat4(q :L0Quat4) :L0Affine3D {
		s_tmp.initFromQuaternion(q);
		concat(s_tmp);
		return this;
	}

	public function concatXRotation(x :double) : L0Affine3D {
		if (x == 0) { return this; }
		s_tmp.initAsXRotation(x);
		concat(s_tmp);
		return this;
	}


	public function concatYRotation(x :double) : L0Affine3D {
		if (x == 0) { return this; }		
		s_tmp.initAsYRotation(x);
		concat(s_tmp);
		return this;
	}


	public function concatZRotation(x :double) : L0Affine3D {
		if (x == 0) { return this; }		
		s_tmp.initAsYRotation(x);
		concat(s_tmp);
		return this;
	}


	public function invert() :L0Affine3D {
		initAsInverse(this);
		return this;
	}

	public function get determinant() : double {
		var det :double = 
			m00 * (m11*m22 - m21*m12) +
			m01 * (m22*m20 - m10*m22) +
			m02 * (m10*m21 - m11*m20);
		return det;
	}

	public function initAsInverse(src :L0Affine3D) :L0Affine3D
	{

		var a00 :double = src.m00;
		var a01 :double = src.m01;
		var a02 :double = src.m02;

		var a10 :double = src.m10;
		var a11 :double = src.m11;
		var a12 :double = src.m12;

		var a20 :double = src.m20;
		var a21 :double = src.m21;
		var a22 :double = src.m22;


		var det :double = 
			a00 * (a11*a22 - a21*a12) +
			a01 * (a22*a20 - a10*a22) +
			a02 * (a10*a21 - a11*a20);

		if (det == 0) {
			System.log ("" + src);
			throw new Error("Can't invert source matrix");
		}

		var det_inv :double = 1.0 / det;

		//  a00 a01 a02 a03
		//  a10 a11 a12 a13
		//  a20 a21 a22 a23
		//   0   0   0   1

		// A = [ M   b  ]
		//     [ 0   1  ]
		//
		// Ainv = [ Minv  -Minv*b ]
		//        [   0         1 ]


		// calculate the adjoint matrix
		var b00 :double = a11*a22 - a21*a12;
		var b01 :double = -(a10*a22 - a12*a20);
		var b02 :double = a10*a21 - a11*a20;

		var b10 :double = -(a01*a22 - a02*a21);
		var b11 :double = a00*a22 - a02*a20;
		var b12 :double = -(a00*a21 - a01*a20);

		var b20 :double = a01*a12-a02*a11;
		var b21 :double = -(a00*a12 - a02*a10);
		var b22 :double = a00*a11 - a01*a10;

		m00 = b00 * det_inv;
		m10 = b01 * det_inv;
		m20 = b02 * det_inv;

		m01 = b10 * det_inv;
		m11 = b11 * det_inv;
		m21 = b12 * det_inv;

		m02 = b20 * det_inv;
		m12 = b21 * det_inv;
		m22 = b22 * det_inv;

		// calculate the negative of the vector
		var tx :double = -src.m03;
		var ty :double = -src.m13;
		var tz :double = -src.m23;

		m03 = m00 * tx + m01 * ty + m02 * tz;
		m13 = m10 * tx + m11 * ty + m12 * tz;
		m23 = m20 * tx + m21 * ty + m22 * tz;

		return this;
	}
}