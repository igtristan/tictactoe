package org.iglang.layer0.math;

/**
 * Based on this write up:
 * https://cs.gmu.edu/~jmlien/teaching/cs451/uploads/Main/dual-quaternion.pdf
 */

public class L0DualQuat
{
	internal var m_real :L0Quat4 = new L0Quat4();
	internal var m_dual :L0Quat4 = new L0Quat4();


  public function constructor() {
    m_dual.w = 0;
  }

  public function init() :L0DualQuat {
    m_real.init(0,0,0,1);
    m_dual.init(0,0,0,0);
    return this;
  }

  public function get rotation() :L0Quat4 {
    return m_real;
  }

  public function get translation() :L0Vec3 {
    return null;
  }

  public function initAsCopy(other :L0DualQuat) :L0DualQuat {
    m_real.initAsCopy(other.m_real);
    m_dual.initAsCopy(other.m_dual);
    return this;
  }

  public function initFromQuatAndVec3(q :L0Quat4, v :L0Vec3) :L0DualQuat {
    m_real.initAsCopy(q);
    m_real.normalize();
    m_dual.initFromVec3(v);
    m_dual.concat(m_real).scale(0.5);
    return this;
  }

	public function initFromAffine3D(source :L0Affine3D) :L0DualQuat
	{
		
    m_real.initFromAffine3D(source);    // <-- dont think this yet exists

    // m_dual = ( new Quaternion( t, 0 ) * m_real ) * 0.5f;
    
    m_dual.x = source.m03;
    m_dual.y = source.m13;
    m_dual.z = source.m23;
    m_dual.w = 0;
    m_dual.concat(m_real).scale(0.5);
    return this;
  }


  public function concat(right :L0DualQuat) :L0DualQuat
  {

      throw new Error("NYI");
    /*
    public static DualQuaternion_c operator * (DualQuaternion_c
lhs, DualQuaternion_c rhs)
{
 return new DualQuaternion_c(rhs.m_real*lhs.m_real,
 rhs.m_dual*lhs.m_real + rhs.m_real*lhs.m_dual);
}
*/

    // barf

  }
  public function initAsLerp(left :L0DualQuat, right :L0DualQuat, t :double) :L0DualQuat {
    m_real.initAsLerp(left.m_real,right.m_real,t);
    m_dual.initAsLerp(left.m_dual,right.m_dual,t);
    return this;
  }

   public function initAsLeftConjugateLerp(left :L0DualQuat, right :L0DualQuat, t :double) :L0DualQuat {
    m_real.initAsLeftConjugateLerp(left.m_real,right.m_real,t);
    m_dual.initAsLeftConjugateLerp(left.m_dual,right.m_dual,t);
    return this;
  }

  
  public function initAsBlend(left :L0DualQuat, right :L0DualQuat, t :double) :L0DualQuat {
    if (left.m_real.dot(right.m_real) >= 0) {
      return initAsLerp(left,right,t);
    }
    else {
      return initAsLeftConjugateLerp(left,right,t);
    }
  }

  public function normalize() :void {
      var mag :double = 1.0 / m_real.magnitude;
      m_real.scale(mag);
      m_dual.scale(mag);
  }
}


/*
public class DualQuaternion_c
{
public Quaternion m_real;
public Quaternion m_dual;
public DualQuaternion_c()
{
 m_real = new Quaternion(0,0,0,1);
 m_dual = new Quaternion(0,0,0,0);
}
public DualQuaternion_c( Quaternion r, Quaternion d )
{
 m_real = Quaternion.Normalize( r );
 m_dual = d;
}
public DualQuaternion_c( Quaternion r, Vector3 t )
{
 m_real = Quaternion.Normalize( r );
 m_dual = ( new Quaternion( t, 0 ) * m_real ) * 0.5f;
}
public static float Dot( DualQuaternion_c a,
DualQuaternion_c b )
{
 return Quaternion.Dot( a.m_real, b.m_real );
}
public static DualQuaternion_c operator* (DualQuaternion_c
q, float scale)
{
 DualQuaternion_c ret = q;
 ret.m_real *= scale;
 ret.m_dual *= scale;
 return ret;
}
public static DualQuaternion_c Normalize( DualQuaternion_c q
)
{
 float mag = Quaternion.Dot( q.m_real, q.m_real );
 Debug_c.Assert( mag > 0.000001f );
 DualQuaternion_c ret = q;
 ret.m_real *= 1.0f / mag;
 ret.m_dual *= 1.0f / mag;
 return ret;
}
public static DualQuaternion_c operator + (DualQuaternion_c
lhs, DualQuaternion_c rhs)
{
 return new DualQuaternion_c(lhs.m_real + rhs.m_real,
 lhs.m_dual + rhs.m_dual);
}
// Multiplication order - left to right
public static DualQuaternion_c operator * (DualQuaternion_c
lhs, DualQuaternion_c rhs)
{
 return new DualQuaternion_c(rhs.m_real*lhs.m_real,
 rhs.m_dual*lhs.m_real + rhs.m_real*lhs.m_dual);
}
public static DualQuaternion_c Conjugate( DualQuaternion_c q
)
{
 return new DualQuaternion_c( Quaternion.Conjugate(
q.m_real ), Quaternion.Conjugate( q.m_dual ) );
}
public static Quaternion GetRotation( DualQuaternion_c q )
{
 return q.m_real;
}
public static Vector3 GetTranslation( DualQuaternion_c q )
{
 Quaternion t = ( q.m_dual * 2.0f ) * Quaternion.Conjugate(
q.m_real );
 return new Vector3( t.X, t.Y, t.Z );
}
public static Matrix DualQuaternionToMatrix(
DualQuaternion_c q )
{
 q = DualQuaternion_c.Normalize( q );
 Matrix M = Matrix.Identity;
 float w = q.m_real.W;
 float x = q.m_real.X;
 float y = q.m_real.Y;
 float z = q.m_real.Z;
 // Extract rotational information
 M.M11 = w*w + x*x - y*y - z*z;
 M.M12 = 2*x*y + 2*w*z;
 M.M13 = 2*x*z - 2*w*y;

 M.M21 = 2*x*y - 2*w*z;
 M.M22 = w*w + y*y - x*x - z*z;
 M.M23 = 2*y*z + 2*w*x;
 M.M31 = 2*x*z + 2*w*y;
 M.M32 = 2*y*z - 2*w*x;
 M.M33 = w*w + z*z - x*x - y*y;
// Extract translation information
 Quaternion t = (q.m_dual * 2.0f) * Quaternion.Conjugate(
q.m_real);
 M.M41 = t.X;
 M.M42 = t.Y;
 M.M43 = t.Z;
 return M;
 */