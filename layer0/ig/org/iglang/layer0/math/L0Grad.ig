package org.iglang.layer0.math;

public class L0Grad
{
	public var x :double;
	public var y :double;
	public var z :double;
  	
  	public function constructor(x :double, y :double, z :double) 
  	{
    	this.x = x; this.y = y; this.z = z;
  	}
  
	public function dot2(x :double, y :double) :double {
		return this.x*x + this.y*y;
	}

	public function dot3(x :double, y :double, z :double) :double {
		return this.x*x + this.y*y + this.z*z;
	}
}