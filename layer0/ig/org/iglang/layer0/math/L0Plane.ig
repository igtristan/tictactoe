package org.iglang.layer0.math;

// could use this as an example for the non trivial work
// http://glmatrix.net/docs/mat4.js.html#line23
public class L0Plane
{
	public var a:double;
	public var b:double;
	public var c:double;
	public var d:double;

	// Ax + By + Cz + D = 0;


	/**
	 * This assumes the normal is normalized!
	 */
	public function initFromPointAndNormal(
		x :double, y:double, z:double,
		nx :double, ny:double, nz:double) : L0Plane {

		d = -(x*nx+y*ny+z*nz);
		a = nx;
		b = ny;
		c = nz;
		return this;
	}

	public function signedDistance(x :double, y:double, z:double) : double{
		return a*x + b*y + c*z + d;
	}

	public function distance(x :double, y:double, z:double) :double {
		return Math.abs(a*x + b*y + c*z + d);
	}
}