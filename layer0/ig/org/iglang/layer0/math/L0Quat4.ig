package org.iglang.layer0.math;

/*
    |a * b| = |a||b| cos(theta)
    |a||b| cos(theta) = dot(a,b)

*/

public class L0Quat4 {
    public var w :double;
    public var x :double;
    public var y :double;
    public var z :double; 


    public function dot(other :L0Quat4) :double {
        return x*other.x + y*other.y + z*other.z + w*other.w;
    }

    // create a new object with the given components
    public function init(  x :double ,  y:double ,  z:double , w :double ) :L0Quat4 
    {
        this.w = w;
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }



    public function initAsLerp(left :L0Quat4, right :L0Quat4, t:double): L0Quat4
    {

        x = left.x + (right.x - left.x) * t;
        y = left.y + (right.y - left.y) * t;
        z = left.z + (right.z - left.z) * t;
        w = left.w + (right.w - left.w) * t;
        return this;
    }

    public function initAsLeftConjugateLerp(left :L0Quat4, right :L0Quat4, t:double): L0Quat4
    {

        x = -left.x + (right.x + left.x) * t;
        y = -left.y + (right.y + left.y) * t;
        z = -left.z + (right.z + left.z) * t;
        w = -left.w + (right.w + left.w) * t;
        return this;
    }

    /**
     * Extract the rotational component from an Affine3D
     * http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
     */

    public function initFromAffine3D(matrix :L0Affine3D) :L0Quat4
    {
        var tr :double = matrix.m00 + matrix.m11 + matrix.m22;

        if (tr > 0) { 
          var S :double = Math.sqrt(tr+1.0) * 2; // S=4*this.w 
          this.w = 0.25 * S;
          this.x = (matrix.m21 - matrix.m12) / S;
          this.y = (matrix.m02 - matrix.m20) / S; 
          this.z = (matrix.m10 - matrix.m01) / S; 
        } 
        else if ((matrix.m00 > matrix.m11)&&(matrix.m00 > matrix.m22)) { 
          var S :double = Math.sqrt(1.0 + matrix.m00 - matrix.m11 - matrix.m22) * 2; // S=4*this.x 
          this.w = (matrix.m21 - matrix.m12) / S;
          this.x = 0.25 * S;
          this.y = (matrix.m01 + matrix.m10) / S; 
          this.z = (matrix.m02 + matrix.m20) / S; 
        } 
        else if (matrix.m11 > matrix.m22) { 
          var S :double = Math.sqrt(1.0 + matrix.m11 - matrix.m00 - matrix.m22) * 2; // S=4*this.y
          this.w = (matrix.m02 - matrix.m20) / S;
          this.x = (matrix.m01 + matrix.m10) / S; 
          this.y = 0.25 * S;
          this.z = (matrix.m12 + matrix.m21) / S; 
        } 
        else { 
          var  S :double = Math.sqrt(1.0 + matrix.m22 - matrix.m00 - matrix.m11) * 2; // S=4*this.z
          this.w = (matrix.m10 - matrix.m01) / S;
          this.x = (matrix.m02 + matrix.m20) / S;
          this.y = (matrix.m12 + matrix.m21) / S;
          this.z = 0.25 * S;
        }
        
        return this;
    }

    public function initFromVec3(v :L0Vec3) :L0Quat4
    {
        this.w = 0;
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
        return this;
    }

    public function initAsCopy(other :L0Quat4) :L0Quat4
    {
        this.x = other.x;
        this.y = other.y;
        this.z = other.z;
        this.w = other.w;
        return this;
    }

    /**
     * Convert into  unit quaternion
     */
    public function normalize() : void {
        var mag_inv :double = 1.0 / Math.sqrt(x*x+y*y+z*z+w*w);
        x *= mag_inv;
        y *= mag_inv;
        z *= mag_inv;
        w *= mag_inv;
    }
    /**
     * Initialize from an axis angle
     * TODO make a version that takes all doubles
     */

    public function initFromAxisAngle(aa :L0AxisAngle) :L0Quat4 {
        // guess this at most can represent a rotation on 360 degrees in eithe direction
        // 
        this.w =        Math.cos( aa.angle/2 );
        this.x = aa.x * Math.sin( aa.angle/2 );
        this.y = aa.y * Math.sin( aa.angle/2 );
        this.z = aa.z * Math.sin( aa.angle/2 );
        return this;
    }

    /**
     * Concatenate another quaternion onto this one
     */

    public function concat( b :L0Quat4) :L0Quat4{
        var a :L0Quat4 = this;
        var y0 :double= a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z;
        var y1 :double= a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y;
        var y2 :double= a.w*b.y - a.x*b.z + a.y*b.w + a.z*b.x;
        var y3 :double= a.w*b.z + a.x*b.y - a.y*b.x + a.z*b.w;
        this.w = y0;
        this.x = y1;
        this.y = y2;
        this.z = y3;
        return this;
    }

  /**
     * Concatenate the conjugte of another quaternion onto this one
     */

    public function concatConjugate( b :L0Quat4) :L0Quat4{
        var a :L0Quat4 = this;

        var bx:double = -b.x;
        var by:double = -b.y;
        var bz:double = -b.z;
        var bw:double = b.w;

        var y0 :double= a.w*bw - a.x*bx - a.y*by - a.z*bz;
        var y1 :double= a.w*bx + a.x*bw + a.y*bz - a.z*by;
        var y2 :double= a.w*by - a.x*bz + a.y*bw + a.z*bx;
        var y3 :double= a.w*bz + a.x*by - a.y*bx + a.z*bw;
        this.w = y0;
        this.x = y1;
        this.y = y2;
        this.z = y3;
        return this;
    }


    /**
     * Concat a quaterion represented by a vector and a w component of 0
     * onto this one.
     */

    public function concatVec3( b :L0Vec3) :L0Quat4{
        var a :L0Quat4 = this;
        var y0 :double= a.w*0 - a.x*b.x - a.y*b.y - a.z*b.z;
        var y1 :double= a.w*b.x + a.x*0 + a.y*b.z - a.z*b.y;
        var y2 :double= a.w*b.y - a.x*b.z + a.y*0 + a.z*b.x;
        var y3 :double= a.w*b.z + a.x*b.y - a.y*b.x + a.z*0;
        this.w = y0;
        this.x = y1;
        this.y = y2;
        this.z = y3;
        return this;
    }

    /**
     * Concat the components of another quaternion
     */

    public function add(b :L0Quat4) :L0Quat4
    {
        x += b.x;
        y += b.y;
        z += b.z;
        w += 0;
        return this;
    }

    public function addScaled(b :L0Quat4, s:double): L0Quat4
    {
        x += b.x*s;
        y += b.y*s;
        z += b.z*s;
        w += b.w*s;//why was this at some pt0?
        return this;
    }

    public function scale(s :double): L0Quat4
    {
        x *= s;
        y *= s;
        z *= s;
        w *= s;
        return this;
    }
    /*
        alterantive.  maybe faster?
        https://blog.molecular-matters.com/2013/05/24/a-faster-quaternion-vector-multiplication/
    */

    public function mulVec3_alt(src :L0Vec3, dst:L0Vec3) : void 
    {
        var sx :double = src.x;
        var sy :double = src.y;
        var sz :double = src.z;

        var qw :double = w;
        var qx :double = x;
        var qy :double = y;
        var qz :double = z;

        //t = 2 * cross(q.xyz, v)
        var tx :double = 2*(sy*qz - sz*qy);
        var ty :double = 2*(sz*qx - sx*qz);
        var tz :double = 2*(sx*qy - sy*qx);        

        //v' = v + q.w * t + cross(q.xyz, t)
        dst.x = sx + qw * tx + (qy*tz - tz*qy);
        dst.x = sy + qw * ty + (qz*tx - tx*qz);
        dst.x = sz + qw * tz + (qx*ty - ty*qx);        
    }

    public function mulVec3(src :L0Vec3, dst:L0Vec3) :void
    {

        var qw :double = w;
        var qx :double = x;
        var qy :double = y;
        var qz :double = z;

        var qxx :double = qx*qx;
        var qxy :double = qx*qy;
        var qxz :double = qx*qz;
        var qxw :double = qx*qw;
        var qyy :double = qy*qy;
        var qyz :double = qy*qz;
        var qyw :double = qy*qw;
        var qzz :double = qz*qz;
        var qzw :double = qz*qw;

        var q00: double = (1 - 2*qyy - 2*qzz) * src.x;    
        var q01: double = (2*qxy - 2*qzw) * src.y;    
        var q02: double = (2*qxz + 2*qyw) * src.z;
    
        var q10: double = (2*qxy + 2*qzw) * src.x;    
        var q11: double = (1 - 2*qxx - 2*qzz) * src.y;
        var q12: double = (2*qyz - 2*qxw) * src.z;
    
        var q20: double = (2*qxz - 2*qyw) * src.x;
        var q21: double = (2*qyz + 2*qxw) * src.y;
        var q22: double = (1 - 2*qxx - 2*qyy) * src.z;

        dst.x = q00 + q01 + q02;
        dst.y = q10 + q11 + q12;
        dst.z = q20 + q21 + q22;
    }

    // return a string representation of the invoking object
    public override function toString() :String {
        return "L0Quat4[" + w + " + " + x + "i + " + y + "j + " + z + "k]";
    }

    /**
     *
     * adapted from:
     * https://github.com/libgdx/libgdx/blob/master/gdx/src/com/badlogic/gdx/math/Quaternion.java
     */
    public  function initAsSlerp(start : L0Quat4, end :L0Quat4, alpha:double) :L0Quat4
    {
        var  d :double = start.x * end.x + 
                          start.y * end.y + 
                          start.z * end.z + 
                          start.w * end.w;
        var  absDot :double = (d < 0) ? -d : d;

        // Set the first and second scale for the interpolation
        var scale0 :double = 1 - alpha;
        var scale1 :double = alpha;

        // Check if the angle between the 2 quaternions was big enough to
        // warrant such calculations
        if ((1 - absDot) > 0.1) {// Get the angle between the 2 quaternions,
            // and then store the sin() of that angle
            var angle :double = Math.acos(absDot);
            var invSinTheta :double = 1 / Math.sin(angle);

            // Calculate the scale for q1 and q2, according to the angle and
            // it's sine value
            scale0 = Math.sin((1 - alpha) * angle) * invSinTheta;
            scale1 = Math.sin(alpha * angle)        * invSinTheta;
        }

        if (d < 0) {
            scale1 = -scale1;
        }

        // Calculate the x, y, z and w values for the quaternion by using a
        // special form of linear interpolation for quaternions.
        x = (scale0 * start.x) + (scale1 * end.x);
        y = (scale0 * start.y) + (scale1 * end.y);
        z = (scale0 * start.z) + (scale1 * end.z);
        w = (scale0 * start.w) + (scale1 * end.w);

        // Return the interpolated quaternion
        return this;
    }

    public function get magnitude() :double {
        return Math.sqrt(w*w + x*x +y*y + z*z);
    }


/*
    // return the quaternion norm
 
    // return the quaternion conjugate
    public Quaternion conjugate() {
        return new Quaternion(w, -x, -y, -z);
    }

    // return a new Quaternion whose value is (this + b)
    public Quaternion plus(Quaternion b) {
        Quaternion a = this;
        return new Quaternion(a.w+0, a.x+b.x, a.y+b.y, a.z+b.z);
    }


   

    // return a new Quaternion whose value is the inverse of this
    public Quaternion inverse() {
        double d = w*w + x*x + y*y + z*z;
        return new Quaternion(w/d, -x/d, -y/d, -z/d);
    }


    // return a / b
    // we use the definition a * b^-1 (as opposed to b^-1 a)
    public Quaternion divides(Quaternion b) {
        Quaternion a = this;
        return a.times(b.inverse());
    }
*/

    public function getEulerAngels(v :L0Vec3) :L0Vec3
    {
        var q:L0Quat4 = this;

        var q00 :double = q.w*q.w;
        var q11 :double = q.x*q.x;
        var q22 :double = q.y*q.y;
        var q33 :double = q.z*q.z;

        var r11 :double = q00 + q11 - q22  -q33;
        var r21 :double = 2 * (q.x*q.y + q.w*q.z);
        var r31 :double = 2 * (q.x*q.z - q.w*q.y);
        var r32 :double = 2 * (q.y*q.z + q.w*q.x);
        var r33 :double = q00 - q11 + q22 + q33;
        
        var tmp :double = Math.abs(r31);
        if (tmp > 0.999999)
        {
            var r12 : double = 2 * (q.x*q.y - q.w*q.z);
            var r13 : double = 2 * (q.x*q.z + q.w*q.y);

            v.x = 0;
            v.y = -Math.PI/2 * r31/tmp;             //<-- doule check this
            v.z = Math.atan2(-r12, -r31*r13);
            return v;
        }

        v.x = Math.atan2(r32, r33);
        v.y = Math.asin(-r31);
        v.z = Math.atan2(r21, r11);
        return v;
    }
}   
