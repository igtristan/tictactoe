package org.iglang.layer0.math;

// could use this as an example for the non trivial work
// http://glmatrix.net/docs/mat4.js.html#line23
public class L0Vec3
{
	public var x :double = 0;
	public var y :double = 0;
	public var z :double = 0;

	public function init(a :double, b:double, c:double): L0Vec3
	{
		x = a;
		y = b;
		z = c;
		return this;
	}

	public function initFromVec3Scaled(o :L0Vec3, s:double) :L0Vec3 {
		x = o.x*s;
		y = o.y*s;
		z = o.z*s;
		return this;
	}


	public function initFromVec3(o :L0Vec3) :L0Vec3 {
		x = o.x;
		y = o.y;
		z = o.z;
		return this;
	}

	public function initAsCopy(o :L0Vec3): L0Vec3 {
		x = o.x;
		y = o.y;
		z = o.z;
		return this;
	}

	public function clear() : void {
		x = 0;
		y = 0;
		z = 0;
	}

	public function add(v :L0Vec3) :void {
		x += v.x;
		y += v.y;
		z += v.z;
	}

	public function sub(v :L0Vec3) :void {
		x -= v.x;
		y -= v.y;
		z -= v.z;
	}

	public function scale(f :double) :void {
		x *= f;
		y *= f;
		z *= z;
	}


	public function projectOntoVec3(other :L0Vec3) : L0Vec3 {
		var other_mag2 :double = 
			Math.square(other.x) + 
			Math.square(other.y) + 
			Math.square(other.z);
		
		var dp :double = (x*other.x + y*other.y + z*other.z) / other_mag2;
		x = other.x * dp;
		y = other.y * dp;
		z = other.z * dp;
		return this; 
	}

	public function projectOntoUnitVec3(other :L0Vec3) : L0Vec3 {
		var dp :double = x*other.x + y*other.y + z*other.z;
		x = other.x * dp;
		y = other.y * dp;
		z = other.z * dp;
		return this; 
	}


	/**
	 * Remove the  component of the vector that is orthogonal
	 * to the plane normal
	 */
	public function projectOntoPlaneWithUnitNormal(norm :L0Vec3) : L0Vec3 {
		var dp :double = x*norm.x + y*norm.y + z*norm.z;
		x -= norm.x * dp;
		y -= norm.y * dp;
		z -= norm.z * dp;
		return this; 
	}

	/**
	 * Perform a cross product between this vector an another
	 * specified by 3 floating point values
	 */

	public function cross3f(vx :double, vy :double, vz:double) :void {
		var tx :double = y*vz - z*vy;
		var ty :double = z*vx - x*vz;
		var tz :double = x*vy - y*vx;
		x = tx;
		y = ty;
		z = tz;
	}

	/**
	 * Perform a cross product between this vector an another
	 */

	public function cross(v :L0Vec3) :void {
		var tx :double = y*v.z - z*v.y;
		var ty :double = z*v.x - x*v.z;
		var tz :double = x*v.y - y*v.x;
		x = tx;
		y = ty;
		z = tz;
	}

	public function addScaled(v :L0Vec3, s:double) :void {
		x += v.x * s;
		y += v.y * s;
		z += v.z * s;
	}

	public function get magnitude() :double {
		return Math.sqrt(x*x+y*y+z*z);
	}

	public function normalize() :void 
	{
		var mag_inv : double = 1.0 / Math.length3(x,y,z);
		x *= mag_inv;
		y *= mag_inv;
		z *= mag_inv;
	}

	public override function toString() : String {
		return "L0Vec3[" + x +"," + y +"," + z + "]";
	}
}