package org.iglang.layer0.math;

// could use this as an example for the non trivial work
// http://glmatrix.net/docs/mat4.js.html#line23
public class L0Vec4
{
	public var x :double = 0;
	public var y :double = 0;
	public var z :double = 0;
	public var w :double = 0;

	public function initFromVec4(o :L0Vec4) :L0Vec4 {
		x = o.x;
		y = o.y;
		z = o.z;
		w = o.w;
		return this;
	}


	public function initFromVec4f(x :double, y :double, z:double, w:double) :L0Vec4 {
		x = x;
		y = y;
		z = z;
		w = w;
		return this;
	}
}