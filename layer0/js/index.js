
const fs   = require("fs");
const vm   = require("vm");
const IGVM = require("igvm");

const scripts = 
["l0_net.js",
 "l0.js"];

for (var i = 0; i < scripts.length; i++) {
	const path = scripts[i];
	const code = fs.readFileSync(__dirname + '/' + path, 'utf-8');
    vm.runInThisContext(code);
}

l0._loadModule = function(n) {
	var required = require(n);
	console.error("_loadModule: " + n + " " + JSON.stringify(required));
	return required;
}

module.exports = l0;
