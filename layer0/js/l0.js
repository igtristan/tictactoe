

"use strict";

// note... this is how we do anistrophy with webgl 1
// https://blog.tojicode.com/2012/03/anisotropic-filtering-in-webgl.html


// 1 need to be able to create a user based ArrayBuffer
// 2 need to create proper inheritance model (applying member variables)
// 3 need to be able to actually load a mesh

function trace (s) {
	console.log(s);
}


//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map

// has, set, get

var   s_stats = null;


var l0 = (function()
{

function mat4_invert(out, a) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,

        // Calculate the determinant
        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!det) { 
        return null; 
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

    return out;
};


function mat4_identity(out) {
	out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
}

function mat4_create() {
    var out = new Float32Array(16);
    mat4_identity(out);
    return out;
};

function mat4_multiply(out, a, b) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

    // Cache only the current line of the second matrix
    var b0  = b[0], b1 = b[1], b2 = b[2], b3 = b[3];  
    out[0] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[1] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[2] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[3] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[4]; b1 = b[5]; b2 = b[6]; b3 = b[7];
    out[4] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[5] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[6] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[7] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[8]; b1 = b[9]; b2 = b[10]; b3 = b[11];
    out[8] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[9] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[10] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[11] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[12]; b1 = b[13]; b2 = b[14]; b3 = b[15];
    out[12] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[13] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[14] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[15] = b0*a03 + b1*a13 + b2*a23 + b3*a33;
    return out;
};

function mat4_perspective(out, fovy, aspect, near, far) {
    var f = 1.0 / Math.tan(fovy / 2),
        nf = 1 / (near - far);
    out[0] = f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = (2 * far * near) * nf;
    out[15] = 0;
    return out;
};


function mat4_ortho(out, left, right, bottom, top, near, far) {
    const lr = 1 / (left - right);
    const bt = 1 / (bottom - top);
    const nf = 1 / (near - far);
    out[0] = -2 * lr;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = -2 * bt;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 2 * nf;
    out[11] = 0;
    out[12] = (left + right) * lr;
    out[13] = (top + bottom) * bt;
    out[14] = (far + near) * nf;
    out[15] = 1;
    return out;
};	


var   m_config = null;
var   m_timeout = null;

/////////////////////////////////////////
// Performance / Debugging
/////////////////////////////////////////

var s_debug_mesh_count = 0;
var s_debug_texture_count = 0;
var s_debug_draw_count = 0;
var s_debug_draw_count_acc = 0;
var s_debug_draw_uniform = 0;
var s_debug_draw_uniform_acc = 0;
function fn_getDebugI(name)
{
	name = IGVM.stringToNative(name);
	
	if (name === "mesh_count") {
		return s_debug_mesh_count;
	}
	else if (name === "texture_count") {
		return s_debug_texture_count;
	}
	else if (name == "draw_count") {
		return s_debug_draw_count;
	}
	else if (name == "uniforms_assigned") {
		return s_debug_draw_uniform;
	}
	else {
		return 0;
	}
}

//////////////////////////////////////////
// Graphics States
////////////////////////////////////////

var m_camera_near   = 1.0;
var m_camera_far    = 1000.0;
var m_camera_fov    = Math.PI / 2.0;
var m_camera_aspect = 2.0;
var m_camera_perspective = true;
var m_camera_l = -1.0;
var m_camera_r =  1.0;
var m_camera_b = -1.0;
var m_camera_t =  1.0;
var m_proj_matrix = null;
var m_proj_view_inv_matrix = null;
//90* Math.PIm_gl.canvas.width / m_gl.canvas.height;


const GFX_STATE_DEPTH_READ  = 1;
const GFX_STATE_DEPTH_WRITE = 2;
const GFX_STATE_BLEND       = 3;
const GFX_STATE_MAP = {
	"DEPTH_READ"  : GFX_STATE_DEPTH_READ,
	"DEPTH_WRITE" : GFX_STATE_DEPTH_WRITE,
	"BLEND"       : GFX_STATE_BLEND 
}
const GFX_STATE_COUNT = 4;
const s_default_state = [0,0,0,0];
var   s_state_current = [0,0,0,0];


function graphicsStateApply(array) {
	for (var i = 1; i < GFX_STATE_COUNT; i++) {
		setGraphicsState(i, array[i]);
	}
}

function setGraphicsState(key, value)
{
	if (s_state_current[key] !== value) {
		s_state_current[key] = value;
	}


	const bool_value = value === 2;

	// note in order to clear the depth depthMask(true) needs to be
	// set 
	//console.log("apply " + key + "=" + value + " " + bool_value);

	if (key === GFX_STATE_BLEND) {
		
		if (bool_value) {
			m_gl.enable(m_gl.BLEND);
			m_gl.blendFunc(m_gl.ONE, m_gl.ONE_MINUS_SRC_ALPHA);
		}
		else {
			m_gl.disable (m_gl.BLEND);  
			m_gl.blendFunc(m_gl.ONE, m_gl.ZERO);
		}
	}
	else if (key === GFX_STATE_DEPTH_WRITE) {
		m_gl.depthMask(bool_value);
	}
	else if (key === GFX_STATE_DEPTH_READ) {
		if (bool_value) {
			m_gl.enable(m_gl.DEPTH_TEST);
		}
		else {
			m_gl.disable (m_gl.DEPTH_TEST);  
		}
	}
	else {
		console.log("undefined key : " + key);
	}

}

///////////////////////////////////////////////////////////
// NAME MANAGER
///////////////////////////////////////////////////////////


var m_name_idx   = 2;
var m_name_to_id = {};

function nameArrayToIdArray(arr) {
	const result = [];
	for (let i = 0; i < arr.length; i++) {
		let name = arr[i];

		result.push(nameToId(name));
	}
	return result;
}

function nameToId(name)
{
	if (-1 != name.indexOf(':')) {
		name = name.substr(0, name.indexOf(':'));
	}

	var bit =0;
	if (name.endsWith("_dim")) {
		name = name.substr(0, name.length - 4);
		bit = 512;
	}


	if (!m_name_to_id.hasOwnProperty(name)) {
		m_name_to_id[name] = m_name_idx;
		m_name_idx         ++;
	}

	return m_name_to_id[name] + bit;
}

const NAME_u_texture0      = nameToId("u_texture0");
const NAME_u_texture1      = nameToId("u_texture1");
const NAME_u_texture2      = nameToId("u_texture2");
const NAME_u_texture3      = nameToId("u_texture3");
const NAME_u_texture4      = nameToId("u_texture4");
const NAME_u_texture5      = nameToId("u_texture5");
const NAME_u_texture6      = nameToId("u_texture6");
const NAME_u_texture7      = nameToId("u_texture7");
const NAME_u_proj          = nameToId("u_proj");
const NAME_u_view          = nameToId("u_view");
const NAME_u_proj_view     = nameToId("u_proj_view");
const NAME_u_model         = nameToId("u_model");		
const NAME_u_camera_near   = nameToId("u_camera_near");
const NAME_u_camera_far    = nameToId("u_camera_far");
const NAME_u_camera_aspect = nameToId("u_camera_aspect");
const NAME_u_camera_fov    = nameToId("u_camera_fov");

const NAME_u_texture0_dim  = nameToId("u_texture0_dim");
const NAME_u_texture1_dim  = nameToId("u_texture1_dim");
const NAME_u_texture2_dim  = nameToId("u_texture2_dim");
const NAME_u_texture3_dim  = nameToId("u_texture3_dim");
const NAME_u_texture4_dim  = nameToId("u_texture4_dim");
const NAME_u_texture5_dim  = nameToId("u_texture5_dim");
const NAME_u_texture6_dim  = nameToId("u_texture6_dim");
const NAME_u_texture7_dim  = nameToId("u_texture7_dim");


function fn_hmac(type, data, secret)
{	
	if (type !== 1) {
		IGVM.throwException("Invalid HMAC algo");
	}


	data = IGVM.stringToNative(data);
	secret = IGVM.stringToNative(secret);

	var crypto = require('crypto');
	var result = crypto.createHmac('sha256', secret).update(data).digest('hex')

	return IGVM.nativeToString(result);
}

	
///////////////////////////////////////////////////////////
// HANDLE MANAGER
///////////////////////////////////////////////////////////

const HANDLE_IMAGE = 1;
const HANDLE_MESH  = 2;
const HANDLE_AUDIO_DATA = 3;
const HANDLE_WEBSOCKET = 4;
const HANDLE_SHADER = 5;
const HANDLE_HTTPREQUEST = 6;
const HANDLE_SURFACE = 7;
const HANDLE_AUDIO_SOURCE = 8;

const HANDLE_TYPE_TO_STRING = [ 
	"undefined", "image", "mesh", "audio-data","websocket","shader", "http-request", "surface",
	'audio-source'];



var m_handle_seq = 1;
var m_handles    = new Map();


function handleDestroy(handle_id, type) {
	if (handle_id === 0) { return null; }
	if (!m_handles.has(handle_id)) {
		return null;
	}
	const v = m_handles.get(handle_id);
	if (v.type != type) {
		// or should this throw an excepion?
		return null;
	}

	var data = v.data;
	m_handles.delete(handle_id);
	return data;
}

function handleResolve(handle_id, type) {
	if (handle_id === 0) { return null; }
	if (!m_handles.has(handle_id)) {
		return null;
	}
	const v = m_handles.get(handle_id);
	if (v.type != type) {
		// or should this throw an excepion?
		return null;
	}

	return v.data;
}

function handleResolveStrict(handle_id, type) {
	const data = handleResolve(handle_id, type);
	if (null === data) {
		var type_as_string = HANDLE_TYPE_TO_STRING[type];
		console.log("FAILED TO RESOLVE HANDLE: " + handle_id + " " + type_as_string);
		IGVM.throwException("Invalid handle: " + type_as_string);
	}
	return data;
}


function handleAlloc(data, type) {
	const seq_start = m_handle_seq;
	do
	{
		const handle_id = m_handle_seq;

		// each data element should have a type field
		if (!m_handles.has(handle_id)) {
			m_handles.set(handle_id, { 'data' : data, 'type' : type});
			data.id = handle_id;
			return handle_id;
		}

		m_handle_seq = (handle_id + 1) & 0x7fffffff;
	}
	while (m_handle_seq != seq_start);


	IGVM.throwException("Failed to alloc handle of type: " + HANDLE_TYPE_TO_STRING[type]);
}

var m_handle_manager = {

	'HANDLE_IMAGE'        : 1,
	'HANDLE_MESH'         : 2,
	'HANDLE_AUDIO_DATA'   : 3,
	'HANDLE_WEBSOCKET'    : 4,
	'HANDLE_SHADER'       : 5,
	'HANDLE_HTTPREQUEST'  : 6,
	'HANDLE_SURFACE'      : 7,
	'HANDLE_AUDIO_SOURCE' : 8,

	'alloc':          handleAlloc,
	'resolve':        handleResolve,
	'resolveStrict' : handleResolveStrict,
	'destroy'       : handleDestroy
};

function L0SoundImp()
{
	this.id     = 0;
	this.loaded = false;
	this.url   = "internal";
}

function L0SoundSourceImp()
{
	this.x              = 0;
	this.y              = 0;
	this.z              = 0;
	this.id             = 0;
	this.destroy        = false;
	this.position_dirty = true;
	this.play_queue     = [];
}


function updateBuffer(gl, buffer, data)
{
	gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
	gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);
}

function L0ShaderImp()
{
	this.program = null;
	this.loaded  = false;
	this.url  = "internal";
	this.attribute_names = [];
	this.attribute_locations = {};
	this.uniform_names = [];
	this.uniform_name_ids = [];
	this.uniform_locations = [];
	this.state = s_default_state.slice(0);
}

L0ShaderImp.prototype.bind = function() {
	this.program.bind();
	graphicsStateApply(this.state);
};

L0ShaderImp.prototype.initAsDefault = function() {
	
	const defaux =
	{	
		'uniform_names' : ['u_texture0','u_view','u_proj','u_model'],
		'attribute_names' : ['a_pos','a_uv','a_color'],
		'state':
		{ 
			"BLEND" 	  : true,
		  	"DEPTH_READ"  : false,
		  	"DEPTH_WRITE" : false
		},
		'vertex_lines' :[
			`attribute vec4 a_pos;
			attribute vec2 a_uv;
			attribute vec4 a_color;
			uniform mat4       u_view;
			uniform mat4       u_proj;
			uniform mat4       u_model;
			
			varying highp vec2 v_uv;
			varying highp vec4 v_color;
			
			void main() {  
				gl_Position = u_proj * u_view * u_model * a_pos;
				v_color = a_color;
				v_uv    = a_uv;
			}`
		],
		'fragment_lines': [
			`uniform sampler2D u_texture0;

			varying highp vec2 v_uv;
			varying highp vec4 v_color;

			void main() {
				 highp vec4 color = texture2D(u_texture0, v_uv);
				 highp float a = color.a;

				 // convert the texture to pre multiplied alpha
				 gl_FragColor = v_color * (color * vec4(a,a,a,1.0));
			}`
		]
	};

	this.initFromJson(defaux);
}

// TODO make this a compilation step
L0ShaderImp.prototype.initFromJson = function(buffer)
{
	this.attribute_names = buffer.attribute_names;
	this.uniform_names   = buffer.uniform_names;
	this.uniform_name_ids = nameArrayToIdArray(this.uniform_names);

	var vertex_source    = buffer.vertex_lines .join('\n');
	var fragment_source  = buffer.fragment_lines.join('\n');

	this.process(buffer.state, vertex_source, fragment_source);
}

L0ShaderImp.prototype.process = function(state, vertex_source, fragment_source) 
{
	for (var s in state) {	
		if (GFX_STATE_MAP.hasOwnProperty(s)) {
			this.state[GFX_STATE_MAP[s]] = state[s] ? 2 : 1;
		}
	}

	if (vertex_source.length == 0|| fragment_source.length == 0) {
		throw "Missing frag or vertex source for shader";
	}


	this.program = l0_graphics.createShader(vertex_source, fragment_source);
	var i;
	const ulen = this.uniform_names.length;

	console.log("uniform dump");
	for (i = 0; i < ulen; i++) 
	{
		const uniform_name    = this.uniform_names[i];

		let   uniform_key = uniform_name;
		if (uniform_name.indexOf(':') !== -1) {
			const uniform_toks = uniform_name.split(':');
			uniform_key = uniform_toks[0];
		}

		const location = this.program.getUniformLocation(uniform_key);
		this.uniform_locations.push(location);
		
		// check for the error case
		if (-1 === location) {
			this.uniform_names[i]     = null;
			this.uniform_name_ids[i]  = 0;
			this.uniform_locations[i] = 0;
		}	
	}

	for ( i = this.attribute_names.length - 1; i >= 0 ; i--) {
		const attribute_name = this.attribute_names[i];
		this.attribute_locations[attribute_name] = this.program.getAttribLocation(attribute_name);
		if (-1 ===  this.attribute_locations[attribute_name]) {
			console.log(attribute_name + " is " + this.attribute_locations[attribute_name]);
			//throw "Can't deal with this."
			 this.attribute_names.splice(i, 1);
			delete this.attribute_locations[attribute_name];
		}	

	}

	this.loaded = true;
};


function shaderCreate() {
		const shader = new L0ShaderImp();
		handleAlloc(shader, HANDLE_SHADER);
		return shader;
}

function L0Event(_type) {
	this.x = 0;
	this.y = 0;
	this.type = _type;
	this.key_code   = 0;
	this.key_string = null;
}

function L0Renderable(t, vi) {
	this.texture0     = t;
	this.vertex_index = vi;
	this.vertex_count = 0;
}

function L0MeshImp() {
	this.id           = 0;
	this.loaded       = false;
	this.is_custom    = false;
	this.buffer_dirty = false;
	this.buffer       = null;

	this.gl_id        = null;   // buffer the mesh is loaded into
	this.vertex_count = 0;      // number of verticies
	this.stride       = 0;      // byte stride between verticies 
	this.fields       = {};     // names of fields suppported and float counts
	this.update_frequency = 0;  // 0 = never, 1 = once per frame, 2 = multiple times per frame
}

L0MeshImp.prototype.addFloatField = function(field_name, offset, entries) {
	this.fields[field_name] = {'offset': offset, 'count': entries};
};


L0MeshImp.prototype.initAsFullScreen = function()
{
	this.addFloatField('a_pos',   0,  3);
	this.addFloatField('a_uv',    12, 2);
	this.addFloatField('a_norm',  20, 3);
	this.addFloatField('a_color', 32, 4);

	// have to do this because gl_VertexId is not available
	const fullscreen_data = new Float32Array(12 * 3);
	fullscreen_data[12]   = 2;
	fullscreen_data[24+1] = 2;

	const buffer_gl = m_gl.createBuffer();
	s_debug_mesh_count++;
	m_gl.bindBuffer(m_gl.ARRAY_BUFFER, buffer_gl);
	m_gl.bufferData(m_gl.ARRAY_BUFFER, fullscreen_data, m_gl.STATIC_DRAW);

	this.gl_id        = buffer_gl;
	this.vertex_count = 3;
	this.loaded       = true;
	this.stride       = 12 * 4;
};


function meshCreate() {
	var mesh = new L0MeshImp();
	handleAlloc(mesh, HANDLE_MESH);
	return mesh;
}

/////////////////////////////////////////////////
// Surface Implementation

function L0SurfaceImp()
{
	this.id     = 0;
	this.width  = 0;
	this.height = 0;
	this.images = [];
	this.color_texture_count = 1;
	this.clear_color = {'r' :  0, 'g' : 0, 'b' : 0, 'a' : 1};
	// this is actually a l0_graphics reference
	this.gl_id  = null;
}

L0SurfaceImp.prototype.bind = function() {
	this.gl_id.bind();

	// do we need to address the default frme buffer?
	if (this.color_texture_count != 1)
	{
	var draw_buffers = [];
	for (var i = 0 ; i < this.color_texture_count; i++) {
		draw_buffers.push(m_gl.COLOR_ATTACHMENT0 + i);	//COLOR_ATTACHMENT0_EXT + i);
	}

	m_gl.drawBuffers(draw_buffers);
	}
}

L0SurfaceImp.prototype.bindAndClear = function() {
	//this.gl_id.bind();
	this.bind();
	this.gl_id.clear(this.clear_color);
}



L0SurfaceImp.prototype.clear = function() {
	this.gl_id.clear(this.clear_color);
}



/////////////////////////////////////////////////
// Image Implementation

function L0ImageImp() {
	this.id     = 0;
	this.last_use = 0;
	this.url    = '';
	this.gl_id  = null;
	this.gl_renderbuffer_id = null;
	this.width  = 0;
	this.height = 0;
	this.loaded = false;
	this.mode_locked = false;
	this.mode   = 0;  // linear
	this.has_mip_chain = false;
}

L0ImageImp.prototype.setMode = function(m) {
	if (!this.mode_locked && this.mode != m) {
		if (m == 2 && !this.has_mip_chain) {
			m = 0;
		}

		if (m === 0) {
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.LINEAR);
		}
		else if (m === 1) {
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.NEAREST);
		}
		else if (m == 2) {
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.NEAREST_MIPMAP_LINEAR);
		}
		this.mode = m;
	}
}

L0ImageImp.prototype.destroy = function() {
	// delete from the asset map
	if (this.url !== 'internal') {
		delete m_asset_map[this.url];
	}
	
	// finally just destro the texture associated
	if (this.gl_id != null) {
		m_gl.deleteTexture(this.gl_id);
	}
	if (this.gl_renderbuffer_id != null) {
		m_gl.deleteRenderbuffer(this.gl_renderbuffer_id);
	}
	this.gl_id              = null;
	this.gl_renderbuffer_id = null;
}
////////////////////////////////////////////////////////////
// STATE 
////////////////////////////////////////////////////////////

const MAIN_MODULE = "org.iglang.layer0.L0";

const MOUSE_DOWN  = 0x11;
const MOUSE_UP    = 0x10;
const MOUSE_MOVE  = 0x14;

const KEY_PRESSED    = 0x21;
const KEY_RELEASED   = 0x20;

var m_update_count = 0;
var m_vm_object   = null;
var s_stop = false;
var m_element_id = null;
var m_gl = null;

var m_font_cache   = {};
//var m_render_queue = [];
var m_event_queue  = [];
//var m_font_images  = [];
var m_surfaces     = [ null ];
var m_sound_sources = [ null ];
var m_asset_map    = {};


// holds the current view and view inverse matrix
var  m_view_matrix      = null;
var  m_view_matrix_inv  = null;

// used when the model matrix is not meant to be usd
var m_identity_matrix = null;

// holds the current model matrix 
var m_model_matrix = null;

// used for creating the model matrix using only 2D affine data
var m_y_plane      = 0.0;

var m_shader_default  = null;
var m_surface_default = null;
var m_active_shader   = null;
var m_active_shader_id = -2;
const m_global_uniforms = new Array(1024);

var s_revision = 0;

////////////////////////////////////////////////////////////
// IGVM Callbacks
////////////////////////////////////////////////////////////

// what width and height should be returned in this ontext?

function fn_nameToId(name) {
	name = IGVM.stringToNative(name);
	return nameToId(name);
}

function fn_width()      { return 0 | m_gl.canvas.width; } // was clientWidth
function fn_height()     { return 0 | m_gl.canvas.height; } // was clientHeight
function fn_eventPoll(e) { 
	if (m_event_queue.length == 0) { return 0; }
	var event = m_event_queue.shift();
	e.m_type  = event.type;
	e.m_x     = event.x;
	e.m_y     = event.y;
	e.m_key_code = event.key_code;
	return 1;
}
function fn_time() { return Date.now(); }

//application/octet-stream
// see https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Sending_and_Receiving_Binary_Data


function fn_httpRequestGetQueryString(req_id) {
	var qs =  handleResolveStrict(req_id, HANDLE_HTTPREQUEST);
	return IGVM.nativeToString(qa);
}

function fn_httpRequest(query_string, 
		post_mime, post_data, post_start, post_length) 
{    
	query_string = IGVM.stringToNative(query_string);

	const req = new XMLHttpRequest();
	const connection_wrapper = { 
		"connection" : req, 
		"id"         : 0,
		"query_string" : query_string
	};
	var handle_id = handleAlloc(connection_wrapper, HANDLE_HTTPREQUEST);


	if (post_length != 0) {    
		post_mime    = IGVM.stringToNative(post_mime);

		req.open("POST", query_string, true);
		req.setRequestHeader("Content-Length", post_length);
		req.setRequestHeader('Content-Type',   post_mime);
	 
		req.responseType = "arraybuffer";
	}
	else
	{
		req.open("GET", query_string, true);
		req.responseType = "arraybuffer";
	}

	// what about the response code?  where does that come from
	function failure(event) {
		try
		{
			IGVM.call(MAIN_MODULE, "httpResponse", connection_wrapper.id, 0, IGVM.OBJECT_NULL);
		}
		catch (e) 
		{

			console.error("HttpRequest::failure Unhandled exception");			
			console.error(e);
			console.error(IGVM.stringToNative(e.m_message));
			s_stop = true;
			//throw e;
		}
	}

	function success(event) {
		var array_buffer = req.response;
		if (!array_buffer) {
			failure(event);
		}
		
		try 
		{
			var ba = IGVM.nativeArrayBufferToByteArray(array_buffer);  
			IGVM.call(MAIN_MODULE, "httpResponse", connection_wrapper.id, 200, ba);
		}
		catch (e) 
		{

			console.error("HttpRequet::success Unhandled exception");			
			console.error(e);
			console.error(IGVM.stringToNative(e.m_message));
			s_stop = true;
			//throw e;
		}
	};


	req.addEventListener("load",  success);
	req.addEventListener("error", failure);
	req.addEventListener("abort", failure);

	if (post_length != 0) {    
		throw "Post not yet supported";
	}
	else {
		req.send(null);
	}
	return handle_id;
}


///////////////////////////////////////////////////////////////////
// Graphics Group
///////////////////////////////////////////////////////////////////

function fn_setViewAffine3D(affine3d) { 

	var m = m_view_matrix;

	m[0]  = IGVM.getField(affine3d, "m00");
	m[1]  = IGVM.getField(affine3d, "m10");
	m[2]  = IGVM.getField(affine3d, "m20");
	
	m[4]  = IGVM.getField(affine3d, "m01");
	m[5]  = IGVM.getField(affine3d, "m11");
	m[6]  = IGVM.getField(affine3d, "m21");
	
	m[8]  = IGVM.getField(affine3d, "m02");
	m[9]  = IGVM.getField(affine3d, "m12");
	m[10] = IGVM.getField(affine3d, "m22");

	m[12] = IGVM.getField(affine3d, "m03");
	m[13] = IGVM.getField(affine3d, "m13");
	m[14] = IGVM.getField(affine3d, "m23");

	mat4_invert(m_view_matrix_inv, m);

	rebuildProjectionMatrix();
}

function fn_setAffine2DYPlane(yp) {
	m_y_plane = yp;
}

function fn_setAffine3D(affine3d) 
{
	if (IGVM.isNull(affine3d)) {
		mat4_identity(m_model_matrix);
		setGlobalMatrix(NAME_u_model, m_model_matrix);
	}
	else
	{
		populateMatrixFromAffine3D(m_model_matrix, affine3d);
		setGlobalMatrix(NAME_u_model, m_model_matrix);
	}
}

function fn_setAffine2Df(m00, m01, m02, m10, m11, m12) {
	m_model_matrix[0] = m00;
	m_model_matrix[1] = 0;
	m_model_matrix[2] = m10;
	m_model_matrix[3] = 0;

	m_model_matrix[4] = 0;
	m_model_matrix[5] = 1;
	m_model_matrix[6] = 0;
	m_model_matrix[7] = 0;
	
	m_model_matrix[8] = m01;
	m_model_matrix[9] = 0;
	m_model_matrix[10] = m11;
	m_model_matrix[11] = 0;
	
	m_model_matrix[12] = m02;
	m_model_matrix[13] = m_y_plane;
	m_model_matrix[14] = m12;
	m_model_matrix[15] = 1;

	setGlobalMatrix(NAME_u_model, m_model_matrix);
}


function fn_imageCreateFromText(family, sz, str) {
	
	family = IGVM.stringToNative(family);
	str    = IGVM.stringToNative(str);


	// this cache needs to be purged on the regular
	const url = family + ":" + sz + ":" + str;
	if (m_asset_map.hasOwnProperty(url)) {
		var font_image = m_asset_map[url];
		font_image.last_use = m_update_count;
		return font_image.id;
	}

	const img = new L0ImageImp();
	loadTextureFromFont(family, sz, str, img);
	img.last_use  = m_update_count;
	img.url       = url;

	const img_handle = handleAlloc(img, HANDLE_IMAGE);
	
	img.last_use  = m_update_count;
	img.url       = url;
	//m_font_images.push(img);
	m_asset_map[url] = img;
	return img_handle;
}


function fn_fontWidth(family, sz, str) {
	family = IGVM.stringToNative(family);
	str    = IGVM.stringToNative(str);

	return getTextWidth(family, sz, str);
}
	
/////////////////////////////////////////////////////////////////
// Shader Group
/////////////////////////////////////////////////////////////////

function fn_shaderLoad(url) {
	url = IGVM.stringToNative(url);
	if (m_asset_map.hasOwnProperty(url)) {
		return m_asset_map[url].id;
	}
	const shader   = shaderCreate();
	shader.url = url;
	assetRequest(url, shader, 
		function(ret_shader, array_buffer) 
		{
			const char_array = new Uint8Array(array_buffer, 0, array_buffer.byteLength);
			const json       = String.fromCharCode.apply(null, char_array);
			
			trace ("shader: " + url);

			try{

				const source = JSON.parse(json); 

				ret_shader.initFromJson(source);		
				ret_shader.loaded = true;			
			}
			catch (ex) 
			{
				console.error("JSON parse failed:");
				console.error(json);
				s_stop = true;
				throw ex;				
			}

		}
	);

	m_asset_map[url] = shader;
	return shader.id;
}

function fn_shaderLoaded(shader_id) {
	return handleResolveStrict(shader_id, HANDLE_SHADER).loaded;
}

function fn_shaderBind(shader_id) {

	if (shader_id === -1) {
		m_active_shader = m_shader_default;
		m_active_shader.bind();
		m_active_shader_id = -1;
	
		s_revision ++;

	}
	else {
		if (shader_id !== m_active_shader_id)
		{
			var shader      = handleResolveStrict(shader_id, HANDLE_SHADER);
			m_active_shader_id = shader_id;
			m_active_shader = (shader.loaded) ? shader : null; 
			if (m_active_shader !== null) {
				m_active_shader.bind();
			}

			s_revision ++;
		}
	}

}
	
/////////////////////////////////////////////////////////////////
// Surface Group
/////////////////////////////////////////////////////////////////


function fn_surfaceCreate(color_textures, depth_texture )
{
	if (IGVM.isNull(color_textures)) {
		IGVM.throwException("Arguments to surfaceCreate cannot be null");
	}
	const gl = m_gl;
	color_textures = IGVM.arrayToInt32Array(color_textures);

	var s = new L0SurfaceImp();
	handleAlloc(s, HANDLE_SURFACE);

	const gl_id= gl.createFramebuffer();

  	gl.bindFramebuffer( gl.FRAMEBUFFER, gl_id );

  	s.color_texture_count = color_textures.length;


  	if (color_textures.length > 1) {
  		console.log("MRT");
  		console.log(color_textures);
  	}

	for (var i = 0; i < color_textures.length; i++) {
		const image = handleResolveStrict(color_textures[i], HANDLE_IMAGE);


		if (i == 0) {
			console.log("create frambuffer: " + image.width + " " + image.height);
			s.gl_id = l0_graphics.createFramebuffer(gl_id, image.width, image.height);
	
		}

		//console.log("Color image");
		//console.log(image);


		if (image.gl_id !== null) {
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0 + i, 
				 gl.TEXTURE_2D, image.gl_id, 0);
		}
		else {
			IGVM.throwException("Expecting L0Image of type texture for color attachment: " + i);	
		}
	}


	if (depth_texture !== 0)
	{
		const depth_image = handleResolveStrict(depth_texture, HANDLE_IMAGE);

		//console.log("Deppth image");
		//console.log(depth_image);


		if (depth_image.gl_renderbuffer_id !== null) {
			gl.framebufferRenderbuffer(
		          gl.FRAMEBUFFER,
		          gl.DEPTH_ATTACHMENT,
		          gl.RENDERBUFFER,
		          depth_image.gl_renderbuffer_id);
		}
		else {
			gl.framebufferTexture2D( 
				gl.FRAMEBUFFER,
				gl.DEPTH_ATTACHMENT, 
				gl.TEXTURE_2D, 
				depth_image.gl_id, 
				0 );
		}
	}


	const status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
	gl.bindFramebuffer( gl.FRAMEBUFFER, null);

  	if (status != gl.FRAMEBUFFER_COMPLETE) {//} && !gl.isContextLost()) {
  		let status_string = "Unknown";
  		
  		if (status == gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT) status_string = "The attachment types are mismatched or not all framebuffer attachment points are framebuffer attachment complete.";
		if (status == gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT) status_string = "There is no attachment.";
		if (status == gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS) status_string = "Height and width of the attachment are not the same.";
		if (status == gl.FRAMEBUFFER_UNSUPPORTED) status_string = "The format of the attachment is not supported or if depth and stencil attachments are not the same renderbuffer.";
		if (status == gl.FRAMEBUFFER_INCOMPLETE_MULTISAMPLE) status_string = "The values of gl.RENDERBUFFER_SAMPLES";
  		if (status == gl.RENDERBUFFER_SAMPLES) status_string = "RENDERBUFFER_SAMPLES are different among attached renderbuffers, or are non-zero if the attached images are a mix of renderbuffers and textures.";
  		
  		console.log("Invalid Surface configuration: " + status_string);
  		IGVM.throwException("Invalid Surface configuration: " + status_string);
  	}

	// think the spec should be a key into a JSON dump that 
	// defines surfaces

	//console.log("Created surface: " + s.id);
	return s.id;
}
function fn_surfaceWidth(surface_id ) {
	handleResolveStrict(surface_id, HANDLE_SURFACE).width;
}
function fn_surfaceHeight(surface_id ) {
	handleResolveStrict(surface_id, HANDLE_SURFACE).height;
}
function fn_surfaceSetClearColor(surface_id, r,g,b,a ) {
	handleResolveStrict(surface_id, HANDLE_SURFACE).clear_color = {'r' : r, 'g' : g, 'b' : b, 'a' : a};
}
function fn_surfaceGetImage(surface_id , idx)
{
	const images = handleResolveStrict(surface_id, HANDLE_SURFACE).images;
	if (idx < 0 || idx >= images) {
		IGVM.throwException("Invalid image index");
	}
	return images[idx];
}
function fn_surfaceBind(surface_id) {
	
	if (surface_id == -1) {
		m_surface_default.bind();
	}
	else {
		handleResolveStrict(surface_id, HANDLE_SURFACE).bind();
	}
}

function fn_surfaceClear(surface_id) {
	if (surface_id == -1) {
		m_surface_default.clear();
	}
	else {
		handleResolveStrict(surface_id, HANDLE_SURFACE).clear();
	}
}

/////////////////////////////////////////////////////////////////
// Mesh Group
/////////////////////////////////////////////////////////////////

	// TODO have a production vs dev mode

function fn_meshLoad(url) {

	url = IGVM.stringToNative(url);
	if (m_asset_map.hasOwnProperty(url)) {
		return m_asset_map[url].id;
	}
	var img   = meshCreate();
	assetRequest(url, img, 
		function(ret_mesh, array_buffer) 
		{
			loadMeshFromArrayBuffer(array_buffer, ret_mesh);
		}
	);

	m_asset_map[url] = img;
	return img.id;
}

function fn_meshLoadFrom(byte_array) 
{
	var ab = IGVM.getByteArrayAsArrayBuffer(byte_array); 

	var img   = meshCreate();
	loadMeshFromArrayBuffer(img, ab);
	return img.id;
}

function fn_meshLoaded(mesh_id) {
		return handleResolveStrict(mesh_id, HANDLE_MESH).loaded;
}


/**
 * Create a mesh with a particular format string
 * 3f:a_pos,4f:a
 */
function fn_meshCreate(format_string, update_frequency) 
{

	const mesh = meshCreate();

	var posn = 0;
	format_string = IGVM.stringToNative(format_string).split(",");
	for (var i = 0; i < format_string.length; i++) {
		const parts = format_string[i].split(':');
		if (parts[0].length != 2) {
			IGVM.throwException("Super shitty length format string");
		}

		const entry_count = parts[0].charCodeAt(0) - '0'.charCodeAt(0);
		if (parts[0].charCodeAt(1) != 'f'.charCodeAt(0)) {
			IGVM.throwException("Shitty format string");
		}

		// a key followed the format specifier
		if (parts.length == 2) {
				mesh.addFloatField(parts[1], posn, entry_count);
		}
		posn += entry_count * 4;
	}

	if (0 === posn) {
		IGVM.throwException("Mesh cannot have a stride of zero");
	}

	const buffer_gl = m_gl.createBuffer();
	s_debug_mesh_count ++;
	m_gl.bindBuffer(m_gl.ARRAY_BUFFER, buffer_gl);
	m_gl.bufferData(m_gl.ARRAY_BUFFER, new Float32Array(0), m_gl.STATIC_DRAW);
	m_gl.bindBuffer(m_gl.ARRAY_BUFFER, null);
	mesh.gl_id        = buffer_gl;

	mesh.vertex_count = 0;
	mesh.is_custom    = true;
	mesh.loaded       = false;
	mesh.stride       = posn;
	mesh.update_frequency = update_frequency;

	return mesh.id;
}

function fn_meshSetVerticiesFromFloatArray(mesh_id, fa, count) {
	var ab = IGVM.arrayToNativeArrayBuffer(fa);

	var mesh = handleResolveStrict(mesh_id, HANDLE_MESH);

	//if (mesh.vertex_count != 0) {
	//	IGVM.throwException("meshAddVerticiesFromFloatArray is not fully flushed out");
	//}

	if (!mesh.is_custom) {
		IGVM.throwException("Cannot modify a mesh loaded from disk.");
	}

	//const verticies_to_add =  Math.floor(ab.byteLength / mesh.stride);
	//console.log("ADDING VERTICIES " + verticies_to_add + " " + ab.byteLength + " " + mesh.stride);

	m_gl.bindBuffer(m_gl.ARRAY_BUFFER, mesh.gl_id);
	m_gl.bufferData(m_gl.ARRAY_BUFFER, ab, (mesh.update_frequency == 0) ? m_gl.STATIC_DRAW : m_gl.DYNAMIC_DRAW);
	m_gl.bindBuffer(m_gl.ARRAY_BUFFER, null);

	mesh.loaded = true;
	mesh.vertex_count = count;
}

/**
 * Add verticies to this mesh from another mesh
 */

function fn_meshAddVerticiesFromMesh(mesh_id, src_mesh_id, affine3d)
{
	var mesh = handleResolveStrict(mesh_id, HANDLE_MESH);
	if (!mesh.is_custom) {
		IGVM.throwException("Cannot modify a static mesh");
	}


	var src_mesh = handleResolveStrict(src_mesh_id, HANDLE_MESH);
	
	var mat = mat4_create();
	populateMatrixFromAffine3D(mat, affine3d);

	IGVM.throwException("Need to fill out all of the messy details");
}

/**
 * Reset the vertex count of a given mesh to zero
 */

function fn_meshClear(mesh_id) {
	var mesh = handleResolveStrict(mesh_id, HANDLE_MESH);
	if (!mesh.is_custom) {
		IGVM.throwException("Cannot destroy a non custom mesh");
	}
	mesh.vertex_count = 0;
}

/**
 * Destory a user created mesh and all of its backing data
 */

function fn_meshDestroy(mesh_id) {
	var mesh = handleResolveStrict(mesh_id, HANDLE_MESH);
	if (!mesh.is_custom) {
		IGVM.throwException("Cannot destroy a non custom mesh");
	}

	handleDestroy(mesh_id, HANDLE_MESH);
	console.log("fn_meshDestroy TODO");
}



function GlobalEntry (name, type) {
	this.m_type      = type;
	this.m_matrix    = new Float32Array(16);
	this.m_image_id  = 0;
	this.m_revision  =-1;
}


function _getGlobalEntry(/*int*/ name, type) {
	//name = nameToId(name);
	if (!m_global_uniforms[name]) {
		m_global_uniforms[name] = new GlobalEntry(type);
	}
	const gu = m_global_uniforms[name];
	gu.m_type = type;

	return gu;
}

function setGlobalMatrix(name, mat) {
	const gu = _getGlobalEntry(name, 0x20);
	gu.m_matrix = mat;
	gu.m_revision = -1;
}

function fn_setGlobalAffine3D(name, affine3d) {
	const gu = _getGlobalEntry(name, 0x20);
	populateMatrixFromAffine3D(gu.m_matrix,affine3d);
	gu.m_revision = -1;
}

function fn_setGlobalUniform(name, type, v0, v1,v2,v3) {
	setGlobalUniform(name, type,v0,v1,v2,v3);
}

function fn_setGlobalImage(name, image_id) {
	
	const gu = _getGlobalEntry(name, 0x10);
	gu.m_image_id = image_id;

	const gu2 = _getGlobalEntry(name + 512, 0x10);
	gu2.m_image_id = image_id;
}

function setGlobalUniform(name, type, v0, v1,v2,v3) 
{
	const gu = _getGlobalEntry(name, type);
	if (gu.m_matrix[0] !== v0 ||
		gu.m_matrix[1] !== v1 ||
		gu.m_matrix[2] !== v2 ||
		gu.m_matrix[3] !== v3 ||
		gu.m_type !== type)
	{
		gu.m_matrix[0] = v0;
		gu.m_matrix[1] = v1;
		gu.m_matrix[2] = v2;
		gu.m_matrix[3] = v3;
		gu.m_type = type;
		gu.m_revision = -1;
	}
}


/////////////////////////////////////////////
// Images
/////////////////////////////////////////////

function fn_imageCreate(w, h, format) {
	const img   = new L0ImageImp();
	img.width  = w;
	img.height = h;
	populateTexture(img, 'internal', w, h, format, null);
	return handleAlloc(img, HANDLE_IMAGE);
}

function fn_imageLoad(url) {
	url = IGVM.stringToNative(url);
	if (m_asset_map.hasOwnProperty(url)) {
		return m_asset_map[url].id;
	}
	var img   = new L0ImageImp();
	loadTextureFromURL(m_gl, url, img);
	
	m_asset_map[url] = img;
	return handleAlloc(img, HANDLE_IMAGE);
}
function fn_imageSetSpanFromFloatArray(image_id, layer, x0, y0, pixel_count, data, data_offset)
{
	var img = handleResolveStrict(image_id, HANDLE_IMAGE);

	//console.log("maybe 1");
	m_gl.bindTexture(m_gl.TEXTURE_2D, img.gl_id);
	m_gl.texSubImage2D(
      m_gl.TEXTURE_2D, layer, x0, y0, pixel_count, 1,
      m_gl.RGBA, m_gl.FLOAT, data, data_offset
    );		// https://developer.mozilla.org/en-US/docs/Web/API/ArrayBufferView with webgl2

	m_gl.bindTexture(m_gl.TEXTURE_2D, null);
	//console.log("..");
}

function fn_imageLoaded(image_id) {
	return handleResolveStrict(image_id, HANDLE_IMAGE).loaded;
}
function fn_imageWidth(image_id) {
	return handleResolveStrict(image_id, HANDLE_IMAGE).width;
}
function fn_imageHeight(image_id) {
	return handleResolveStrict(image_id, HANDLE_IMAGE).height;
}
//ffunction fn_imageDraw(image_id, w, h) {
//	imageDraw(handleResolveStrict(image_id, HANDLE_IMAGE), 0, 0, w, h);
//}

function fn_exit() {
	s_stop = true;
}

function fn_getConfig(key, defaux) {
	key    = IGVM.stringToNative(key);

	if (!m_config.hasOwnProperty(key)) { return defaux; }
	return IGVM.nativeToString('' + m_config[key]);
}





/////////////////////////////////////////////////////////////////////////////////
// Internal
/////////////////////////////////////////////////////////////////////////////////

function populateMatrixFromAffine3D(m, affine3d)
{  
	m[0]  = IGVM.getField(affine3d, "m00");
	m[1]  = IGVM.getField(affine3d, "m10");
	m[2]  = IGVM.getField(affine3d, "m20");
	m[3]  = 0;
	m[4]  = IGVM.getField(affine3d, "m01");
	m[5]  = IGVM.getField(affine3d, "m11");
	m[6]  = IGVM.getField(affine3d, "m21");
	m[7]  = 0;
	m[8]  = IGVM.getField(affine3d, "m02");
	m[9]  = IGVM.getField(affine3d, "m12");
	m[10] = IGVM.getField(affine3d, "m22");
	m[11] = 0;
	m[12] = IGVM.getField(affine3d, "m03");
	m[13] = IGVM.getField(affine3d, "m13");
	m[14] = IGVM.getField(affine3d, "m23");
	m[15] = 1;
}

function logMat4(m) {
	for (let x = 0; x < 4; x++) {
		console.log("[", + m[x+0] + "," + m[x+4] + "," +m[x+8] + "," + m[x+12] + "]");
	}
}


function rebuildProjectionMatrix() {
	if (m_camera_perspective) {
		mat4_perspective(m_proj_matrix,
								 m_camera_fov,
								 m_camera_aspect,
								 m_camera_near,
								 m_camera_far);
	}
	else {
		 mat4_ortho(m_proj_matrix, m_camera_l,
		 					 m_camera_r,
		 					 m_camera_b,
		 					 m_camera_t,
		 					 m_camera_near,
		 					 m_camera_far);
	}
	// ha this actually worked
	mat4_multiply(m_proj_view_inv_matrix, m_proj_matrix, m_view_matrix_inv);

	setGlobalMatrix(NAME_u_view,      m_view_matrix_inv);
	setGlobalMatrix(NAME_u_proj,      m_proj_matrix);
	setGlobalMatrix(NAME_u_proj_view, m_proj_view_inv_matrix);

}

function fn_projectionSetNearFar(n, f) {
	m_camera_near = n;
	m_camera_far = f;
	setGlobalUniform(NAME_u_camera_near, 0x01, n, 0,0,0);
	setGlobalUniform(NAME_u_camera_far,  0x01, f, 0,0,0);

	rebuildProjectionMatrix();
}

function fn_perspectiveSetAspectAndFov(a, f) {
	m_camera_aspect = a;
	m_camera_fov = f;
	m_camera_perspective = true;
	setGlobalUniform(NAME_u_camera_aspect, 0x01, a, 0,0,0);
	setGlobalUniform(NAME_u_camera_fov,  0x01, f, 0,0,0);
	rebuildProjectionMatrix();
}

function fn_orthoSetBounds(l,r,b,t) {
	m_camera_l = l;
	m_camera_r = r;
	m_camera_b = b;
	m_camera_t = t;
	m_camera_perspective = false;
	rebuildProjectionMatrix();
}




function fn_meshDrawN(mesh_id, texture_arr, texture_count, mode)
{

	const mesh    = handleResolveStrict(mesh_id, HANDLE_MESH);

	if (!mesh.loaded) {
		//console.log("mesh not loaded");
		return; // NOP
	}

	const gl = m_gl;
	const program = m_active_shader;
	if (program === null || !program.loaded) {
		//console.log("program is not yet loaded");
		return;
	}

	// this might not make any sense
	if (mesh.buffer_dirty)
	{
		m_gl.bindBuffer(m_gl.ARRAY_BUFFER, mesh.gl_id); 
		m_gl.bufferData(m_gl.ARRAY_BUFFER, mesh.buffer, m_gl.DYNAMIC_DRAW);
		mesh.buffer_dirty = false;
	}


	// Mark existing attributes	and textyres
	l0_graphics.activeMark();


	// Configure the attribute data
	///////////////////////////////

	for (let i = program.attribute_names.length - 1; i >= 0; i--) {
		const attribute_name = program.attribute_names[i];
		if (!mesh.fields.hasOwnProperty(attribute_name)) 
		{
			// should we apply a default if it isn't supported by the mesh
			// or should the mesh loader just give all of the values?
			continue;
		}

		const field = mesh.fields[attribute_name];

		l0_graphics.activeAttribute(program.attribute_locations[attribute_name]);
		m_gl.bindBuffer(m_gl.ARRAY_BUFFER, mesh.gl_id); 
		m_gl.vertexAttribPointer(
			program.attribute_locations[attribute_name],
			field.count, m_gl.FLOAT, false, mesh.stride, field.offset);
	}


	
	s_debug_draw_uniform_acc += configureUniforms(program, texture_arr);

	// disable anything that isn't in use
	///////////////////////////////////
	l0_graphics.activeClean();

	if (mode === 3) {
		m_gl.drawArrays(m_gl.TRIANGLES, 0, mesh.vertex_count);
	}
	else if (mode === 1) {
		m_gl.drawArrays(m_gl.POINTS, 0, mesh.vertex_count);
	}
	else {
		IGVM.throwException('Invalid draw mode.  1 or 3 expected');
	}

	s_debug_draw_count_acc++;
}

// split this off for separate testing
function configureUniforms(program, texture_arr) 
{
	let uniforms_assigned = 0;
	let max_texture_unit = 7;

	// Configure the uniform data
	//////////////////////////////
	for (let i = program.uniform_names.length - 1; i >= 0; i--)
	{
		const uniform_name_id  = program.uniform_name_ids[i];
		const gu               = m_global_uniforms[uniform_name_id];
		if (gu !== null && typeof gu !== 'undefined')
		{
			// this cuts the bindings that occur in half
			if (gu.m_revision === s_revision) {
				continue;
			}

			gu.m_revision = s_revision;
		}

		uniforms_assigned ++;

		const uniform_name     = program.uniform_names[i];
		const uniform_location = program.uniform_locations[i];


		switch (uniform_name_id)
		{
			case 0:
				// do nothing.. it was not found in the source so it got zerod out
				break;

			case  NAME_u_texture0:
			case  NAME_u_texture1:
			case  NAME_u_texture2:
			case  NAME_u_texture3:
			case  NAME_u_texture4:
			case  NAME_u_texture5:
			case  NAME_u_texture6:
			case  NAME_u_texture7:
			{
				const texture_unit = uniform_name_id - NAME_u_texture0;
				const image        = handleResolveStrict(texture_arr[texture_unit], HANDLE_IMAGE);

				l0_graphics.activeTexture(texture_unit, m_gl.TEXTURE_2D, image.gl_id);
				m_gl.uniform1i(uniform_location, texture_unit);
				
				if (uniform_name.endsWith(":NEAREST")) {
					image.setMode(1);
				}
				else if (uniform_name.endsWith(":MIP_LINEAR")) {
					image.setMode(2);
				}
				else {
					image.setMode(0);
				}
			
				break;
			}

			case  NAME_u_texture0_dim:
			case  NAME_u_texture1_dim:
			case  NAME_u_texture2_dim:
			case  NAME_u_texture3_dim:
			case  NAME_u_texture4_dim:
			case  NAME_u_texture5_dim:
			case  NAME_u_texture6_dim:
			case  NAME_u_texture7_dim:
			{
				const texture_unit = uniform_name_id - NAME_u_texture0_dim; 
				const image        = handleResolveStrict(texture_arr[texture_unit], HANDLE_IMAGE);

				m_gl.uniform2f(uniform_location, image.width, image.height);
				
				break;
			}

			default: 
			{

				if (null == gu) {
					throw new Error("Global doesn't exist crashy crashy: " + uniform_name);
				}

				switch (gu.m_type)
				{
					case 1 :
						m_gl.uniform1f(uniform_location, gu.m_matrix[0]);
						break;
					case 2: 
						m_gl.uniform2f(uniform_location, gu.m_matrix[0],gu.matrix[1]);
						break;
					case 3: 
						m_gl.uniform3f(uniform_location, gu.m_matrix[0],gu.m_matrix[1],gu.m_matrix[2]);
						break;
					case 4: 
						m_gl.uniform4f(uniform_location, gu.m_matrix[0],gu.m_matrix[1],
								gu.m_matrix[2],gu.m_matrix[3]);
						break;
					case 0x10: 
					{
						// this isnt able to handle modifiers on these globals
						//console.log("assigning globall uniform");
						const image = handleResolveStrict(gu.m_image_id, HANDLE_IMAGE);

						if (uniform_name.endsWith("_dim"))
						{
							m_gl.uniform2f(uniform_location, image.width, image.height);
						}
						// the thing can be tagged with a image mode
						else
						{
							const texture_unit = max_texture_unit;
							max_texture_unit--;

							l0_graphics.activeTexture(texture_unit, m_gl.TEXTURE_2D, image.gl_id);
							m_gl.uniform1i(uniform_location, texture_unit);
							
							if (uniform_name.endsWith(":NEAREST")) {
								image.setMode(1);
							}
							else if (uniform_name.endsWith(":MIP_LINEAR")) {
								image.setMode(2);
							}
							else {
								image.setMode(0);
							}
						}
						break;
					}
					case 0x20:
						m_gl.uniformMatrix4fv(uniform_location, false, gu.m_matrix);
						break;
				}
			}
		} // end of switch
	} // end of loop

	return uniforms_assigned;
}



function resize(gl) {

	/*
	//https://webglfundamentals.org/webgl/lessons/webgl-resizing-the-canvas.html
	var realToCSSPixels = window.devicePixelRatio;
	// reports correct number for my scaled in moe

	console.log("canvas: " + m_gl.canvas.width + "x" + m_gl.canvas.height);
	console.log("canvas-client-height" + m_gl.canvas.clientWidth + "x" + m_gl.canvas.clientHeight );
	console.log("pixel ratio: " + window.devicePixelRatio);
	// Lookup the size the browser is displaying the canvas in CSS pixels
	// and compute a size needed to make our drawingbuffer match it in
	// device pixels.
	var displayWidth  = Math.floor(m_gl.canvas.clientWidth  * realToCSSPixels);
	var displayHeight = Math.floor(m_gl.canvas.clientHeight * realToCSSPixels);

	console.log("calculated size: " + displayWidth + " x " + displayHeight);

	// Check if the canvas is not the same size.
	if (m_gl.canvas.width  !== displayWidth ||
			m_gl.canvas.height !== displayHeight) {

		// Make the canvas the same size
		m_gl.canvas.width  = displayWidth;
		m_gl.canvas.height = displayHeight;
	 // m_gl.canvas.clientWidth = 640;
		//m_gl.canvas.clientWidth = 480;
	}
	*/
}

var m_text_canvas = null;
var m_text_context = null;

function getTextWidth(family, sz, str)
{
	console.log("EXPENSIVE get text width: " + family + " " + sz + " " + str);
	var font = [sz, "px", family].join("")

	if (m_text_canvas == null) {
		m_text_canvas = document.createElement("canvas");  // tood verify the sytax
		m_text_context = m_text_canvas.getContext('2d');
	}
	
	m_text_context.font      = font
	m_text_context.textAlign = "left"
	m_text_context.textBaseline = "top"

	const dims = m_text_context.measureText(str);
	return dims.width;
}
// TODO integrate this with the rest of the code base
function loadTextureFromFont( family, sz,str, img) 
{    
	//console.log("loadTextureFromFont: " + family + " " + sz + " " + str);
	var font = [sz, "px", " " ,family].join("")

	if (m_text_canvas == null) {
		m_text_canvas = document.createElement("canvas");  // tood verify the sytax
		m_text_context = m_text_canvas.getContext('2d');
	}

	var drawCanvas  = m_text_canvas;  //document.createElement("canvas");  // tood verify the sytax
	var drawContext = m_text_context; //drawCanvas.getContext('2d');

	//console.log("full font identiication: " + font);
	drawContext.font         = font
	drawContext.textAlign    = "left"
	drawContext.textBaseline = "top"

	const dims = drawContext.measureText(str);
	const w = dims.width;
	const h = 2 * sz;

	// need to set up some sort of expiry... but meh
	img.width  = w;
	img.height = h;
	img.loaded = true;

	//const w = bits.nextPow2(dims.width)
	//const h = bits.nextPow2(2 * sz);

	// not sure why a bunch of this config is repeated
	drawCanvas.width = w
	drawCanvas.height = h
	drawContext.clearRect(0, 0, w, h)
	drawContext.fillStyle   = "#fff";
	drawContext.font         = font
	drawContext.textAlign    = "left"
	drawContext.textBaseline = "top"
	drawContext.fillText(str, 0, 0);

			
	
	
	const level     = 0;
	const internal_format = m_gl.RGBA;
	const width     = w;
	const height    = h;
	const border    = 0;
	const src_format = m_gl.RGBA;
	const src_type   = m_gl.UNSIGNED_BYTE;
	
	const texture = m_gl.createTexture();
	s_debug_texture_count ++;
	m_gl.bindTexture(m_gl.TEXTURE_2D, texture);
	m_gl.texImage2D(m_gl.TEXTURE_2D, level, internal_format, src_format, src_type, drawCanvas);
	m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_WRAP_S,     m_gl.CLAMP_TO_EDGE);
	m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_WRAP_T,     m_gl.CLAMP_TO_EDGE);
	m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.LINEAR);
	m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_BASE_LEVEL, 0);
	m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MAX_LEVEL,  0);
	m_gl.bindTexture(m_gl.TEXTURE_2D, null);


	img.url   = null;    // was auto generated
	img.gl_id = texture;  // tid generated

	console.log(",,");
}

function loadMeshFromArrayBuffer(array_buffer, mesh)
{
	if (!array_buffer) {
		console.log("failed to retrieve data");
	}


	const HEADER_SIZE_BYTES = 4 + 4 + (4*4) + 4;

	var u8  = new Uint8Array(array_buffer);
	var f32 = new Float32Array(array_buffer);
	var i32 = new Int32Array(array_buffer);

	const objc = 'objC';
	for (let i = 0; i < 4; i++)
	{
		if (u8[i] != objc.charCodeAt(i)) {
				console.log("bad format: " + i + " " + String.fromCharCode(u8[i]));
				return 1;
		}
	}

	if (i32[1] != 0) {
		console.log("bad version");
		return 2;
	}

	const FIELD_NAMES = [ null, 'a_pos', 'a_uv', 'a_norm', 'a_color'];
	var num_floats = 0;
	for (var j = 0; j < 4; j++) {
		var field = i32[2 + j];
		var field_entries = field & 0xf;
		if (0 === field_entries) {
			continue;
		}
		var field_type    = (field >> 4) & 0xf;
		var field_name    = FIELD_NAMES[field_type];

		mesh.addFloatField(field_name, num_floats * 4, field_entries);
		num_floats += field_entries;
	}
	mesh.stride       = num_floats * 4;
	const count  = i32[6];
	const float_array = new Float32Array(array_buffer, HEADER_SIZE_BYTES);

	const buffer_gl = m_gl.createBuffer();
	s_debug_mesh_count ++;
	m_gl.bindBuffer(m_gl.ARRAY_BUFFER, buffer_gl);
	m_gl.bufferData(m_gl.ARRAY_BUFFER, float_array, m_gl.STATIC_DRAW);

	mesh.gl_id        = buffer_gl;
	mesh.vertex_count = count; 
	mesh.loaded       = true;

	return 0;
}

function assetRequest(url, object, callback)
{
	// in production mode this should just grab the raw asset
	var full_url = '/assets' + url;	//index.php?l0-action=read&p=' + encodeURIComponent(url);

	//console.log("get request: " + full_url);

	var req = new XMLHttpRequest();
	req.open("GET", full_url, true);
	req.responseType = "arraybuffer";

	req.onload = function (event) 
	{
		if (req.status == 200 && req.response)
		{
			callback(object, req.response);
		}
	};
	req.send(null);
}

function populateTexture(img, url, width, height, format, pixel) 
{
	//https://www.youtube.com/watch?v=rfQ8rKGTVlg#t=31m42s
	
	img.url    = url;
	

	const level     = 0;


	//var ext = m_gl.getExtension('OES_texture_half_float');

	// evaluate the webgl format ot use
	var renderbuffer = false;
	var internalFormat = m_gl.RGBA;
	var srcFormat = m_gl.RGBA;
	var srcType   = m_gl.UNSIGNED_BYTE;
	const   channels = format & 0xf;
	const   storage_type = format >> 4;
	switch (storage_type) {
		case 1: {
			if (channels === 4) {
				internalFormat = m_gl.RGBA;
				srcFormat      = m_gl.RGBA;
				srcType        = m_gl.UNSIGNED_BYTE;
			}
			else if (channels === 3) {
				internalFormat = m_gl.RGB;
				srcFormat = m_gl.RGB;
				srcType = m_gl.UNSIGNED_BYTE;
			}
			break;
		}
		case 2: {
			//console.log(m_gl);
			//console.log("Half float format: " + channels + " url: " + url);
			//console.log("enums: " + m_gl.RGB16F + " " + m_gl.RGBA16F);
			if (channels === 4) {
				internalFormat = m_gl.RGBA16F;
				srcFormat      = m_gl.RGBA;
				srcType        = m_gl.HALF_FLOAT;
				//if (pixel == null) srcType =  m_gl.UNSIGNED_INT; //ext.HALF_FLOAT_OES;
			}
			else if (channels === 3) {
				internalFormat = m_gl.RGB16F;
				srcFormat = m_gl.RGB;
				srcType = m_gl.HALF_FLOAT;
				//if (pixel == null) srcType = m_gl.UNSIGNED_INT; //ext.HALF_FLOAT_OES;
			}
			break;
		}
		case 4: {
			//console.log(m_gl);
			//console.log("float format: " + channels + " url: " + url);
			//console.log("enums: " + m_gl.RGB32F + " " + m_gl.RGBA32F);
			if (channels === 4) {
				internalFormat = m_gl.RGBA32F;
				srcFormat      = m_gl.RGBA;
				srcType        = m_gl.FLOAT;
				//if (pixel == null) srcType =  m_gl.UNSIGNED_INT; //ext.HALF_FLOAT_OES;
			}
			else if (channels === 3) {
				internalFormat = m_gl.RGB32F;
				srcFormat = m_gl.RGB;
				srcType = m_gl.FLOAT;
				//if (pixel == null) srcType = m_gl.UNSIGNED_INT; //ext.HALF_FLOAT_OES;
			}
			break;
		}
		case 0xe: {
			internallFormat = m_gl.DEPTH_COMPONENT;
			srcFormt = m_gl.DEPTH_COMPONENT;
			srcType = m_gl.UNSIGNED_INT;
			break;
		}
		case 0xf: {
			renderbuffer = true;
			break;
		}
	}

	//http://www.cs.cornell.edu/courses/cs4620/2017sp/cs4621/lecture08/exhibit02.html

	if (renderbuffer)
	{
		const db = m_gl.createRenderbuffer();
		m_gl.bindRenderbuffer(m_gl.RENDERBUFFER, db);
		img.gl_renderbuffer_id = db;
		
		m_gl.renderbufferStorage(
		  m_gl.RENDERBUFFER, m_gl.DEPTH_COMPONENT16, width, height);
		m_gl.bindRenderbuffer(m_gl.RENDERBUFFER, null);
	}
	else 
	{

		// so we're somehow having issues with our float textures????
		const texture = m_gl.createTexture();
		s_debug_texture_count++;
		m_gl.bindTexture(m_gl.TEXTURE_2D, texture);


		//  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.FLOAT, data);

		m_gl.texImage2D(m_gl.TEXTURE_2D, level, internalFormat,
									width, height, 0 /*border*/, srcFormat, srcType,
									pixel);
		m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_WRAP_S,     m_gl.CLAMP_TO_EDGE);
		m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_WRAP_T,     m_gl.CLAMP_TO_EDGE);

		if (storage_type != 4)
		{
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.LINEAR);
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MAG_FILTER, m_gl.LINEAR);
		}
		else {
			// todo indiate that this mode is not changeable unless
			// OES_texture_float_linear is availble
			img.mode_locked = true;
			img.mode        = 1;
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.NEAREST);
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MAG_FILTER, m_gl.NEAREST);
		}

		m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_BASE_LEVEL, 0);
		m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MAX_LEVEL,  0);
		m_gl.bindTexture(m_gl.TEXTURE_2D, null);

		img.gl_id = texture;
	}
}

//
// Initialize a texture and load an image.
// When the image finished loading copy it into the texture.
//
function loadTextureFromURL(gl, url, img) 
{
	if (url === null || url.length == 0) {
		console.log("bad url for loadTextureFromURL: " + url);
		IGVM.throwException("Completely invalid url: " + url);
	}
	
	if (!url.startsWith('/assets/')) {
		url =  '/assets' + url;
	}

	// Because images have to be download over the internet
	// they might take a moment until they are ready.
	// Until then put a single pixel in the texture so we can
	// use it immediately. When the image has finished downloading
	// we'll update the texture with the contents of the image.
	/////////////////////////////////////////////////////////////////
	populateTexture(img, url, 1, 1, 0x14, new Uint8Array([0, 0, 0, 255]));

	const image  = new Image();
	image.onerror = function()
	{
		console.log("texture load failed: " + url);
	};


	image.onload = function() 
	{
	//	console.log("texture loaded: " + url);

		const level          = 0;
		const internalFormat = m_gl.RGBA;
		const srcFormat      = m_gl.RGBA;
		const srcType        = m_gl.UNSIGNED_BYTE;

		m_gl.bindTexture(m_gl.TEXTURE_2D, img.gl_id);
		m_gl.texImage2D (m_gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, image);

		// make the mip chain available for use
		if (image.width == image.height && isPowerOf2(image.width) && isPowerOf2(image.height)) {
			 // Yes, it's a power of 2. Generate mips.
			 m_gl.generateMipmap(m_gl.TEXTURE_2D);
			 m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.LINEAR);
			 m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MAG_FILTER, m_gl.LINEAR);
		
			 img.has_mip_chain = true;
		}
		else {
			//console.log("non power of 2 image: " + url);

			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MIN_FILTER, m_gl.LINEAR);
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MAG_FILTER, m_gl.LINEAR);
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_BASE_LEVEL, 0);
			m_gl.texParameteri(m_gl.TEXTURE_2D, m_gl.TEXTURE_MAX_LEVEL,  0);
		}



		// populate the real with and height
		img.width  = image.width;
		img.height = image.height;
		img.loaded = true;
	};

	image.src = url;
}

function isPowerOf2(value) {
	return (value & (value - 1)) == 0;
}



// assumes target or event.target is canvas
function getCanvasRelativeMousePosition(event, target,  out) {
	target = target || event.target;

	var rect = target.getBoundingClientRect();

	var pos = {
		x: event.clientX - rect.left,
		y: event.clientY - rect.top,
	};

	out.x = pos.x * target.width  / target.clientWidth;
	out.y = pos.y * target.height / target.clientHeight;
}

function cleanCache()
{
	/*
	for (var i = m_font_images.length -1; i >= 0; i--) {
		var fi = m_font_images[i];
		// l
		if (fi.last_use < m_update_count - 10) 
		{
			// put the last element in the list in this slot
			// and decrease the length of the m_font_images array
			const curr_len_m1 = m_font_images.length - 1;
			m_font_images[i] = m_font_images[curr_len_m1];
			m_font_images[curr_len_m1] = null;
			m_font_images.length = curr_len_m1;

			// free up the images slot (THIS NEEDS REWRKING)
			//m_images[fi.id] = null;
			
			var img = handleDestroy(fi.id, HANDLE_IMAGE);
			img.destroy();


		 // console.log("font image is candidate for cleanup: " + fi.url);
		}
	}
	*/
}


function scriptInclude(url) {
	const script = document.createElement( 'script' );
	script.type  = 'text/javascript';
	script.async = false;
	script.src   = url;
	document.body.appendChild( script );
	return script;
}

//////////////////////////////////////////////////////////////
// PUBLIC INTERFACE
//////////////////////////////////////////////////////////////
return {
	'load': function(path_to_scripts, callback){
		var last = null;
		last = scriptInclude(path_to_scripts+"l0_net.js");
		last = scriptInclude(path_to_scripts+"l0_graphics.js");
		last = scriptInclude(path_to_scripts+"l0_audio.js");
		
		last.onload = function() { callback(); };
	},


	'init': function(element_id, main_klass, konfig) 
	{
		//////////////////////////////////////////
		// Process the config
		///////////////////////////////////////////
		konfig = konfig || {};
		if (!konfig.hasOwnProperty('headless')) { 
			konfig['headless'] = false; 
		}
		m_config =konfig;



		// initialize internal matrix structures
		m_proj_matrix          = mat4_create();
		m_view_matrix          = mat4_create();
		m_view_matrix_inv      = mat4_create();
		m_proj_view_inv_matrix = mat4_create();
		m_identity_matrix      = mat4_create();
		m_model_matrix         = mat4_create();
		



		
		console.log("L0 initializing (?)");
		console.log("###############");

		if (!konfig['headless'])
		{
			m_element_id = element_id;
		

			const canvas = document.getElementById(element_id);

			// Initialize the GL context
			const gl = canvas.getContext("webgl2");
			m_gl = gl;

			// pull up the float texture extension
			const EXT_color_buffer_float = gl.getExtension('EXT_color_buffer_float');
			//var ext2 = gl.getExtension('EXT_color_buffer_half_float');
			//var ext3 = gl.getExtension('GL_EXT_draw_buffers');
			//var ext4 = gl.getExtension('WEBGL_draw_buffers');
			//ext2 = true;
			//ext3 = true;
			//ext4 = true;
			//var ext3 = 
			//var OES_texture_float_EXT = gl.getExtension('OES_texture_float');


			// Only continue if WebGL is available and working
			if (!gl || !EXT_color_buffer_float) {//} || !ext2 || !ext3 || !ext4) {
				alert("Unable to initialize WebGL.  WebGL2 or ColorBufferFloat extension not available.");
				s_stop = true;
				return;
			}

			//https://www.html5rocks.com/en/mobile/touch/
			canvas.ontouchmove = function(event) {
				event.preventDefault();		
				for (var i = 0; i < event.touches.length; i++) {
					var touch = event.touches[i];		
					//touch.pageX, 
					//touch.pageY	
				}
			};

			canvas.onmousedown = function (event) { 
				event.preventDefault();

				const e = new L0Event(MOUSE_DOWN);
				getCanvasRelativeMousePosition(event,canvas, e);
				m_event_queue.push(e); 
				return false;
			};
			canvas.onmouseup   = function (event) { 
				event.preventDefault();
				
				const e = new L0Event(MOUSE_UP);
				getCanvasRelativeMousePosition(event,canvas, e);
				m_event_queue.push(e); 
				return false;
			};
			canvas.onmousemove = function (event) { 
				event.preventDefault();
				
				const e = new L0Event(MOUSE_MOVE);
				getCanvasRelativeMousePosition(event,canvas, e);
				m_event_queue.push(e); 
				return false;
			};
			canvas.onmouseenter = function (event) {

			};
			canvas.onmouseexit  = function (event) {

			};
			canvas.oncontextmenu = function(event) {
				console.log("context menu");
				return false;
			};
			document.onkeydown = function(event) {
				event.preventDefault();

				// this completely retriggers like an annoying bugger
				const e = new L0Event(KEY_PRESSED);
				e.key_code  = event.keyCode; 
				m_event_queue.push(e); 
				return false;
			};
			document.onkeyup   = function(event) {
				event.preventDefault();
				const e = new L0Event(KEY_RELEASED);
				e.key_code  = event.keyCode; 
				m_event_queue.push(e); 
				return false;
			};
			canvas.onkeypress = function(event) {
				event.preventDefault();
				

				console.log("kp");
				console.log("onkeypress: " + JSON.stringify(event));

				// this might have 
			};

			/////////////////////////////////////////////////////////////////
			// IGVM callbacks
			/////////////////////////////////////////////////////////////////


			IGVM.setNative(MAIN_MODULE, "width__get",  fn_width);
			IGVM.setNative(MAIN_MODULE, "height__get", fn_height);
			IGVM.setNative(MAIN_MODULE, "eventPoll",   fn_eventPoll);
			IGVM.setNative(MAIN_MODULE, "imageCreateFromText", fn_imageCreateFromText);
			IGVM.setNative(MAIN_MODULE, "fontWidth", fn_fontWidth);
			IGVM.setNative(MAIN_MODULE, "getDebugI", fn_getDebugI);

			// mesh group

			IGVM.setNative(MAIN_MODULE, "meshLoad",   fn_meshLoad);
			// this might make more sense being accessed from somewhere else
			//IGVM.setNative(MAIN_MODULE, "meshLoadFrom",   fn_meshLoadFrom);
			IGVM.setNative(MAIN_MODULE, "meshLoaded",       fn_meshLoaded);
			IGVM.setNative(MAIN_MODULE, "setGlobalUniform", fn_setGlobalUniform);
			IGVM.setNative(MAIN_MODULE, "setGlobalImage",   fn_setGlobalImage);
			IGVM.setNative(MAIN_MODULE, "setGlobalAffine3D",  fn_setGlobalAffine3D);

			IGVM.setNative(MAIN_MODULE, "meshDrawN",   fn_meshDrawN);
			IGVM.setNative(MAIN_MODULE, "meshCreate",  fn_meshCreate);
			IGVM.setNative(MAIN_MODULE, "meshDestroy", fn_meshDestroy);
			IGVM.setNative(MAIN_MODULE, "meshSetVerticiesFromFloatArray", fn_meshSetVerticiesFromFloatArray);
			IGVM.setNative(MAIN_MODULE, "meshAddVerticiesFromMesh", fn_meshAddVerticiesFromMesh);
			IGVM.setNative(MAIN_MODULE, "meshClear",   fn_meshClear);

			IGVM.setNative(MAIN_MODULE, "surfaceCreate",   fn_surfaceCreate);
			IGVM.setNative(MAIN_MODULE, "surfaceWidth",   fn_surfaceWidth);
			IGVM.setNative(MAIN_MODULE, "surfaceHeight",   fn_surfaceHeight);
			IGVM.setNative(MAIN_MODULE, "surfaceGetImage",   fn_surfaceGetImage);
			IGVM.setNative(MAIN_MODULE, "surfaceBind",   fn_surfaceBind);
			IGVM.setNative(MAIN_MODULE, "surfaceClear",   fn_surfaceClear);
			IGVM.setNative(MAIN_MODULE, "surfaceSetClearColor", fn_surfaceSetClearColor);


			IGVM.setNative(MAIN_MODULE, "shaderLoad",   fn_shaderLoad);
			IGVM.setNative(MAIN_MODULE, "shaderLoaded", fn_shaderLoaded);
			IGVM.setNative(MAIN_MODULE, "shaderBind",   fn_shaderBind);

			IGVM.setNative(MAIN_MODULE, "imageCreate", fn_imageCreate);
			IGVM.setNative(MAIN_MODULE, "imageLoad",   fn_imageLoad);
			IGVM.setNative(MAIN_MODULE, "imageLoaded", fn_imageLoaded);
			IGVM.setNative(MAIN_MODULE, "imageWidth",  fn_imageWidth);
			IGVM.setNative(MAIN_MODULE, "imageHeight", fn_imageHeight);
			IGVM.setNative(MAIN_MODULE, "imageSetSpanFromFloatArray", fn_imageSetSpanFromFloatArray);
			IGVM.setNative(MAIN_MODULE, "setAffine3D", fn_setAffine3D);
			IGVM.setNative(MAIN_MODULE, "setViewAffine3D", fn_setViewAffine3D);
			//IGVM.setNative(MAIN_MODULE, "setTint",     fn_setTint);
			IGVM.setNative(MAIN_MODULE, "projectionSetNearFar", fn_projectionSetNearFar);
			IGVM.setNative(MAIN_MODULE, "perspectiveSetAspectAndFov", fn_perspectiveSetAspectAndFov);
			IGVM.setNative(MAIN_MODULE, "orthoSetBounds", fn_orthoSetBounds);


		}
		else {

		}
		
		// common includes
		IGVM.setNative(MAIN_MODULE, "nameToId",    fn_nameToId);
		IGVM.setNative(MAIN_MODULE, "getConfig",   fn_getConfig);
		IGVM.setNative(MAIN_MODULE, "exit",        fn_exit);
		IGVM.setNative(MAIN_MODULE, "httpRequest", fn_httpRequest);
		IGVM.setNative(MAIN_MODULE, "httpRequestGetQueryString", fn_httpRequestGetQueryString);
		IGVM.setNative(MAIN_MODULE, "time",        fn_time);
		IGVM.setNative(MAIN_MODULE, "hmac",        fn_hmac);
		


		if (konfig['headless'])
		{
			l0_net.init(IGVM, MAIN_MODULE, m_handle_manager);

			m_vm_object = IGVM.newObject(main_klass, []);
			if (!m_vm_object) {
				alert("Unable to create object");
			}


			var m_last_step = new Date().getTime();

			// call init on the object
			IGVM.callNamed("init", m_vm_object, m_last_step / 1000.0);

			m_timeout = setInterval(function() {
				try{
					const current_time = Date.now();
					//trace ("setInterval...timeout: " + (current_time/1000.0));
					IGVM.callNamed("update", m_vm_object, current_time/1000.0, (current_time - m_last_step) / 1000.0);   
					m_last_step = current_time;
				}
				catch (e) {
					console.error(e);
					console.error("Unhandled exception");
					cancelInterval(m_timeout);
					// ?? should do... exit all tgether
				}
			}, 1000/64);
		}
		else
		{
			l0_net.init(IGVM, MAIN_MODULE, m_handle_manager);

			const gl = m_gl;
			////////////////////////////////////////////////////////////////////////
			// Setup
			////////////////////////////////////////////////////////////////////////

			resize(gl);


			l0_graphics.init(gl, gl.canvas.width, gl.canvas.height);
			l0_audio  .init(IGVM, MAIN_MODULE, m_handle_manager);

			m_surface_default = new L0SurfaceImp();
			handleAlloc(m_surface_default, HANDLE_SURFACE);
			m_surface_default.width  = gl.canvas.width;
			m_surface_default.height = gl.canvas.height;
			m_surface_default.gl_id  = l0_graphics.getDefaultFramebuffer();


			fn_projectionSetNearFar(1, 256.0);
			// 90 degree fov
			fn_perspectiveSetAspectAndFov(Math.PI / 2, m_gl.canvas.width / m_gl.canvas.height);

			// create an setup the default shader
			m_shader_default = m_active_shader = shaderCreate();
			m_active_shader.initAsDefault();
			m_active_shader_id = -1;

			// create and setfup the default fullscreen mesh
			const fullscreen_mesh = meshCreate();
			fullscreen_mesh.initAsFullScreen();

			///////////////////////////////////////////////////////////////////
			// Initialize the runtime
			//////////////////////////////////////////////////////////////////
			//console.log("Creating class: " + main_klass);
			
			console.log("L0 creating main class: " + main_klass);
			m_vm_object = IGVM.newObject(main_klass, []);
			if (!m_vm_object) {
				alert("Unable to create object");
			}

			// call init on the object
			console.log("L0 Initializing main class: " + main_klass);
			IGVM.callNamed("init", m_vm_object, 0);
			//const image_h = fn_imageLoad("img.jpg");

			var m_last_step_set = false;
			var m_last_step = 0.0;

		

			function update(step) 
			{
				const update_start_time = Date.now();

				s_debug_draw_count = s_debug_draw_count_acc;
				s_debug_draw_count_acc = 0;


				s_debug_draw_uniform = s_debug_draw_uniform_acc;
				s_debug_draw_uniform_acc = 0;

				// check if its the first update
				if (!m_last_step_set) {
					m_last_step     = step;
					m_last_step_set = true;
				}

				//console.log("an update");
				IGVM.callNamed("update", m_vm_object, step/1000.0, (step - m_last_step) / 1000.0);   


				// should this be considered the final blit?
				// should there be previous steps
				m_surface_default.bindAndClear();
				for (var k = 0; k < s_default_state.length; k++) {
					s_state_current[k] = s_default_state[k];
				}
				//s_state_current = s_default_state.slice(0);
				fn_shaderBind(-1);
			
				// clear out any potential lingering state
				///////////////////////////////////////////
				l0_graphics.activeMark();
				l0_graphics.activeClean();

				// start a render pass
				IGVM.callNamed("render", m_vm_object, step/1000.0, (step - m_last_step) / 1000.0);

				m_last_step = step;
				m_update_count ++;


				//s_stats.end();

				// this would have cleaned out the texture cache
				cleanCache();

				const update_end_time = Date.now();

				IGVM.callNamed("framePost",  m_vm_object,  
						update_start_time * 1.0,
						update_end_time * 1.0);

				if (!s_stop) {
					window.requestAnimationFrame(update);
				}
			}

			window.requestAnimationFrame(update);
		}
	},


	'requestFullScreen': function()
	{
		// this completely resizes the web-gl context
		// although this behaviour might differ based on the platform
		// ie resizeing the context, or just stretching the context
		var elem = document.getElementById(m_element_id);
		if(elem.requestFullscreen) {
			elem.requestFullscreen();
		}
		else if(elem.webkitRequestFullscreen) {
			elem.webkitRequestFullscreen();
		}
		else if(elem.mozRequestFullscreen) {
			elem.mozRequestFullscreen();
		}
		else if(elem.msRequestFullscreen) {
			elem.msRequestFullscreen();
		}
		else {
			console.log("couldn't find fullscreen endpoint");
		}
	},

	// stop the currently running game
	'stop': function() {

		// need to also kill all outgoing requests
		s_stop = true;
		if (m_timeout !== null) {
			clearTimeout(m_timeout);
			m_timeout = null;
		}
	},

	'_loadModule': function(n) {
		throw "Load Module has not been defined";
	},

	'setExistingServer': function(path, server) {
		// todo log
		l0_net.setExistingServer(path, server);
	}
}
})();

