"use strict";

var l0_audio = (function() 
{ 
  const MAX_BUS_COUNT = 16;

  // on init this'll be passed in
  var m_hm = null;
  var s_audio_context = null;
  var s_asset_map = new Map();
  var s_buses = [];

  function getOrCreateBus(bus_id) 
  {
    if (bus_id < 0 || bus_id >= MAX_BUS_COUNT) {
      throw new Exception("Invalid bus id");
    }
    var i  = 0;
    while (i <= bus_id) 
    {
        if (s_buses.length < i || s_buses[i] == null) 
        {
            var gain_node = s_audio_context.createGain();
            gain_node.connect(s_audio_context.destination);
            gain_node.gain.value = 1;
   
            s_buses[i] = gain_node;
        }
        i++;
    }
    return s_buses[bus_id];
  }

  function fn_audioBusSetVolume(bus_id, value) {
      getOrCreateBus(bus_id).gain.value = value;
  }

  function fn_audioDataLoaded(data_id) {
    var audio_data =m_hm.resolveStrict(data_id, m_hm.HANDLE_AUDIO_DATA);
    return audio_data.loaded ? 1 : 0;
  }

  function fn_audioDataLoad(url) 
  {
    if (s_asset_map.has(url)) {
      return s_asset_map.get(url).id;
    }

    var data = {
      "id"     : -1,
      "buffer" : null,
      "loaded" : false
    };

    const request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    // Decode asynchronously
    request.onload = function() {
      s_audio_context.decodeAudioData(request.response, function(buffer) 
      {
        console.log("AUDIO DAtA BUFFER FILLED");
        // store the buffer???
        data.buffer =buffer;
        data.loaded = true;
      }, 
      function (ex) {
        console.log(ex);
        console.log("Failed to load: " + url);
      });
    }
    request.send();

    data.id =  m_hm.alloc(data,m_hm.HANDLE_AUDIO_DATA);
  
    s_asset_map.set(url, data);
    return data.id;
  }


  function fn_audioSourceCreate(bus_id, positional, x, y, z) 
  {
    if (positional)
    {
      var panner = audioCtx.createPanner();
      panner.panningModel = 'HRTF';
      panner.distanceModel = 'inverse';
      panner.refDistance = 1;
      panner.maxDistance = 10000;
      panner.rolloffFactor = 1;
      //panner.coneInnerAngle = 360;
      //panner.coneOuterAngle = 0;
      //panner.coneOuterGain = 0;
      panner.positionX.value = x;
      panner.positionY.value = y;
      panner.positionZ.value = z;
      panner.connect(getOrCreateBus(bus_id));

      const data = {
        "input":  panner,
        "gain":   null,
        "panner": panner,
        "loaded": true
      };

      return m_hm.alloc(data,m_hm.HANDLE_AUDIO_SOURCE);
    }
    else {
      const gain_node = s_audio_context.createGain();
      gain_node.connect(s_audio_context.destination);
      gain_node.gain.value = 1;

      const data = {
        "input":  gain_node,
        "gain" :  gain_node,
        "panner": null,
        "loaded": true
      };

      return m_hm.alloc(data,m_hm.HANDLE_AUDIO_SOURCE);
    }
  }




  function fn_audioSourceSetPosition(audio_source_id, x,y,z) 
  {
    var audio_source = m_hm.resolveStrict(audio_source_id, m_hm.HANDLE_AUDIO_SOURCE);
    audio_source.panner.positionX = x;
    audio_source.panner.positionY = y;
    audio_source.panner.positionZ = z;
  }
  
  function fn_audioSourceSetVelocity(audio_source_id, vx,vy,vz) {
    var audio_source = m_hm.resolveStrict(audio_source_id, m_hm.HANDLE_AUDIO_SOURCE);
    if (audio_source.panner == null) {
       return;
    }


  }
  
  function fn_audioListenerSetOrientation(x,y,z, fx,fy,fz, ux, uy, uz) {
    const listener = s_audio_context.listener;
    listener.positionX.value = x;
    listener.positionY.value = y;
    listener.positionZ.value = z;
    listener.forwardX.value = fx;
    listener.forwardY.value = fy;
    listener.forwardZ.value = fz;
    listener.upX.value = ux;
    listener.upY.value = uy;
    listener.upZ.value = uz;
  }


  function fn_audioSourceDestroy(audio_source_id) 
  {
    var audio_source = m_hm.
      resolveStrict(audio_source_id, m_hm.HANDLE_AUDIO_SOURCE);

    if (audio_source.panner != null) {
      audio_source.panner.disconnect();
      audio_source.panner = null;
    }
     if (audio_source.gain != null) {
      audio_source.gain.disconnect();
      audio_source.gain = null;
    }
    FreeHandle(audio_source);
  }

  function fn_audioSourcePlay(audio_source_id, audio_data_id, volume)
  {
    const audio_source = m_hm.resolveStrict(audio_source_id, m_hm.HANDLE_AUDIO_SOURCE);
    const audio_data   = m_hm.resolveStrict(audio_data_id, m_hm.HANDLE_AUDIO_DATA);
    if (!audio_data.loaded) {
      console.error("Can't play unloaded audio");
      return false;
    }


    var gain_node = s_audio_context.createGain();
    gain_node.connect(audio_source.input);
    gain_node.gain.value = volume;
    
    const buffer_source    = s_audio_context.createBufferSource();
    buffer_source.buffer = audio_data.buffer;
    buffer_source.connect(gain_node);
    


    buffer_source.onended = function() {
      gain_node.disconnect();
      buffer_source.disconnect();
    };

    buffer_source.start(s_audio_context.currentTime);
  }

  function fn_init()
  {
    s_audio_context = new (window.AudioContext || window.webkitAudioContext);
   
   /*
    //https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Web_audio_spatialization_basics
    const listener = s_audio_context.listener;
    listener.positionX.value = 0;
    listener.positionY.value = 0;
    listener.positionZ.value = 0;
    listener.forwardX.value = 0;
    listener.forwardY.value = 0;
    listener.forwardZ.value = -1;
    listener.upX.value = 0;
    listener.upY.value = 1;
    listener.upZ.value = 0;
    */
  }

  /*
var m_hm = {

  'HANDLE_IMAGE'        : 1,
  'HANDLE_MESH'         : 2,
  'HANDLE_AUDIO_DATA'   : 3,
  'HANDLE_WEBSOCKET'    : 4,
  'HANDLE_SHADER'       : 5,
  'HANDLE_HTTPREQUEST'  : 6,
  'HANDLE_SURFACE'      : 7,
  'HANDLE_AUDIO_SOURCE' : 8,

  'alloc':          handleAlloc,      // data, type
  'resolve':        handleResolve,    // id, type
  'resolveStrict' : handleResolveStrict, // id, type
  'handleDestroy' : handleDestroy   // id, typ
};
*/

  return {
    "init" : function(vm, main_module, hm) 
    {
      m_hm = hm;

      fn_init();

      vm.setNative(main_module, "audioDataLoad", function(url) {
          return fn_audioDataLoad(vm.stringToNative(url));
        });;
      vm.setNative(main_module, "audioDataLoaded", fn_audioDataLoaded);


      vm.setNative(main_module, "audioSourceCreate",  fn_audioSourceCreate);
      vm.setNative(main_module, "audioSourceSetPosition", fn_audioSourceSetPosition);
      vm.setNative(main_module, "audioSourceSetVelocity", fn_audioSourceSetVelocity);
      vm.setNative(main_module, "audioListenerSetOrientation", fn_audioListenerSetOrientation);
      vm.setNative(main_module, "audioBusSetVolume",    fn_audioBusSetVolume);
      vm.setNative(main_module, "audioSourceDestroy",   fn_audioSourceDestroy);
      vm.setNative(main_module, "audioSourcePlay",      fn_audioSourcePlay);
    }
  };
})();
