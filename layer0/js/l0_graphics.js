"use strict";

var l0_graphics = (function() 
{
	var m_gl = null;
	var SCREEN_FRAMEBUFFER = null;

	var m_active_attributes     = [];
	var m_active_attribute_keys = [];
	var m_active_textures       = [];
	var m_active_texture_keys   = [];
	//////////////////////////////////////////////////////////


	/**
	 * Flag if the resource in question as being in use.
	 * Returns whether the resouce in questions was already in use
	 */
	function markUse(id,     key_array, active_array)
	{
		var idx = key_array.indexOf(id);
		if (idx == -1) {
			key_array.push(id);
			active_array.push(false);
			idx = active_array.length - 1;
		}

		const is_active = active_array[idx];
		active_array[idx] = true;
		return is_active;
	}


	/**
	 * Invokes the callback in question if the resource isnot in use

	 */

	function cleanUse(key_array, active_array, callback) 
	{
		var len = 0;
		const curr_len = key_array.length;
		for (var i = 0; i < curr_len; i++) {
			const id     = key_array[i];
			const active = active_array[i];
			if (!active) {
				callback(id);
				continue;
			}

			key_array   [len] = id;
			active_array[len] = true;
			len++;
		}

		key_array   .length = len;
		active_array.length = len; 
	}
	//////////////////////////////////////////////////////////
	// FrameBuffer
	//////////////////////////////////////////////////////////

	function L0Framebuffer(w,h) {
		this.m_gl_id       = null;
		this.m_width       = w;
		this.m_height      = h;
		//this.m_attachments = [];
	}


	L0Framebuffer.prototype.bind = function() {
		//console.log("bind " + this.m_gl_id + " " + this.m_width + " " + this.m_height);
		m_gl.bindFramebuffer(m_gl.FRAMEBUFFER, this.m_gl_id);
 	   // Tell WebGL how to convert from clip space to pixels
    	m_gl.viewport(0, 0, this.m_width, this.m_height);
	
      	m_gl.depthFunc(m_gl.LEQUAL);
		m_gl.enable   (m_gl.CULL_FACE);
        m_gl.cullFace (m_gl.BACK);
        m_gl.frontFace(m_gl.CCW);	
	}

	L0Framebuffer.prototype.bindAndClear = function() {
		this.bind();
		this.clear(null);
	}


	L0Framebuffer.prototype.clear = function(clear_color) {
		// well crap.. you say
		m_gl.depthMask(true);

 		if (clear_color !== null) {
 			m_gl.clearColor(clear_color.r, clear_color.g, clear_color.b, clear_color.a);
 		}
 		else if (this.m_gl_id != null) {
 			m_gl.clearColor(0.0, 1.0, 0.0, 1.0);
 		}
 		else {
			m_gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
      	}
      	m_gl.clearDepth(1.0);                 // Clear everything
      	m_gl.clear(m_gl.COLOR_BUFFER_BIT | m_gl.DEPTH_BUFFER_BIT);
	}

	///////////////////////////////////////////////////////////////
	// Shader
	///////////////////////////////////////////////////////////////

	function L0Shader() {
		this.m_gl_id = null;
	}

	L0Shader.prototype.bind = function() {
		m_gl.useProgram(this.m_gl_id); 
	}


	L0Shader.prototype.getAttribLocation = function(key) {
		return m_gl.getAttribLocation(this.m_gl_id, key);
    };


	L0Shader.prototype.getUniformLocation = function(key) {
		return m_gl.getUniformLocation(this.m_gl_id, key);
    };

	//
	// Initialize a shader program, so WebGL knows how to draw our data
	////////////////////////////////////////////////////////////////////

	function createShaderProgram(gl, vsSource, fsSource) 
	{
	  const vertexShader   = loadShader(gl, gl.VERTEX_SHADER, vsSource);
	  const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

	  if (vertexShader === null || fragmentShader === null) {
	  	return null;
	  }

	  // Create the shader program
	  const shaderProgram = gl.createProgram();
	  gl.attachShader(shaderProgram, vertexShader);
	  gl.attachShader(shaderProgram, fragmentShader);
	  gl.linkProgram(shaderProgram);

	  // If creating the shader program failed, alert
	  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
	  	const log = gl.getProgramInfoLog(shaderProgram);
	  	console.log(log);
	    alert('Unable to initialize the shader program: ' + log);
	    return null;
	  }

	  return shaderProgram;
	}

	//
	// creates a shader of the given type, uploads the source and
	// compiles it.
	/////////////////////////////////////////////////////////////////

	function loadShader(gl, type, source) {
	  const shader = gl.createShader(type);

	  // Send the source to the shader object

	  gl.shaderSource(shader, source);

	  // Compile the shader program

	  gl.compileShader(shader);

	  // See if it compiled successfully

	  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
	  	const log = gl.getShaderInfoLog(shader);
	  	
	  	console.log("SHADER COMPILATION FAILED: ");
	  	console.log(source);

	  	console.log("LOG FOLLOWS: ");
	    console.log(log)



	    alert('An error occurred compiling the shaders: ' + log);
	    gl.deleteShader(shader);
	    return null;
	  }

	  return shader;
	}

	return {
		init: function(gl, w, h) {
			m_gl               = gl;
			SCREEN_FRAMEBUFFER = new L0Framebuffer(w,h);
		},

		activeAttribute: function(id) {
			if (!markUse(id, m_active_attribute_keys, m_active_attributes)) {
				m_gl.enableVertexAttribArray(id);
			}
		},

		activeTexture: function(texture_unit, mode, data) {
			markUse(texture_unit, m_active_texture_keys, m_active_textures);

			m_gl.activeTexture(m_gl.TEXTURE0 + texture_unit);
			if (null == data) {
				m_gl.bindTexture  (m_gl.TEXTURE_2D, null);
			}
			else {
				m_gl.bindTexture  (mode, data);
			}
		},

		activeMark() {
			for (let i = m_active_textures.length - 1; i>= 0; i--) {
				m_active_textures[i] = false;
			}
		
			for (let i = m_active_attributes.length - 1; i>= 0; i--) {
				m_active_attributes[i] = false;
			}
		},

		activeClean: function() {
			cleanUse(m_active_attribute_keys, m_active_attributes, function (id) {
				m_gl.disableVertexAttribArray(id);
			});

			cleanUse(m_active_texture_keys, m_active_textures, function(id) {
				m_gl.activeTexture(m_gl.TEXTURE0 + id, null, null)
			});
		},

		getDefaultFramebuffer: function() {
			return SCREEN_FRAMEBUFFER;
		},


		/**
		 This is actually used
		 **/
		createFramebuffer: function (gl_id,w,h) 
		{
			const fb = new L0Framebuffer(w, h);
			fb.m_gl_id = gl_id

			return fb;
		},

		createShader: function(vertex_source, fragment_source) {
			let s     = new L0Shader();
			let gl_id = createShaderProgram(m_gl, vertex_source, fragment_source);

			s.m_gl_id = gl_id;
			return s;
		}

	};
})();
