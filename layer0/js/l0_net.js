"use strict";
      
var l0_net = (function() 
{	
	///////////////////////////////////////////////////////////////////
	// Net Group
	///////////////////////////////////////////////////////////////////

	const SOCKET_STATE_CONNECTING  = 1;
	const SOCKET_STATE_CONNECTED   = 2;
	const SOCKET_STATE_ERROR       = 3;
	const SOCKET_STATE_CLOSING     = 4;
	const SOCKET_STATE_CLOSED      = 5;
	const SOCKET_STATE_ACCEPTING   = 10;
	const SOCKET_STATE_ACCEPT      = 11;
	const SOCKET_STATE_MESSAGE     = 12;


	const SOCKET_OP_READ           = 1;
	const SOCKET_OP_WRITE          = 2;
	const SOCKET_OP_CLOSE          = 3;

	var s_active_servers = {};
	var s_hm   = null;
	var s_vm = null;

	function SocketImp(ws, listener, state, child) 
	{
		//////////////////////////////////////////////////////////
		// Constructor Logic

		this.m_socket    = ws;
		this.m_socket_id = 0;
		this.m_listener  = listener;
		this.m_state     = state;
		this.m_queue     = [];
		
		const self = this;
		if (child )
		{
			ws.onopen = function () {
				//console.error("onopen");
				self.setState(SOCKET_STATE_CONNECTED);
			};

			ws.onerror = function (error) {

				//console.error("onerror");
				self.setState(SOCKET_STATE_ERROR);
				self.setState(SOCKET_STATE_CLOSED);
			};

			ws.onmessage = function (event) {
				//console.error("onmessage");
				self.message(event.data);
			};

			ws.onclose = function (message) {

				console.error("onclose");
				self.setState(SOCKET_STATE_CLOSED);
			};
		}
	}

	SocketImp.prototype.setListener = function(l) {
		this.m_listener = l;
	};

	SocketImp.prototype.setState = function(state_id) {
		if (this.m_state != state_id) 
		{
		    this.m_state = state_id;
		    if (s_vm.isNull(this.m_listener)) {
				return null;
			}
		    s_vm.callNamed("onSocketEvent", this.m_listener, this.m_socket_id, state_id, 0);  
		}
	};

	SocketImp.prototype.isClosedOrClosing = function() {
		return this.m_state === SOCKET_STATE_CLOSING ||
		    this.m_state === SOCKET_STATE_CLOSED;
	};

	SocketImp.prototype.accept = function(child, ip, referrer, data) {
		if (s_vm.isNull(this.m_listener)) {
			return null; // ??? todo replace null w something from the vm
		}
	  	return s_vm.callNamed( "onSocketAccept",this.m_listener, 
	  		this.m_socket_id,
	  		child.m_socket_id,
	  		s_vm.nativeToString(ip),
	  		s_vm.nativeToString(referrer),
	  		s_vm.nativeToString(data)); 
	};

	SocketImp.prototype.message = function(msg) {
	    // just reject the message if we are closing
	    
	    if (this.isClosedOrClosing()) {
	      return;
	    }
	    this.m_queue.push(msg);
	    if (s_vm.isNull(this.m_listener)) {
			return;
		}
	    s_vm.callNamed("onSocketEvent",this.m_listener,  this.m_socket_id, SOCKET_STATE_MESSAGE, 0);  
	};


	function fn_socketImpOp(ws_id, op, buffer, start, len)
	{
		if (op == SOCKET_OP_READ)
		{
			// read
			const ws = s_hm.resolveStrict(ws_id, s_hm.HANDLE_WEBSOCKET);
			const q  = ws.m_queue;
			//console.error("q len: " + q.length);
			if (q.length == 0) {
				return 0;	// ?? should be throw an exception
			}
			//console.error(new Int8Array(q[0]));
			//console.error("q0 le: " + q[0].byteLength);
			const ba = s_vm.populateByteArray(buffer, q[0], 0, q[0].byteLength);
			q.shift();
			return 1;
		}
		else if (op === SOCKET_OP_WRITE) 
		{
			// write
			const ws = s_hm.resolveStrict(ws_id, s_hm.HANDLE_WEBSOCKET);
			if (ws.m_state === SOCKET_STATE_CONNECTED) {
				const buf = s_vm.byteArrayToNativeArrayBuffer(buffer);
				//console.error("writing: ");
				//console.error(new Int8Array(buf));
				const ready_state = ws.m_socket.readyState;
				if (2 == ready_state) {
					console.error("socket is already closing");
				}
				else
				{
					ws.m_socket.send(buf);
				}

			}
			else {
				console.error("not writing- " + ws.m_state);
			}
			return 0;
		}
		else if (op == SOCKET_OP_CLOSE) 
		{
			// close
			const ws = s_hm.resolveStrict(ws_id, s_hm.HANDLE_WEBSOCKET);
			if (!ws.isClosedOrClosing()) {
				ws.m_socket.close();
				ws.setState(SOCKET_STATE_CLOSING);
			}
			return 0;
		}
		else {
			throw "Unknown socket op";
		}
	}


	function stripProtocolAndPath(p) {
		const missing_protocol = p.substr(p.indexOf("://") + 3);
		const pos =  missing_protocol.indexOf("/");
		if (pos >= 0) {
			return missing_protocol.substr(0, pos);
		}
		else {
			return missing_protocol;
		}
	}

	function fn_socketImpConnect(address, listener, is_server)
	{ 
		address = s_vm.stringToNative(address);

		if (address.indexOf("ws://") !== 0 &&
			address.indexOf("wss://") !== 0) {
		 	s_vm.throwException("We only support websockets at the moment. Found: " + address);
		}
		

		console.error("addr addr addr: " + address);

		if (is_server) 
		{
			console.error("l0_net: Creating Server Socket");
		  	const $ws        = l0._loadModule('ws');
		  	const $url       = l0._loadModule('url');

		  	console.error('wstype: ' + typeof($ws));
		  	console.error('urltype: ' + typeof($url));

		  	if (typeof($url) === 'undefined') {
		  		throw new Exception("Failed to load url require");
		  	}

		  	if (typeof($ws) === 'undefined') {
		  		throw new Exception("Failed to load websocket require");
		  	}

		  	const addr       = stripProtocolAndPath(address);
		  	const addr_path  = $url.parse(address).pathname;
		  	let   wss        = null;

			console.error("address we're attempting. " + address);
			console.error("the addr we're querying "  + addr);
		  	console.error("l0_net: Existing servers: ");
		  	for (let k in s_active_servers) {
		  		console.error(k);
		  	}
		  	// check if there is already an express server open on the given
		  	// port that we want to use
		  	if (s_active_servers.hasOwnProperty(addr)) 
		  	{

		  		wss = new $ws.Server({noServer: true});
		  		let server = s_active_servers[addr];
		  		
		  		console.error("l0_net: Binding to existing server at path: " + addr_path);
		  		

		  		server.on('upgrade', function upgrade(request, socket, head) 
		  		{
		  			const pathname = $url.parse(request.url).pathname;
		  			if (pathname === addr_path) {
						wss.handleUpgrade(request, socket, head, function done(ws) {
							wss.emit('connection', ws, request);
						});
					} 
					else {
						socket.destroy();
					}
		  		});
		  	}
		  	else {
		  		// but where does port come from?
		  		let port = 80;
		  		const colon_pos = addr.indexOf(':');
		  		if (colon_pos >= 0) {
		  			port = parseInt(addr.substring(colon_pos, addr.length));
		  		}
		  		wss = new $ws.Server({ 'port': port });
		  	}

			

			const connection_server = new SocketImp(wss, listener, SOCKET_STATE_ACCEPTING, false);
			const wss_handle_id = s_hm.alloc(connection_server, s_hm.HANDLE_WEBSOCKET);
			connection_server.m_socket_id = wss_handle_id;

			wss.on('connection', function(connection, req) 
			{
				connection.binaryType = 'arraybuffer';
				const child           = new SocketImp(connection, null, SOCKET_STATE_CONNECTED, true);
				const child_handle_id = s_hm.alloc(child, s_hm.HANDLE_WEBSOCKET);
				child.m_socket_id     = child_handle_id;

				// get the ip and whoever connected to this listener
				const remote_address = req.connection.remoteAddress;
				const origin         = req.headers.origin;
				const data           = req.url;

				const child_listener = connection_server.accept(child, remote_address, origin, data);
				if (!s_vm.isNull(child_listener)) {
					child.setListener(child_listener);
				}
				else {
					connection.close();
				}
		  });

		  return wss_handle_id;
		}
		// an outgoing socket was requested
		/////////////////////////////////////
		else 
		{
			console.error("l0_net: creating outgoing websocket: " + address);
			const   connection        = new WebSocket(address);
			connection.binaryType = 'arraybuffer';

			const c         = new SocketImp(connection, listener, SOCKET_STATE_CONNECTING, true);
			const handle_id = s_hm.alloc(c, s_hm.HANDLE_WEBSOCKET);
			c.m_socket_id   = handle_id;

			return handle_id;
		}
	}

	return {
		'init': function (igvm, main_module, handle_manager) {
			s_hm = handle_manager;
			s_vm = igvm;
			console.error('l0_net.init - ' + l0.MAIN_MODULE);
	    	igvm.setNative(main_module, "socketImpConnect", fn_socketImpConnect);
	      	igvm.setNative(main_module, "socketImpOp",      fn_socketImpOp);
		},

		// this is to hack around express 
		// so that we can use the same port for WS and normal http traffic
		'setExistingServer': function(p, s) {
			s_active_servers[stripProtocolAndPath(p)] = s;
		}
	};
})();
